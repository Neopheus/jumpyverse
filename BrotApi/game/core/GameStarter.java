package de.brotcrunsher.game.core;

import java.awt.Insets;
import java.awt.event.KeyEvent;

import Control.Keyboard;
import Control.Mouse;


public class GameStarter implements Runnable{
	private static int screenWidth = 800;
	private static int screenHeight = 600;
	private static int napTime = 15;
	private static String title = "Title";
	private static boolean exitOnEsc = true;
	
	public static void setScreenWidth(int screenWidth){
		GameStarter.screenWidth = screenWidth;
	}
	public static void setScreenHeight(int screenHeight){
		GameStarter.screenHeight = screenHeight;
	}
	public static void setNapTime(int napTime){
		GameStarter.napTime = napTime;
	}
	public static void setTitle(String title){
		GameStarter.title = title;
	}
	public static void setExitOnEsc(boolean exitOnEsc){
		GameStarter.exitOnEsc = exitOnEsc;
	}
	
	public static String getTitle(){
		return title;
	}
	public static int getScreenWidth(){
		return screenWidth;
	}
	public static int getScreenHeight(){
		return screenHeight;
	}
	public static int getNapTime(){
		return napTime;
	}
	public static boolean isExitOnEsc(){
		return exitOnEsc;
	}
	
	public static void startGame(Game game){
		new Thread(new GameStarter(game)).start();
	}
	
	private final Game game;
	
	private GameStarter(Game game){
		this.game = game;
	}
	
	@Override
	public void run() {
		game.preInitialize();
		Screen f = new Screen(game, screenWidth, screenHeight, title);
		
		game.initialize();
		
		
		long lastFrame = System.currentTimeMillis();
		while(true){
			if(Keyboard.isKeyDown(KeyEvent.VK_ESCAPE) && exitOnEsc) System.exit(0);
			
			long thisFrame = System.currentTimeMillis();
			float timeSinceLastFrame = ((float)(thisFrame-lastFrame))/1000f;
			lastFrame = thisFrame;
			
			
			game.update(timeSinceLastFrame);
			
			
			Keyboard.reset();
			Mouse.reset();
			
			f.redraw();
			try {
				Thread.sleep(napTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
