package de.brotcrunsher.game.core;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

import de.brotcrunsher.gfx.Brush;

import Control.Keyboard;
import Control.Mouse;

class Screen extends JFrame{
	
	private BufferStrategy strat;
	private Game game;
	private Brush brush;
	
	public Screen(Game game, int screenWidth, int screenHeight, String title){
		super(title);
		setLayout(null);
		addKeyListener(new Keyboard());
		Mouse m = new Mouse();
		addMouseListener(m);
		addMouseMotionListener(m);
		this.game = game;
		
		brush = new Brush();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		getContentPane().setPreferredSize(new Dimension(screenWidth, screenHeight));
		//setSize(screenWidth, screenHeight);
		pack();
		setVisible(true);
		setResizable(false);
		setLocationRelativeTo(null);
		
		
		try {
			Thread.sleep(300);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		makeStrat();
		makeOffset();
		pack();
	}
	
	public void makeStrat() {
		createBufferStrategy(2);
		strat = getBufferStrategy();
	}
	
	public void makeOffset(){
		brush.setOffsetX(getInsets().left);
		brush.setOffsetY(getInsets().top);
		Mouse.setOffsetX(getInsets().left);
		Mouse.setOffsetY(getInsets().top);
	}
	
	
	public void redraw() {
		Graphics g = strat.getDrawGraphics();
		brush.setGraphics((Graphics2D)g);
		draw(brush);
		g.dispose();
		strat.show();
	}

	private void draw(Brush g) {
		game.draw(g);
	}
}