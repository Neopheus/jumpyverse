package de.brotcrunsher.game.core;

import java.awt.Graphics;
import java.awt.Rectangle;

import de.brotcrunsher.gfx.Brush;

public interface IGameObject {
	public void update(float timeSinceLastFrame);
	public void draw(Brush g);
	public Rectangle getBounding();
	public float getX();
	public float getY();
	public void setX(float x);
	public void setY(float y);
}
