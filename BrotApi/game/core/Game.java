package de.brotcrunsher.game.core;

import de.brotcrunsher.gfx.Brush;

public interface Game {
	/**
	 * The first method called! Everything here will be executed before everything else.
	 */
	public void preInitialize();
	
	/**
	 * This method is called when the frame is made for the first time.
	 */
	public void initialize();
	
	/**
	 * Called every Gameframe.
	 * @param timeSinceLastFrame
	 */
	public void update(float timeSinceLastFrame);
	
	/**
	 * Called every Gameframe.
	 * @param g
	 */
	public void draw(Brush g);
}
