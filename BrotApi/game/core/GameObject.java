package de.brotcrunsher.game.core;

import java.awt.Graphics;
import java.awt.Rectangle;

import de.brotcrunsher.gfx.Brush;

public abstract class GameObject implements IGameObject{
	protected float x;
	protected float y;
	protected Rectangle bounding;
	protected final static int DIRECTIONUP = 0;
	protected final static int DIRECTIONDOWN = 1;
	protected final static int DIRECTIONLEFT = 2;
	protected final static int DIRECTIONRIGHT = 3;
	
	
	
	@Override
	public abstract void update(float timeSinceLastFrame);

	@Override
	public abstract void draw(Brush g);
	
	@Override
	public Rectangle getBounding(){
		return bounding;
	}
	
	@Override
	public float getX() {
		return x;
	}

	@Override
	public float getY() {
		return y;
	}

	@Override
	public void setX(float x) {
		this.x = x;
	}

	@Override
	public void setY(float y) {
		this.y = y;
	}

}
