package de.brotcrunsher.util;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * This Class represents the content of your Clipboard (CTRL+C and CTRL+V)
 * @author Jakob Schaal
 *
 */
public class ClipboardContent implements ClipboardOwner {
	private static final Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	private static final ClipboardOwner clipboardOwner = new ClipboardContent();
	
	
	
	public static void setClipboardContent(String string){
		clipboard.setContents(new StringSelection(string), clipboardOwner);
	}
	
	public static String getClipboardContents() throws IOException{
		try {
			return (String)clipboard.getContents(null).getTransferData(DataFlavor.stringFlavor);
		} catch (UnsupportedFlavorException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@Override
	public void lostOwnership(Clipboard clipboard, Transferable contents) {
		//DO NOTHING
	}
}
