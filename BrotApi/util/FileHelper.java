package de.brotcrunsher.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.FileChannel;

public class FileHelper {
	//TODO Javadoc!
	
	
	public static void copyFile(File source, File destination) throws IOException {
		destination.createNewFile();	//Creates a new File if it does not exist yet.
		FileChannel sourceChannel = null;
		FileChannel destinationChannel = null;
		try {
			sourceChannel = new FileInputStream(source).getChannel();
			destinationChannel = new FileOutputStream(destination).getChannel();
			destinationChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
		}
		finally {
			if(sourceChannel != null) {
				sourceChannel.close();
			}
			if(destinationChannel != null) {
				destinationChannel.close();
			}
		}
	}
	
}
