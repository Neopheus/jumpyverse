package de.brotcrunsher.util;

import java.util.Random;

import Wrapper.Circle;
import Wrapper.Point;

/**
 * This class helps you generating different random things like a random Point in or on a Circle.
 * 
 * @author JakobSchaal
 *
 */
public abstract class RandomGenerator {
	//TODO Not yet tested!
	private final static Random r = new Random();
	
	/**
	 * Calculates a random {@link Point} inside a circle.
	 * @param radius The radius of the circle.
	 * @return The random Point.
	 */
	public static Point pointInCircle(float radius){
		while(true){
			float x = r.nextFloat()*radius*2-radius;
			float y = r.nextFloat()*radius*2-radius;
			float abs = x*x+y*y;	//Note that this isn't the real abs value, a sqrt calculation would be too expensive.
			if(abs<=radius*radius){
				return new Point(x, y);
			}
		}
	}
	
	/**
	 * Calculates a random {@link Point} inside the given {@link Circle}.
	 * @param c The Circle
	 * @return The random Point.
	 */
	public static Point pointInCircle(Circle c){
		return pointInCircle(c.getRadius(), c.getX(), c.getY());
	}
	
	/**
	 * Calculates a random {@link Point} inside a circle with the given radius and a center at X/Y.
	 * @param radius The radius of the circle.
	 * @param x The X value of the center of the circle.
	 * @param y The Y value of the center of the circle.
	 * @return
	 */
	public static Point pointInCircle(float radius, float x, float y){
		Point p = pointInCircle(radius);
		p.add(x, y);
		return p;
	}
	
	/**
	 * Calculates a random {@link Point} on a circle.
	 * @param radius The Radius of the Circle.
	 * @return The random Point.
	 */
	public static Point pointOnCircle(float radius){
		double rad = r.nextFloat()*Math.PI*2;
		float sin = (float) Math.sin(rad);
		float cos = (float) Math.cos(rad);
		return new Point(sin*radius, cos*radius);
	}
	
	/**
	 * Calculates a random {@link Point} on the given {@link Circle}.
	 * @param c The Circle.
	 * @return The random Point.
	 */
	public static Point pointOnCircle(Circle c){
		return pointOnCircle(c.getRadius(), c.getX(), c.getY());
	}
	
	/**
	 * Calculates a random {@link Point} on a circle with the given radius and a center at X/Y.
	 * @param radius The Radius of the Circle.
	 * @param x The X value of the center of the circle.
	 * @param y The Y value of the center of the circle.
	 * @return The random Point.
	 */
	public static Point pointOnCircle(float radius, float x, float y){
		Point p = pointOnCircle(radius);
		p.add(x, y);
		return p;
	}
}
