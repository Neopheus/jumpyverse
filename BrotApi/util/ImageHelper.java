package de.brotcrunsher.util;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageHelper {
	//TODO Javadoc
	
	public static BufferedImage createBufferedImage(int width, int hight, int imageType, int rgb){
		BufferedImage bi = new BufferedImage(width, hight, imageType);
		for(int x = 0; x<width; x++){
			for(int y = 0; y<hight; y++){
				bi.setRGB(x, y, rgb);
			}
		}
		return bi;
	}
	
	
	public static BufferedImage createBufferedImage(int width, int hight, int imageType, Color color){
		if(color == null)throw new RuntimeException("color was null!");
		return createBufferedImage(width, hight, imageType, color.getRGB());
	}
	
	public static void setColor(BufferedImage bi, int rgb){
		for(int x = 0; x<bi.getWidth(); x++){
			for(int y = 0; y<bi.getHeight(); y++){
				bi.setRGB(x, y, rgb);
			}
		}
	}
	
	public static void setColor(BufferedImage bi, Color c){
		setColor(bi, c.getRGB());
	}
	
	
	public static BufferedImage rotateImage(BufferedImage img, int theta){
		AffineTransform at = new AffineTransform();
		at.translate(img.getHeight()/2, img.getWidth()/2);
		at.rotate(Math.toRadians(theta));
		at.translate(-img.getWidth()/2, -img.getHeight()/2);
		BufferedImage newIMG = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
		Graphics2D g = (Graphics2D) newIMG.getGraphics();
		
		g.drawImage(img, at, null);
		
		g.dispose();
		return newIMG;
	}
	
	
	
	public static BufferedImage load(String path) throws IOException{
		return ImageIO.read(ImageHelper.class.getClassLoader().getResourceAsStream(path));
	}
}
