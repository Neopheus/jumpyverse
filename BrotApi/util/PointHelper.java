package de.brotcrunsher.util;

import java.awt.Polygon;

import Wrapper.Point;

public abstract class PointHelper {
	//TODO Not yet Tested.
	//TODO Javadoc! NooB!
	
	public static Polygon pointarrayToPolygon(Point[] p){
		int[] x = new int[p.length];
		int[] y = new int[p.length];
		for(int i = 0; i<p.length; i++){
			x[i]=(int) p[i].getX();
			y[i]=(int) p[i].getY();
		}
		return new Polygon(x, y, p.length);
	}
}
