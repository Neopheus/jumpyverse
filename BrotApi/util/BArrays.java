package de.brotcrunsher.util;

public class BArrays {
	public static void fillArrayRandom(int[] array){
		for(int i = 0; i<array.length; i++){
			array[i]=Random.nextInt();
		}
	}
	
	public static void fillArrayRandom(int[] array, int min, int max){
		for(int i = 0; i<array.length; i++){
			array[i]=Random.nextInt(max-min+1)+min;
		}
	}
}
