package de.brotcrunsher.util;

import java.awt.Color;

public class ColorGenerator {
	public static Color getRandomRed(){
		int r = Random.nextInt(106)+150;
		int g = Random.nextInt(40);
		int b = Random.nextInt(40);
		
		return new Color(r, g, b);
	}
	
	public static Color getRandomGreen(){
		int r = Random.nextInt(40);
		int g = Random.nextInt(106)+150;
		int b = Random.nextInt(40);
		
		return new Color(r, g, b);
	}
	
	public static Color getRandomBlue(){
		int r = Random.nextInt(40);
		int g = Random.nextInt(40);
		int b = Random.nextInt(106)+150;
		
		return new Color(r, g, b);
	}
	
	public static Color getRandomOrange(){
		int r = Random.nextInt(56)+200;
		int g = Random.nextInt(40)+120;
		int b = Random.nextInt(40);
		
		return new Color(r, g, b);
	}
	
	public static Color getRandomBrown(){
		int r = Random.nextInt(44)+84;
		int g = (int)(r/1.95);
		int b = 0;
		
		return new Color(r, g, b);
	}
}
