package de.brotcrunsher.util;

/**
 * This Class wrapps java.util.Random so it can be used staticly.
 * 
 * @author Jakob Schaal aka. Brotcrunsher
 *
 */
public class Random {
	private static java.util.Random r = new java.util.Random();
	
	public static boolean nextBoolean(){
		return r.nextBoolean();
	}
	
	public static void nextBytes(byte[] bytes){
		r.nextBytes(bytes);
	}
	
	public static double nextDouble(){
		return r.nextDouble();
	}
	
	public static float nextFloat(){
		return r.nextFloat();
	}
	
	public static double nextGaussian(){
		return r.nextGaussian();
	}
	
	public static int nextInt(){
		return r.nextInt();
	}
	
	public static int nextInt(int n){
		return r.nextInt(n);
	}
	
	public static long nextLong(){
		return r.nextLong();
	}
	
	public static void setSeed(long seed){
		r.setSeed(seed);
	}
}
