package de.brotcrunsher.util;

public class Numbers {
	public static final int BIGGESTPRIME = 2147483647;
	public static final double PI = Math.PI;
	public static final double E = Math.E;
	public static final double GOLDENRATIO = 1.6180339887498948482;
}
