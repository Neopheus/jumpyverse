package de.brotcrunsher.util;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Bot{
	private static Robot rob = null;
	static{
		try {
			rob = new Robot();
		} catch (AWTException e) {e.printStackTrace();}
	}
	
	public static BufferedImage createScreenCapture(Rectangle screenRect){
		return rob.createScreenCapture(screenRect);
	}
	
	public static void delay(int ms){
		rob.delay(ms);
	}
	
	public static int getAutoDelay(){
		return rob.getAutoDelay();
	}
	
	public static Color getPixelColor(int x, int y){
		return rob.getPixelColor(x, y);
	}
	
	public static boolean isAutoWaitForIdle(){
		return rob.isAutoWaitForIdle();
	}
	
	public static void keyPress(int keycode){
		rob.keyPress(keycode);
	}
	
	public static void keyRelease(int keycode){
		rob.keyRelease(keycode);
	}
	
	public static void keyPush(int keycode){
		rob.keyPress(keycode);
		rob.keyRelease(keycode);
	}
	
	public static void mouseMove(int x, int y){
		rob.mouseMove(x, y);
	}
	
	public static void mousePress(int buttons){
		rob.mousePress(buttons);
	}
	
	public static void mouseRelease(int buttons){
		rob.mouseRelease(buttons);
	}
	
	public static void mousePush(int buttons){
		rob.mousePress(buttons);
		rob.mouseRelease(buttons);
	}
	
	public static void mouseWheel(int wheelAmt){
		rob.mouseWheel(wheelAmt);
	}
	
	public static void setAutoDelay(int ms){
		rob.setAutoDelay(ms);
	}
	
	public static void setAutoWaitForIdle(boolean isOn){
		rob.setAutoWaitForIdle(isOn);
	}
	
	public static void waitForIdle(){
		rob.waitForIdle();
	}
	
	public static void writeString(String s, boolean clipboard) throws IOException{
		if(!clipboard){
			writeString(s);
			return;
		}
//		String oldTxt = ClipboardContent.getClipboardContents();
		
		ClipboardContent.setClipboardContent(s);
		
		keyPress(KeyEvent.VK_CONTROL);
		keyPress(KeyEvent.VK_V);
		keyRelease(KeyEvent.VK_V);
		keyRelease(KeyEvent.VK_CONTROL);
		
//		ClipboardContent.setClipboardContent(oldTxt);
	}
	
	public static void writeString(String s){
		for(int i = 0; i<s.length(); i++){
			char c = s.charAt(i);
			switch(c){
			//Small Letters
			case('a'):
				keyPush(KeyEvent.VK_A);
				break;
			case('b'):
				keyPush(KeyEvent.VK_B);
				break;
			case('c'):
				keyPush(KeyEvent.VK_C);
				break;
			case('d'):
				keyPush(KeyEvent.VK_D);
				break;
			case('e'):
				keyPush(KeyEvent.VK_E);
				break;
			case('f'):
				keyPush(KeyEvent.VK_F);
				break;
			case('g'):
				keyPush(KeyEvent.VK_G);
				break;
			case('h'):
				keyPush(KeyEvent.VK_H);
				break;
			case('i'):
				keyPush(KeyEvent.VK_I);
				break;
			case('j'):
				keyPush(KeyEvent.VK_J);
				break;
			case('k'):
				keyPush(KeyEvent.VK_K);
				break;
			case('l'):
				keyPush(KeyEvent.VK_L);
				break;
			case('m'):
				keyPush(KeyEvent.VK_M);
				break;
			case('n'):
				keyPush(KeyEvent.VK_N);
				break;
			case('o'):
				keyPush(KeyEvent.VK_O);
				break;
			case('p'):
				keyPush(KeyEvent.VK_P);
				break;
			case('q'):
				keyPush(KeyEvent.VK_Q);
				break;
			case('r'):
				keyPush(KeyEvent.VK_R);
				break;
			case('s'):
				keyPush(KeyEvent.VK_S);
				break;
			case('t'):
				keyPush(KeyEvent.VK_T);
				break;
			case('u'):
				keyPush(KeyEvent.VK_U);
				break;
			case('v'):
				keyPush(KeyEvent.VK_V);
				break;
			case('w'):
				keyPush(KeyEvent.VK_W);
				break;
			case('x'):
				keyPush(KeyEvent.VK_X);
				break;
			case('y'):
				keyPush(KeyEvent.VK_Y);
				break;
			case('z'):
				keyPush(KeyEvent.VK_Z);
				break;
			case('�'):
				keyPush(KeyEvent.VK_A);
				break;
			case('�'):
				keyPush(KeyEvent.VK_O);
				break;
			case('�'):
				keyPush(KeyEvent.VK_U);
				break;
				
			//Big Letters
			case('A'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_A);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('B'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_B);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('C'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_C);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('D'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_D);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('E'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_E);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('F'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_F);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('G'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_G);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('H'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_H);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('I'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_I);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('J'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_J);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('K'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_K);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('L'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_L);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('M'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_M);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('N'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_N);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('O'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_O);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('P'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_P);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('Q'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_Q);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('R'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_R);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('S'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_S);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('T'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_T);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('U'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_U);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('V'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_V);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('W'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_W);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('X'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_X);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('Y'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_Y);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('Z'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_Z);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('�'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_A);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('�'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_O);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('�'):
				rob.keyPress(KeyEvent.VK_SHIFT);
				keyPush(KeyEvent.VK_U);
				rob.keyRelease(KeyEvent.VK_SHIFT);
				break;
			
			//Numbers
			case('0'):
				keyPush(KeyEvent.VK_0);
				break;
			case('1'):
				keyPush(KeyEvent.VK_1);
				break;
			case('2'):
				keyPush(KeyEvent.VK_2);
				break;
			case('3'):
				keyPush(KeyEvent.VK_3);
				break;
			case('4'):
				keyPush(KeyEvent.VK_4);
				break;
			case('5'):
				keyPush(KeyEvent.VK_5);
				break;
			case('6'):
				keyPush(KeyEvent.VK_6);
				break;
			case('7'):
				keyPush(KeyEvent.VK_7);
				break;
			case('8'):
				keyPush(KeyEvent.VK_8);
				break;
			case('9'):
				keyPush(KeyEvent.VK_9);
				break;
				
			//Others
			case(' '):
				keyPush(KeyEvent.VK_SPACE);
				break;
			case(','):
				keyPush(KeyEvent.VK_COMMA);
				break;
			case('.'):
				keyPush(KeyEvent.VK_PERIOD);
				break;
			case('-'):
				keyPush(KeyEvent.VK_MINUS);
				break;
			case('_'):
				keyPress(KeyEvent.VK_SHIFT);
				keyPress(KeyEvent.VK_MINUS);
				keyRelease(KeyEvent.VK_MINUS);
				keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('!'):
				keyPress(KeyEvent.VK_SHIFT);
				keyPress(KeyEvent.VK_1);
				keyRelease(KeyEvent.VK_1);
				keyRelease(KeyEvent.VK_SHIFT);
				break;
			case('/'):
				keyPress(KeyEvent.VK_SHIFT);
				keyPress(KeyEvent.VK_7);
				keyRelease(KeyEvent.VK_7);
				keyRelease(KeyEvent.VK_SHIFT);
				break;
			case(':'):
				keyPress(KeyEvent.VK_SHIFT);
				keyPress(KeyEvent.VK_PERIOD);
				keyRelease(KeyEvent.VK_PERIOD);
				keyRelease(KeyEvent.VK_SHIFT);
				break;
			
			
			default:
				keyPush(KeyEvent.VK_SPACE);
				break;
			}
		}
	}
}
