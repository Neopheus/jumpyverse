package Wrapper;


/**
 * This class represents a Point in a 2 dimensional space. There are several methods that will help you a lot working with these Points.
 * Note that some Methods treat the Point as a Vector because both (Point and Vector) have much in common. So a Point can also be seen as a Vector and vice versa.
 * 
 * @author Jakob Schaal aka Brotcrunsher
 *
 */
public class Point implements Cloneable{
	//TODO: Not yet tested!
	
	protected float f_x;
	protected float f_y;
	private float f_length;
	private boolean b_lengthset = false;
	
	public Point(){
		this(0, 0);
	}
	
	public Point(float x, float y){
		f_x = x;
		f_y = y;
		getLength();
	}
	
	private Point(float x, float y, float length, boolean lengthset){
		f_x = x;
		f_y = y;
		f_length = length;
		b_lengthset = lengthset;
	}
	
	
	/**
	 * 
	 * @return The distance to the zero-point
	 */
	public float getLength(){
		if(b_lengthset)return f_length;
		else{
			f_length = (float) Math.sqrt(f_x*f_x+f_y*f_y);
			b_lengthset = true;
			return f_length;
		}
	}
	
	/**
	 * If p is null, p is set to (0 0).
	 * 
	 * @param p The other point.
	 * @return The distance between this point and p.
	 */
	public float getLength(Point p){
		if(p == null)return getLength();
		float newx = f_x-p.getX();
		float newy = f_y-p.getY();
		return (float) Math.sqrt(newx*newx+newy*newy);
	}
	
	/**
	 * 
	 * @param skalar The skalar (number) with which this point will be multiplied.
	 */
	public void mult(float skalar){
		f_x*=skalar;
		f_y*=skalar;
		if(b_lengthset){
			f_length*=skalar;
			if(f_length<0){
				f_length*=-1;
			}
		}
	}
	
	/**
	 * 
	 * @param skalar The skalar (number) with with the x coordinate of this point will be multiplied.
	 */
	public void multX(float skalar){
		f_x*=skalar;
		if(skalar!=1)b_lengthset = false;
	}
	
	/**
	 * 
	 * @param skalar The skalar (number) with with the y coordinate of this point will be multiplied.
	 */
	public void multY(float skalar){
		f_y*=skalar;
		if(skalar!=1)b_lengthset = false;
	}
	
	
	/**
	 * Adds a Point to this Point. If the given Point is null, nothing happens.
	 * 
	 * @param p The Point to add to this Point.
	 */
	public void add(Point p){
		if(p==null)return;
		f_x+=p.getX();
		f_y+=p.getY();
		b_lengthset=false;
	}
	
	/**
	 * Adds a multiple of a Point to this Point. If the given Point is null, nothing happens.
	 * 
	 * @param p The Point to add to this Point.
	 * @param skalar The given point will be multiplied by this value.
	 */
	public void add(Point p, float skalar){
		if(p==null)return;
		f_x+=p.getX()*skalar;
		f_y+=p.getY()*skalar;
		b_lengthset=false;
	}
	
	/**
	 * Adds two Skalars to this Point. The first Skalar represents the x coordinate of a imaginary Point, and the second the y coordinate.
	 * @param x
	 * @param y
	 */
	public void add(float x, float y){
		f_x += x;
		f_y += y;
		b_lengthset=false;
	}
	
	public void addX(float x){
		f_x += x;
		b_lengthset = false;
	}
	
	public void addY(float y){
		f_y += y;
		b_lengthset = false;
	}
	
	/**
	 * Subtracts a Point from this Point. If the given Point is null, nothing happens.
	 * 
	 * @param p The Point to subtract from this Point.
	 */
	public void subtract(Point p){
		if(p==null)return;
		f_x-=p.getX();
		f_y-=p.getY();
		b_lengthset=false;
	}
	
	/**
	 * Subtracts two Skalars to this Point. The first Skalar represents the x coordinate of a imaginary Point, and the second the y coordinate.
	 * @param x
	 * @param y
	 */
	public void subtract(float x, float y){
		f_x -= x;
		f_y -= y;
		b_lengthset=false;
	}
	
	/**
	 * Normalizes this Point (Length is set to 1, angle stays the same).
	 */
	public void normalize(){
		getLength();
		if(f_length==0)throw new IllegalArgumentException("You cant normalize the Zero-Vektor!");
		f_x/=f_length;
		f_y/=f_length;
		b_lengthset=false;
	}
	
	/**
	 * Returns the square Skalar of this Point. If you want the squareroot of this, use toSkalarSqrt
	 * 
	 * @return The square Skalar of this Point.
	 */
	public float toSkalar(){
		return f_x*f_x+f_y*f_y;
	}
	
	/**
	 * Returns the Skalar of this Point. This method uses Math.sqrt, which may be slow. If you dont need the Sqrt of the Skalar, use toSkalar instead.
	 * 
	 * @return The square Skalar of this Point.
	 */
	public float toSkalarSqrt(){
		return (float) Math.sqrt(toSkalar());
	}
	
	
	/**
	 * Rotates this Point around (0 0) by the given angle (in deg)
	 * @param angle The angle to rotate, positive is counter clockwise, negative is clockwise.
	 */
	public void rotate(float angle){
		angle = (float) Math.toRadians(angle);
		float cos = (float)Math.cos(angle);
		float sin = (float)Math.sin(angle);
		
		float newx = f_x*cos-f_y*sin;
		float newy = f_x*sin+f_y*cos;
		
		f_x = newx;
		f_y = newy;
		b_lengthset = false; //The Length should stay the same, however Math.cos and Math.sin may be sketchy.
	}
	
	/**
	 * Rotates this Point around (0 0) by the given precalculated numbers. Warning, this method does not check if the given numbers could be true, so only call this if you know what you are doing!
	 * @param sin The precalculated sinus.
	 * @param cos The precalculated cosinus.
	 */
	public void rotate(float cos, float sin){
		float newx = f_x*cos-f_y*sin;
		float newy = f_x*sin+f_y*cos;
		
		f_x = newx;
		f_y = newy;
		b_lengthset = false; //The Length should stay the same, however Math.cos and Math.sin may be sketchy.
	}
	
	/**
	 * Forces a lengthrecalculation on the next getLength call.
	 */
	public void forceLengthRecalculation(){
		b_lengthset=false;
	}
	
	
	
	
	@Override
	public boolean equals(Object o){
		Point p;
		
		if(o==null)return false;
		
		if(o instanceof Point) p = (Point)o;
		else return false;
		
		if(f_x!=p.getX())return false;
		if(f_y!=p.getY())return false;
		
		
		return true;
	}
	
	@Override
	public Point clone(){
		Point p = new Point(f_x, f_y, f_length, b_lengthset);
		return p;
	}
	
	
	
	/**
	 * 
	 * @return The X-coordinate of this Point.
	 */
	public float getX(){
		return f_x;
	}
	/**
	 * 
	 * @return The Y-coordinate of this Point.
	 */
	public float getY(){
		return f_y;
	}
	
	/**
	 * Changes the X-coordinate of this Point.
	 * 
	 * @param x The new X-coordinate for this Point.
	 */
	public void setX(float x){
		if(x!=f_x)b_lengthset=false;
		f_x = x;
	}
	
	/**
	 * Changes the X and Y-coorinate of this Point.
	 * @param x The new X-coordinate for this Point.
	 * @param y The new Y-coordinate for this Point.
	 */
	public void setXY(float x, float y){
		if(x!=f_x||y!=f_y)b_lengthset=false;
		f_x = x;
		f_y = y;
	}
	
	/**
	 * Changes the X and Y-coordinate of this Point to the same Value as another Point.
	 * @param that The other Point
	 */
	public void setXY(Point that){
		setXY(that.getX(), that.getY());
	}
	
	/**
	 * Changes the Y-coordinate of this Point.
	 * 
	 * @param y The new Y-coordinate for this Point.
	 */
	public void setY(float y){
		if(y!=f_y)b_lengthset=false;
		f_y = y;
	}
	
	/**
	 * Changes the Length of this Point. This may change the X-, and Y-coorinates, if the length is not the same as before.
	 * 
	 * @param length The new Length of this Point.
	 */
	public void setLength(float length){
		getLength();
		if(f_length==0)throw new IllegalArgumentException("You can't set the length of the Zero-Vector!");
		if(length==f_length)return;
		
		normalize();
		mult(length);
		f_length=length;
	}
}
