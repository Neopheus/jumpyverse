package Control;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class Mouse implements MouseListener, MouseMotionListener{
	private static int x;
	private static int y;
	private static boolean isPressed;
	private static boolean isPressedInThisFrame;
	private static boolean isPressedInNextFrame;
	private static int offsetX;
	private static int offsetY;
	
	public static int getX(){
		return x-offsetX;
	}
	public static int getY(){
		return y-offsetY;
	}
	public static boolean isPressed(){
		return isPressed;
	}
	public static boolean isPressedInThisFrame(){
		return isPressedInThisFrame;
	}
	public static void setOffsetX(int newOffset){
		offsetX = newOffset;
	}
	public static void setOffsetY(int newOffset){
		offsetY = newOffset;
	}
	public static int getOffsetX(){
		return offsetX;
	}
	public static int getOffesetY(){
		return offsetY;
	}
	
	public static void reset(){
		isPressedInThisFrame = isPressedInNextFrame;
		isPressedInNextFrame = false;
	}
	

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent me) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		isPressed = true;
		isPressedInThisFrame = true;
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		isPressed = false;
	}

	@Override
	public void mouseDragged(MouseEvent me) {
		if(isPressed){
			x = me.getX();
			y = me.getY();
		}
	}

	@Override
	public void mouseMoved(MouseEvent me) {
		if(!isPressed){
			x = me.getX();
			y = me.getY();
		}
	}

}
