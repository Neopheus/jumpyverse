package Control;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener{
	//TODO Javadoc!
	private static boolean[] keys = new boolean[1024];
	private static boolean[] keysInThisFrame = new boolean[keys.length];
	private static boolean[] keysInNextFrame = new boolean[keys.length];
	
	public static boolean isKeyDown(int keyValue){
		if(keyValue>=0&&keyValue<keys.length) return keys[keyValue]||keysInThisFrame[keyValue];
		else return false;
	}
	
	public static boolean isKeyDownInThisFrame(int keyValue){
		if(keyValue>=0&&keyValue<keys.length) return keysInThisFrame[keyValue];
		else return false;
	}
	
	public static void reset(){
		for(int i = 0; i<keysInThisFrame.length; i++){
			keysInThisFrame[i]=false;
		}
		boolean[] keysStore = keysInThisFrame;
		keysInThisFrame = keysInNextFrame;
		keysInNextFrame = keysStore;
	}

	@Override
	public void keyPressed(KeyEvent g) {
		int keyCode = g.getKeyCode();
		if(keyCode>=0&&keyCode<keys.length){
			keys[keyCode]=true;
			keysInNextFrame[keyCode]=true;
		}
	}

	@Override
	public void keyReleased(KeyEvent g) {
		int keyCode = g.getKeyCode();
		if(keyCode>=0&&keyCode<keys.length)	keys[keyCode]=false;
	}
	
	

	
	
	
	
	//Unused
	@Override
	public void keyTyped(KeyEvent g) {}
}
