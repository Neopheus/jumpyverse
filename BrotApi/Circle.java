package Wrapper;

/**
 * This class represents a Circle with a x-,y-coordiante and a radius. Note that this class does NOT represent an oval!
 * @author JakobSchaal
 *
 */
public class Circle {
	protected final Point pos;
	protected float f_radius;
	
	/**
	 * Creates a new Circle with the center Point at x/y and the given radius.
	 * @param x
	 * @param y
	 * @param radius
	 */
	public Circle(float x, float y, float radius){
		//Tested
		pos = new Point(x, y);
		f_radius = radius;
	}
	
	/**
	 * Creates a new Circle with the given center Point and the given radius.
	 * The Point is CLONED! Changes at the Point will not affect this circle!
	 * @param p
	 * @param radius
	 */
	public Circle(Point p, float radius){
		//Tested
		this(p.getX(), p.getY(), radius);
	}
	
	/**
	 * Returns if this Circle intersects with another Circle.
	 * @param c
	 * @return
	 */
	public boolean intersects(Circle c){
		float absX = pos.getX()-c.getX();
		float absY = pos.getY()-c.getY();
		double abs = absX*absX+absY*absY;
		return (abs<=(this.getRadius()+c.getRadius())*(this.getRadius()+c.getRadius()));
	}
	
	
	@Override
	public boolean equals(Object o){
		//Tested
		Circle c;
		
		if(o==null)return false;
		
		if(o instanceof Circle) c = (Circle)o;
		else return false;
		
		if(pos.getX()!=c.getX())return false;
		if(pos.getY()!=c.getY())return false;
		if(f_radius!=c.getRadius())return false;
		
		
		return true;
	}
	
	@Override
	public Circle clone(){
		//Tested
		Circle c = new Circle(pos.getX(), pos.getY(), f_radius);
		return c;
	}
	
	/**
	 * Tests if this Circle is entirely inside that Circle.
	 * @param that
	 * @return
	 */
	public boolean inside(Circle that){
		float radidiff = that.f_radius - this.f_radius;
		if(radidiff<0)return false;
		
		float diffx = that.getX() - this.getX();
		float diffy = that.getY() - this.getY();
		
		float abssq = diffx*diffx+diffy*diffy;
		
		return radidiff*radidiff>=abssq;
	}
	
	/**
	 * Tests if that Circle is entirely inside this Circle.
	 * @param that
	 * @return
	 */
	public boolean contains(Circle that){
		return that.inside(this);
	}
	
	
	/**
	 * Return the x-coordiante of this Circle.
	 */
	public float getX(){
		//Tested
		return pos.getX();
	}
	/**
	 * Return the y-coordiante of this Circle.
	 */
	public float getY(){
		//Tested
		return pos.getY();
	}
	/**
	 * Returns the radius of this Circle.
	 */
	public float getRadius(){
		//Tested
		return f_radius;
	}
	
	/**
	 * Returns the Point which represents the middle of this Circle. THIS IS NO COPY!
	 */
	public Point getPos(){
		return pos;
	}
	
	/**
	 * Sets the x-coordinate of this Circle.
	 * @param x
	 */
	public void setX(float x){
		//Tested
		pos.setX(x);
	}
	/**
	 * Sets the y-coordiante of this Circle.
	 * @param y
	 */
	public void setY(float y){
		//Tested
		pos.setY(y);
	}
	/**
	 * Sets the radius of this Circle.
	 * @param radius
	 */
	public void setRadius(float radius){
		//Tested
		f_radius = radius;
	}
}
