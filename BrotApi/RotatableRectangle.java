package Wrapper;

public class RotatableRectangle {
	//TODO Javadoc!!
	//TODO Test!!
	private static boolean autoRotate = true;
	
	private float[] unrotatedx = new float[4];
	private float[] unrotatedy = new float[4];
	private int[] rotatedx = new int[4];
	private int[] rotatedy = new int[4];
	private float alpha;
	private float centerx = 0;
	private float centery = 0;
	
	private float x;
	private float y; 
	private float width;
	private float height;
	
	public RotatableRectangle(float x, float y, float width, float height, float alpha){
		this.alpha = alpha;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		
		unrotatedx[0] = x;
		unrotatedx[1] = x;
		unrotatedx[2] = x+width;
		unrotatedx[3] = x+width;
		
		unrotatedy[0] = y;
		unrotatedy[1] = y+height;
		unrotatedy[2] = y+height;
		unrotatedy[3] = y;
		
		centerx = x+width/2;
		centery = y+height/2;
		
		rotate();
	}
	
	public RotatableRectangle(float x, float y, float width, float height){
		this(x, y, width, height, 0);
	}
	
	
	public boolean intersects(RotatableRectangle that){
		return intersects(that, true);
	}
	
	private boolean intersects(RotatableRectangle that, boolean repeat){
		//Absichtlich keine Arrays, damit der Heap nicht zugem�llt wird und der GC weniger zu tun hat.
		//H�sslich aber Performance und so...
		float thisx0 = this.unrotatedx[0]-this.centerx;
		float thisx1 = this.unrotatedx[1]-this.centerx;
		float thisx2 = this.unrotatedx[2]-this.centerx;
		float thisx3 = this.unrotatedx[3]-this.centerx;
		float thisy0 = this.unrotatedy[0]-this.centery;
		float thisy1 = this.unrotatedy[1]-this.centery;
		float thisy2 = this.unrotatedy[2]-this.centery;
		float thisy3 = this.unrotatedy[3]-this.centery;
		
		float thatx0Orginal = that.unrotatedx[0]-that.centerx;
		float thatx1Orginal = that.unrotatedx[1]-that.centerx;
		float thatx2Orginal = that.unrotatedx[2]-that.centerx;
		float thatx3Orginal = that.unrotatedx[3]-that.centerx;
		float thaty0Orginal = that.unrotatedy[0]-that.centery;
		float thaty1Orginal = that.unrotatedy[1]-that.centery;
		float thaty2Orginal = that.unrotatedy[2]-that.centery;
		float thaty3Orginal = that.unrotatedy[3]-that.centery;
		
		float alpha = this.alpha - that.alpha;
		
		float sin = (float)Math.sin(alpha);
		float cos = (float)Math.cos(alpha);
		
		float thatx0 = cos*thatx0Orginal - sin*thaty0Orginal + that.centerx - this.centerx;
		float thatx1 = cos*thatx1Orginal - sin*thaty1Orginal + that.centerx - this.centerx;
		float thatx2 = cos*thatx2Orginal - sin*thaty2Orginal + that.centerx - this.centerx;
		float thatx3 = cos*thatx3Orginal - sin*thaty3Orginal + that.centerx - this.centerx;
		float thaty0 = sin*thatx0Orginal + cos*thaty0Orginal + that.centery - this.centery;
		float thaty1 = sin*thatx1Orginal + cos*thaty1Orginal + that.centery - this.centery;
		float thaty2 = sin*thatx2Orginal + cos*thaty2Orginal + that.centery - this.centery;
		float thaty3 = sin*thatx3Orginal + cos*thaty3Orginal + that.centery - this.centery;
		
		
		System.out.println(thatx0 + " " + thaty0);
		System.out.println(thatx1 + " " + thaty1);
		System.out.println(thatx2 + " " + thaty2);
		System.out.println(thatx3 + " " + thaty3);
		System.out.println(alpha);
		System.out.println();
		
		
		if(thatx0>=thisx0 && thatx0<=thisx3 && thaty0>=thisy0 && thaty0<=thisy1) return true;
		if(thatx1>=thisx0 && thatx1<=thisx3 && thaty1>=thisy0 && thaty1<=thisy1) return true;
		if(thatx2>=thisx0 && thatx2<=thisx3 && thaty2>=thisy0 && thaty2<=thisy1) return true;
		if(thatx3>=thisx0 && thatx3<=thisx3 && thaty3>=thisy0 && thaty3<=thisy1) return true;
		
		if(repeat)return that.intersects(this, false);
		
		return false;
	}
	
	public int[] getNorotX(){
		int[] returner = new int[4];
		for(int i = 0; i<4; i++){
			returner[i] = (int)unrotatedx[i];
		}
		return returner;
	}
	public int[] getNorotY(){
		int[] returner = new int[4];
		for(int i = 0; i<4; i++){
			returner[i] = (int)unrotatedy[i];
		}
		return returner;
	}
	public int[] getPolyX(){
		return rotatedx;
	}
	public int[] getPolyY(){
		return rotatedy;
	}
	public float getCenterX(){
		return centerx;
	}
	public float getCenterY(){
		return centery;
	}
	
	public void addX(float x){
		setX(this.x+x);
	}
	public void addY(float y){
		setY(this.y+y);
	}
	public void addAlpha(float alpha){
		setAlpha(this.alpha+alpha);
	}
	
	
	
	public void setX(float x){
		this.x = x;
		unrotatedx[0] = x;
		unrotatedx[1] = x;
		unrotatedx[2] = x+width;
		unrotatedx[3] = x+width;
		centerx = x+width/2;
		if(autoRotate) rotate();
	}
	public void setY(float y){
		this.y = y;
		unrotatedy[0] = y;
		unrotatedy[1] = y+height;
		unrotatedy[2] = y+height;
		unrotatedy[3] = y;
		centery = y+height/2;
		if(autoRotate) rotate();
	}
	public void setWidth(float width){
		this.width = width;
		unrotatedx[2] = x+width;
		unrotatedx[3] = x+width;
		centerx = x+width/2;
		if(autoRotate) rotate();
	}
	public void setHeight(float height){
		this.height = height;
		unrotatedy[1] = y+height;
		unrotatedy[2] = y+height;
		centery = y+height/2;
		if(autoRotate) rotate();
	}
	public void setAlpha(float alpha){
		this.alpha = (float) (alpha%(Math.PI*2));
		if(autoRotate) rotate();
	}
	public static void setAutoRotate(boolean autoRotate){
		RotatableRectangle.autoRotate = autoRotate;
	}
	
	public static boolean getAutoRotate(){
		return autoRotate;
	}
	public float getX(){
		return x;
	}
	public float getY(){
		return y;
	}
	public float getWidth(){
		return width;
	}
	public float getHeight(){
		return height;
	}
	public float getAlpha(){
		return alpha;
	}
	
	
	public void rotate(){
		float sin = (float)Math.sin(alpha);
		float cos = (float)Math.cos(alpha);
		
		for(int i = 0; i<unrotatedx.length; i++){
			unrotatedx[i]-=centerx;
			unrotatedy[i]-=centery;
			rotatedx[i] = (int) (unrotatedx[i]*cos - unrotatedy[i]*sin);
			rotatedy[i] = (int) (unrotatedx[i]*sin + unrotatedy[i]*cos);
			rotatedx[i]+=centerx;
			rotatedy[i]+=centery;
			unrotatedx[i]+=centerx;
			unrotatedy[i]+=centery;
		}
	}
}
