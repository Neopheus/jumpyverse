package Media;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

public class LoopSoundWithIntro implements Runnable{
	private byte[] introSamples;
	private byte[] introBuffer;
	private AudioFormat introFormat;
	private InputStream introIstream;
	private int introBufferSize = 0;
	private DataLine.Info introInfo;
	private SourceDataLine introLine = null;
	private int introNumBytesRead;
	
	private byte[] loopSamples;
	private byte[] loopBuffer;
	private AudioFormat loopFormat;
	private InputStream loopIstream;
	private int loopBufferSize = 0;
	private DataLine.Info loopInfo;
	private SourceDataLine loopLine = null;
	private int loopNumBytesRead;
	
	
	private volatile boolean playing = false;
	private Thread thread;
	
	public LoopSoundWithIntro(String pathintro, String pathloop){
		try {
			AudioInputStream introAiStream = null;
			introAiStream = AudioSystem.getAudioInputStream(new File(pathintro));
			introFormat = introAiStream.getFormat();
			introSamples = new byte[(int)(introFormat.getFrameSize()*introAiStream.getFrameLength())];
			DataInputStream introIs = new DataInputStream(introAiStream);
			introIs.readFully(introSamples);
			introBufferSize = introFormat.getFrameSize() * Math.round(introFormat.getSampleRate()/10);
			
			
			AudioInputStream loopAiStream = null;
			loopAiStream = AudioSystem.getAudioInputStream(new File(pathloop));
			loopFormat = loopAiStream.getFormat();
			loopSamples = new byte[(int)(loopFormat.getFrameSize()*loopAiStream.getFrameLength())];
			DataInputStream loopIs = new DataInputStream(loopAiStream);
			loopIs.readFully(loopSamples);
			loopBufferSize = loopFormat.getFrameSize() * Math.round(loopFormat.getSampleRate()/10);
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void play(){
		playing = true;
		thread = new Thread(this);
		thread.start();
	}
	
	public void stop(){
		playing = false;
	}
	
	public void start(){
		play();
	}
	
	public void restart(){
		stop();
		start();
	}
	public synchronized boolean isPlaying(){
		return playing;
	}



	@Override
	public void run() {
		introIstream = new ByteArrayInputStream(introSamples);
		introBuffer = new byte[introBufferSize];
		introInfo = new DataLine.Info(SourceDataLine.class, introFormat);

		try {
			introLine = (SourceDataLine)AudioSystem.getLine(introInfo);
			introLine.open(introFormat, introBufferSize);
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		introLine.start();
		
		introNumBytesRead = 0;
		
		
		
		loopIstream = new ByteArrayInputStream(loopSamples);
		loopBuffer = new byte[loopBufferSize];
		loopInfo = new DataLine.Info(SourceDataLine.class, loopFormat);

		try {
			loopLine = (SourceDataLine)AudioSystem.getLine(loopInfo);
			loopLine.open(loopFormat, loopBufferSize);
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		loopLine.start();
		
		loopNumBytesRead = 0;
		
		try{
			while(introNumBytesRead != -1 && isPlaying()){
				introNumBytesRead = introIstream.read(introBuffer, 0, introBuffer.length);
				if(introNumBytesRead != -1){
					introLine.write(introBuffer, 0, introNumBytesRead);
				}
			}
			introLine.close();
			
			while(isPlaying()){
				while(loopNumBytesRead != -1 && isPlaying()){
					loopNumBytesRead = loopIstream.read(loopBuffer, 0, loopBuffer.length);
					if(loopNumBytesRead != -1){
						loopLine.write(loopBuffer, 0, loopNumBytesRead);
					}
				}
				loopNumBytesRead = 0;
				loopIstream.reset();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		introLine.close();
		loopLine.close();
		
		playing = false;
	}
}
