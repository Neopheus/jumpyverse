package Media;

import java.awt.image.BufferedImage;
import java.util.Arrays;

public class Animation {
	// TODO Not yet Tested.
	// TODO Javadoc! NooB!
	private final float fNEEDEDANIMATIONTIME;
	private float fAnimationTime = 0;
	private BufferedImage[] frames;
	private final float[] afDURATIONS;
	private final boolean USEDURATIONS;

	public Animation(float neededAnimationTime, BufferedImage[] frames) {
		this.fNEEDEDANIMATIONTIME = neededAnimationTime;
		this.frames = frames;
		USEDURATIONS = false;
		afDURATIONS = null;
	}

	public Animation(float[] durations, BufferedImage[] frames) {
		if (durations.length != frames.length)
			throw new IllegalArgumentException(
					"durations and frames must have the same length");
		this.afDURATIONS = Arrays.copyOf(durations, durations.length);
		for (int i = 0; i < durations.length; i++) {
			for (int k = 0; k < i; k++) {
				this.afDURATIONS[i] += durations[k];
			}
		}
		fNEEDEDANIMATIONTIME = this.afDURATIONS[durations.length - 1];
		System.out.println(fNEEDEDANIMATIONTIME);
		this.frames = frames;
		USEDURATIONS = true;
	}

	public void update(float timeSinceLastFrame) {
		fAnimationTime += timeSinceLastFrame;
		if (fAnimationTime >= fNEEDEDANIMATIONTIME) {
			fAnimationTime -= fNEEDEDANIMATIONTIME;
		}
	}

	public BufferedImage getCurrentFrame() {
		if (frames.length == 0)
			return null;
		if (!USEDURATIONS) {
			for (int i = 0; i < frames.length; i++) {
				if (fAnimationTime < (float) (fNEEDEDANIMATIONTIME
						/ frames.length * (i + 1))) {
					return frames[i];
				}
			}
			return frames[frames.length - 1];
		} else {
			for (int i = 0; i < frames.length; i++) {
				if (fAnimationTime < afDURATIONS[i]) {
					return frames[i];
				}
			}
			return frames[frames.length - 1];
		}
	}

	public float getAnimationTime() {
		return fAnimationTime;
	}

	public float getNeededAnimationTime() {
		return fNEEDEDANIMATIONTIME;
	}

	public BufferedImage[] getImages() {
		return frames;
	}

	public BufferedImage getImage(int i) {
		return frames[i];
	}

	public void setAnimationTime(float animationTime) {
		this.fAnimationTime = animationTime;
	}

	public void setNeededAnimationTime(float neededAnimationTime) {
		this.fAnimationTime = neededAnimationTime;
	}

	public void setImages(BufferedImage[] images) {
		frames = images;
	}

	public void setImage(BufferedImage image, int i) {
		frames[i] = image;
	}
}
