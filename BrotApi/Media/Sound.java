package Media;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Sound implements Runnable{
	private byte[] samples;
	private byte[] buffer;
	private AudioFormat format;
	private boolean looping = false;
	private volatile boolean playing = false;
	private Thread thread;
	private InputStream istream;
	private int bufferSize = 0;
	private DataLine.Info info;
	private SourceDataLine line = null;
	private int numBytesRead;
	
	public Sound(String path){
		try {
			AudioInputStream aiStream = null;
			aiStream = AudioSystem.getAudioInputStream(new File(path));
			format = aiStream.getFormat();
			samples = new byte[(int)(format.getFrameSize()*aiStream.getFrameLength())];
			DataInputStream is = new DataInputStream(aiStream);
			is.readFully(samples);
			bufferSize = format.getFrameSize() * Math.round(format.getSampleRate()/10);
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void play(){
		playing = true;
		thread = new Thread(this);
		thread.start();
	}
	
	public void stop(){
		playing = false;
	}
	
	public void start(){
		play();
	}
	
	public void restart(){
		stop();
		start();
	}
	public void setLooping(boolean looping){
		this.looping = looping;
	}
	
	public boolean isLooping(){
		return looping;
	}
	
	public synchronized boolean isPlaying(){
		return playing;
	}



	@Override
	public void run() {
		istream = new ByteArrayInputStream(samples);
		buffer = new byte[bufferSize];
		info = new DataLine.Info(SourceDataLine.class, format);

		try {
			line = (SourceDataLine)AudioSystem.getLine(info);
			line.open(format, bufferSize);
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		line.start();
		
		numBytesRead = 0;
		
		try{
			do{
				while(numBytesRead != -1 && isPlaying()){
					numBytesRead = istream.read(buffer, 0, buffer.length);
					if(numBytesRead != -1){
						line.write(buffer, 0, numBytesRead);
					}
				}
				numBytesRead = 0;
				istream.reset();
			}while(looping);
		} catch (IOException e) {
			e.printStackTrace();
		}
		

		line.close();
		
		if(looping&&isPlaying()){
			restart();
		}
		playing = false;
	}
	
}
