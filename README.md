# README #

Group project as part of the media informatics course at the "Hochschule der Medien" (Media University).

### About the game ###

Jumpyverse is a level editor for the corresponding Jump n' Run sidescoller game also included in it.

### About the implementation ###

It's written in Java and uses the Lightweight Java Game Library (LWJGL) and Swing.