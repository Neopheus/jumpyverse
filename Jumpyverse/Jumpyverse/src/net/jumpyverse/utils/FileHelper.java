package net.jumpyverse.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipInputStream;

public class FileHelper {
	public static void unZipIt(final String zipFile) {
		/*
		 * Unzips a file, so you can load it normally. 
		 * The unzipped file will be named like the orginal File, with the exact
		 * same location. Only difference is .temp at the end.
		 * DONT FORGET TO DELETE THIS UNZIPPED FILE!!!
		 */
		byte[] byte_buffer = new byte[1024];

		try {
			ZipInputStream zis = new ZipInputStream(
					new FileInputStream(zipFile));
			zis.getNextEntry();
			
			File newFile = new File(zipFile+".temp");

			FileOutputStream fos = new FileOutputStream(newFile);

			int len;
			while ((len = zis.read(byte_buffer)) > 0) {
				fos.write(byte_buffer, 0, len);
			}

			fos.close();

			zis.closeEntry();
			zis.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

}
