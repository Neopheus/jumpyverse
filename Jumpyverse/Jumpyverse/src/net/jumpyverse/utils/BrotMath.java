package net.jumpyverse.utils;

public class BrotMath {
	public static float modulo(final float value, final float div){
		//The normal % operator does not work like it does in math, at least with negative numbers.
		if(value>=0)return value%div;
		else{
			return ((value % div) + div ) % div;
		}
	}
}
