package net.jumpyverse.utils;

import java.nio.ByteBuffer;

public class ByteReader {
	private int pointer = 0;
	private final byte[] bytes;
	
	public ByteReader(final byte[] bytes){
		this.bytes = bytes;
		pointer = 0;
	}
	
	public byte popByte(){
		byte b = bytes[pointer];
		pointer++;
		return b;
	}
	
	public byte[] popBytes(final int length){
		byte[] bytes = new byte[length];
		for(int i = 0; i<length; i++){
			bytes[i] = popByte();
		}
		return bytes;
	}
	
	public short popShort(){
		byte[] bShort = {bytes[pointer++], bytes[pointer++]};
		return ByteBuffer.wrap(bShort).getShort();
	}
	
	public int popInt(){
		byte[] bInt = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
		return ByteBuffer.wrap(bInt).getInt();
	}
	
	public long popLong(){
		byte[] bLong = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
		return ByteBuffer.wrap(bLong).getLong();
	}
	
	public float popFloat(){
		byte[] bFloat = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
		return ByteBuffer.wrap(bFloat).getFloat();
	}
	
	public double popDouble(){
		byte[] bDouble = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
		return ByteBuffer.wrap(bDouble).getDouble();
	}
	
	public byte[] getBytes(final int length){
		byte[] local_bytes = new byte[length];
		for(int i = 0; i<local_bytes.length; i++){
			local_bytes[i] = bytes[i + pointer];
		}
		pointer += length;
		return local_bytes;
	}
	
	public String getString(final int length){
		return new String(getBytes(length));
	}
}
