package net.jumpyverse.utils;

public class ByteHelper {
	public static byte[] mergeBytes(final byte[]... bytes){
		int size = 0;
		int pointer = 0;
		byte[] returnValue = null;
		
		for(int i = 0; i < bytes.length; i++){
			size += bytes[i].length;
		}
		returnValue = new byte[size];
		for(int i = 0; i<bytes.length; i++){
			for(int k = 0; k<bytes[i].length; k++){
				returnValue[pointer] = bytes[i][k];
				pointer++;
			}
		}
		
		return returnValue;
	}
}
