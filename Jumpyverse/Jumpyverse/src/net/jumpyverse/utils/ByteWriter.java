package net.jumpyverse.utils;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class ByteWriter {
	private List<Byte> bytes = new ArrayList<Byte>();
	
	public ByteWriter(){}
	
	public void putByte(final byte b){
		writeByteArrayToList(ByteBuffer.allocate(1).put(b).array());
	}
	
	public void putBytes(final byte[] b){
		for(int i = 0; i<b.length; i++){
			putByte(b[i]);
		}
	}
	
	public void putShort(final short s){
		writeByteArrayToList(ByteBuffer.allocate(2).putShort(s).array());
	}
	
	public void putInt(final int integer){
		writeByteArrayToList(ByteBuffer.allocate(4).putInt(integer).array());
	}
	
	public void putLong(final long l){
		writeByteArrayToList(ByteBuffer.allocate(8).putLong(l).array());
	}
	
	public void putFloat(final float f){
		writeByteArrayToList(ByteBuffer.allocate(4).putFloat(f).array());
	}
	
	public void putDouble(final double d){
		writeByteArrayToList(ByteBuffer.allocate(8).putDouble(d).array());
	}
	
	public void putChar(final char c){
		writeByteArrayToList(ByteBuffer.allocate(8).putChar(c).array());
	}
	
	
	
	
	
	public byte[] getBytes(){
		byte[] byteArray = new byte[bytes.size()];
		for(int i = 0; i<byteArray.length; i++){
			byteArray[i] = bytes.get(i);
		}
		return byteArray;
	}
	
	public void writeByteArrayToList(final byte[] bytearray){
		for(int i = 0; i<bytearray.length; i++){
			bytes.add(bytearray[i]);
		}
	}
}
