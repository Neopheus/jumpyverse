package net.jumpyverse.game;

import static net.jumpyverse.editor.Session.*;

import java.io.IOException;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.LWJGLCore.Screen;
import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.BackgroundType;
import net.jumpyverse.entities.BlockType;
import net.jumpyverse.entities.DecoObject;
import net.jumpyverse.entities.EnemyObject;
import net.jumpyverse.entities.Walker;
import net.jumpyverse.entities.interactives.InteractiveObject;
import net.jumpyverse.enums.ClickMode;
import net.jumpyverse.enums.Shape;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Renderer {
	private static Texture fillerIcon;
	private static float scrollBarTransparency = 0;
	
	public static void startup(){
		try {
			fillerIcon = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resgui/fillerIcon.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void draw() {
		if(tempLevel.getBackgroundImage()==0){
			Brush.setColor(0.5f, 0.5f, 1, 1);
			Brush.fillRect(0, 0, Screen.width, Screen.height);
		}
		else{
			Brush.setColor(1, 1, 1, 1);
			BackgroundType.draw(tempLevel.getBackgroundImage());
		}
		
		
		
		drawGround(tempLevel, LevelData.BACKGROUND);
		drawGround(tempLevel, LevelData.PLAYGROUND);
		
		if(Session.viewMode == Session.VIEWMODE_EDITOR && ClickMode.currentClickMode != ClickMode.deco){
			Brush.setColor(1, 1, 1, 0.3f);
		}
		for(int i = 0; i<tempLevel.getDecoSize(); i++){
			tempLevel.getDeco(i).draw();
		}
		Brush.setColor(1, 1, 1, 1);
		for(int i = 0; i<tempLevel.getInterSize(); i++){
			if(Session.viewMode == Session.VIEWMODE_EDITOR && ClickMode.currentClickMode != ClickMode.interactive){
				Brush.setColor(1, 1, 1, 0.3f);
			}else{
				Brush.setColor(1, 1, 1, 1);
			}
			InteractiveObject io = tempLevel.getInter(i);
			if(io.doesNeedADraw())io.draw();
		}
		Brush.setColor(1, 1, 1, 1);
		if(Session.viewMode == Session.VIEWMODE_EDITOR && ClickMode.currentClickMode != ClickMode.enemy){
			Brush.setColor(1, 1, 1, 0.3f);
		}
		for(int i = 0; i<tempLevel.getEnemySize(); i++){
			EnemyObject eo = tempLevel.getEnemy(i);
			if(eo.doesNeedADraw()) eo.draw();
		}
		tempLevel.getPlayer().draw();
		drawGround(tempLevel, LevelData.FOREGROUND);
		
		
		
		if(Session.viewMode == Session.VIEWMODE_EDITOR){
			Brush.setColor(0.4f, 0.4f, 0.4f, 0.8f);
			if(Session.selectedInteractive!=null && ClickMode.currentClickMode == ClickMode.interactive){
				InteractiveObject io = Session.selectedInteractive;
				Brush.fillRect(io.getX()-camX, io.getY()-camY, io.getWidth(), io.getHeight());
			}
			
			if(Session.selectedDeco!=null && ClickMode.currentClickMode == ClickMode.deco){
				DecoObject eo = Session.selectedDeco;
				Brush.fillRect(eo.getX()-camX, eo.getY()-camY, 32, 32);
			}
			
			if(Session.selectedEnemy!=null && ClickMode.currentClickMode == ClickMode.enemy){
				EnemyObject eo = Session.selectedEnemy;
				Brush.fillRect(eo.getX()-camX, eo.getY()-camY, eo.getWidth(), eo.getHeight());
			}
			
			Brush.setColor(1, 1, 1, 0.2f);
			if(ClickMode.currentClickMode != ClickMode.block){
				Brush.fillRect(((mouseTileX * LevelData.TILESIZE - camX)) % Screen.width, ((mouseTileY * LevelData.TILESIZE - camY)) % Screen.height, LevelData.TILESIZE, LevelData.TILESIZE);
			}
			else{
				for(int i = mouseTileX+1-brushSize; i<mouseTileX+brushSize; i++){
					for(int k = mouseTileY+1-brushSize; k<mouseTileY+brushSize; k++){
						if(brushShape == Shape.axisbox){
							Brush.fillRect(((i * LevelData.TILESIZE - camX)), ((k * LevelData.TILESIZE - camY)), LevelData.TILESIZE, LevelData.TILESIZE);
						}else if(brushShape == Shape.diagonalbox){
							if(Math.abs(i-mouseTileX)+Math.abs(k-mouseTileY) < brushSize){
								Brush.fillRect(((i * LevelData.TILESIZE - camX)), ((k * LevelData.TILESIZE - camY)), LevelData.TILESIZE, LevelData.TILESIZE);
							}
						}else if(brushShape == Shape.circle){
							int value = Math.abs(i-mouseTileX)*Math.abs(i-mouseTileX)+Math.abs(k-mouseTileY)*Math.abs(k-mouseTileY);
							if(value < brushSize * brushSize){
								Brush.fillRect(((i * LevelData.TILESIZE - camX)), ((k * LevelData.TILESIZE - camY)), LevelData.TILESIZE, LevelData.TILESIZE);
							}
						}
					}
				}
			}
			
			Brush.setColor(0.5f, 0.5f, 0.5f, 0.8f);
			for (int i = 0; i < Screen.height; i += LevelData.TILESIZE) {
				float y = (i - camY) % Screen.height;
				if (y < 0)
					y += Screen.height;
				Brush.drawLine(0, y, Screen.width, y);
			}

			for (int i = 0; i < Screen.width; i += LevelData.TILESIZE) {
				float x = (i - camX) % Screen.width;
				if (x < 0)
					x += Screen.width;
				Brush.drawLine(x, 0, x, Screen.height);
			}
			
			if(Session.fixMousePointX!=-10000&&Session.fixMousePointY!=-10000){
				Brush.setColor(1, 1, 0, 1);
				Brush.fillRect(Session.fixMousePointX-5, Session.fixMousePointY-5, 10, 10);
			}
			
			
			if(ClickMode.currentClickMode == ClickMode.block || ClickMode.currentClickMode == ClickMode.fill){
				Brush.setColor(0, 0, 0, 1);
				Brush.fillRect(Session.mouseX+20-2, Session.mouseY+30-2, 12, 12);
				Brush.setColor(1, 1, 1, 1);
				Brush.drawMiniRepresentation(BlockType.getSpriteSheet((short)(Session.leftClickTileIDBlock*16)).getTextureID(), Session.mouseX+20, Session.mouseY+30);
				if(ClickMode.currentClickMode == ClickMode.fill) Brush.drawImage(fillerIcon, Session.mouseX+20+18, Session.mouseY+30, 16, 16);
			}
			if(ClickMode.currentClickMode == ClickMode.deco){
				Brush.setColor(1, 1, 1, 1);
				DecoObject.draw(Session.mouseX, Session.mouseY, Session.leftClickTileIDDeco);
			}
			if(ClickMode.currentClickMode == ClickMode.enemy){
				Brush.setColor(1, 1, 1, 1);
				if(Session.leftClickTileIDEnemy == Walker.mainID){
					int x = (int) (mouseX-17);
					int y = (int) (mouseY-64+9);
					if(snapToGrid){
						x /= LevelData.TILESIZE;
						y /= LevelData.TILESIZE;
						
						x++;
						y++;
						
						x *= LevelData.TILESIZE;
						y *= LevelData.TILESIZE;
						
					}
					Brush.drawImage(Walker.animation_tex_standing1.getTextureID(), x, y, 64, 128);
				}
			}
			
			if(timeSinceStart < 3){
				logoAcceleration += timeSinceLastFrame*90;
				logoRotation += logoAcceleration*timeSinceLastFrame;
				
				Brush.setColor(1, 1, 1, 3 - timeSinceStart); //2 Sec sichtbar, dann fade out
				Brush.drawImage(logoID, Screen.width/2-200, Screen.height/2-200, 400, 400, logoRotation);
				Brush.setColor(1, 0, 0, 3 - timeSinceStart);
				String dots = "";
				float numOfDots = timeSinceStart - (int)timeSinceStart;	//liefert nachkommastellen
				if(numOfDots<1/4f){
					dots = "";
				}else if(numOfDots<2/4f){
					dots = ".";
				}else if(numOfDots<3/4f){
					dots = "..";
				}else{
					dots = "...";
				}
				Brush.drawString("Loading"+dots, "visitor", 300, Screen.height-64, 64, 64);
				Brush.drawString("Jumpyverse", "visitor", 200, 10, 64, 64);
			}
			
			Brush.setColor(1, 0, 0, 1);
			
			if(Session.showMousePosition){
				Brush.drawString("Mouse Tile X = "+Session.mouseTileX, "visitor", 5, 5, 10, 10);
				Brush.drawString("Mouse Tile Y = "+Session.mouseTileY, "visitor", 5, 16, 10, 10);
			}
			
			if(Session.camDeltaX != 0 || Session.camDeltaY != 0){
				scrollBarTransparency = 3;
			}else{
				scrollBarTransparency -= timeSinceLastFrame * 2;
			}
			
			if(scrollBarTransparency > 0){
				int scrollBarWidth = (int) ((Screen.width/LevelData.TILESIZE / 32f) * LevelData.TILESIZE * Screen.width / Session.tempLevel.getWidth());
				int scrollBarHeight = 10;
				float scrollBarX = (float) (Session.camX / (Session.tempLevel.getWidth() * LevelData.TILESIZE)) * Screen.width;
				Brush.setColor(0, 0, 0, scrollBarTransparency);
				Brush.drawLine(0, Screen.height-scrollBarHeight-1, Screen.width-10, Screen.height-scrollBarHeight);
				Brush.setColor(1, 0.6f, 0, scrollBarTransparency);
				Brush.fillRect(scrollBarX, Screen.height-scrollBarHeight-1, scrollBarWidth, scrollBarHeight);
				Brush.setColor(0, 0, 0, scrollBarTransparency);
				Brush.drawRect(scrollBarX, Screen.height-scrollBarHeight-1, scrollBarWidth, scrollBarHeight);
				
				scrollBarWidth = 10;
				scrollBarHeight = (int) ((Screen.height/LevelData.TILESIZE / 32f) * LevelData.TILESIZE * Screen.height / Session.tempLevel.getHeight());
				float scrollBarY = (float) (Session.camY / (Session.tempLevel.getHeight() * LevelData.TILESIZE)) * Screen.height;
				Brush.setColor(0, 0, 0, scrollBarTransparency);
				Brush.drawLine(Screen.width-scrollBarWidth, 0, Screen.width-scrollBarWidth, Screen.height-10);
				Brush.setColor(1, 0.6f, 0, scrollBarTransparency);
				Brush.fillRect(Screen.width-scrollBarWidth, scrollBarY, scrollBarWidth, scrollBarHeight);
				Brush.setColor(0, 0, 0, scrollBarTransparency);
				Brush.drawRect(Screen.width-scrollBarWidth, scrollBarY, scrollBarWidth, scrollBarHeight);
			}
		}
		
		
		if(Session.viewMode == Session.VIEWMODE_GAME){
			tempLevel.getParticle().draw();
			
			
			int scorePosX = Screen.width-200;
			int scorePosY = 5;
			
			int shadowOffsetX = 1;
			int shadowOffsetY = 1;
			
			Brush.setColor(0, 0, 0, 1);
			Brush.drawString(String.format("Score: %6d", Session.tempLevel.getPlayer().getPoints()), "visitor", scorePosX+shadowOffsetX, scorePosY+shadowOffsetY, 10, 10);
			Brush.setColor(0.8f, 0.8f, 0.8f, 1);
			Brush.drawString(String.format("Score: %6d", Session.tempLevel.getPlayer().getPoints()), "visitor", scorePosX, scorePosY, 10, 10);
		}
		
		
	}
	
	
	
	private static void drawGround(final LevelData leveldata, final int groundID){
		Brush.setColor(1, 1, 1, 1);
		if(Session.viewMode == Session.VIEWMODE_EDITOR && Session.activeGround!=groundID){
			Brush.setColor(1, 1, 1, 0.3f);
		}
		if(Session.viewMode == Session.VIEWMODE_GAME){
			switch(groundID){
			case(LevelData.BACKGROUND):
				Brush.setColor(0.9f, 0.9f, 1, 1);
				break;
			case(LevelData.PLAYGROUND):
				Brush.setColor(1, 1, 1, 1);
				break;
			case(LevelData.FOREGROUND):
				Brush.setColor(0.7f, 0.7f, 0.7f, 1);
				break;
			}
		}
		for (int i = (int) (camX / LevelData.TILESIZE) - 1 ;   i < (camX+Screen.width)/LevelData.TILESIZE + 1; i++) {
			for (int k = (int) (camY / LevelData.TILESIZE) - 1; k < (camY+Screen.height)/LevelData.TILESIZE + 1; k++) {
				short tileID = (short) (leveldata.getTile(i, k, groundID));
				if (tileID != 0) {
					int spritenumber = 0;
							
					
					if(BlockType.hasSameMainID(tileID, leveldata.getTile(i-1, k, groundID))){
						spritenumber+=1;
					}
					if(BlockType.hasSameMainID(tileID, leveldata.getTile(i, k+1, groundID))){
						spritenumber+=2;
					}
					if(BlockType.hasSameMainID(tileID, leveldata.getTile(i+1, k, groundID))){
						spritenumber+=4;
					}
					if(BlockType.hasSameMainID(tileID, leveldata.getTile(i, k-1, groundID))){
						spritenumber+=8;
					}
					
					Brush.drawSprite(BlockType.getSpriteSheet(tileID), i * LevelData.TILESIZE - camX, k * LevelData.TILESIZE - camY, spritenumber);
				}
			}
		}
	}
}
