package net.jumpyverse.enums;

public enum Animations {
	standing, walking, running, stopping, jumping, landing, takenDamage, dead;
}
