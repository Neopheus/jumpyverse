package net.jumpyverse.enums;

public enum ClickMode {
	block, deco, player, fill, interactive, enemy;
	
	
	public static ClickMode currentClickMode = block;
}
