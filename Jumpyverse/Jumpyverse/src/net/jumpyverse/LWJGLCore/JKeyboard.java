package net.jumpyverse.LWJGLCore;

import static org.lwjgl.input.Keyboard.*;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import org.lwjgl.input.Keyboard;

public class JKeyboard implements KeyListener{

	private static boolean[] keysInThisFrame;
	private static boolean[] keysInNextFrame;
	
	public JKeyboard(){
		final int size = 1024;
		keysInThisFrame = new boolean[size];
		keysInNextFrame = new boolean[size];
		System.out.println("key");
	}
	
	public static boolean isKeyDown(final int key){
		if(key>=0&&key<keysInThisFrame.length) return keysInThisFrame[key];
		else return false;
	}
	
	public static boolean wasKeyDown(final int key){
		if(key>=0&&key<keysInNextFrame.length) return keysInNextFrame[key];
		else return false;
	}
	

	public static void poll() {
		for(int i = 0; i<keysInThisFrame.length; i++){
			keysInThisFrame[i]=false;
		}
		
		boolean[] keysStore = keysInThisFrame;
		keysInThisFrame = keysInNextFrame;
		keysInNextFrame = keysStore;
		
		checkKey(KEY_0);
		checkKey(KEY_1);
		checkKey(KEY_2);
		checkKey(KEY_3);
		checkKey(KEY_4);
		checkKey(KEY_5);
		checkKey(KEY_6);
		checkKey(KEY_7);
		checkKey(KEY_8);
		checkKey(KEY_9);
		checkKey(KEY_A);
		checkKey(KEY_ADD);
		checkKey(KEY_APOSTROPHE);
		checkKey(KEY_APPS);
		checkKey(KEY_AT);
		checkKey(KEY_AX);
		checkKey(KEY_B);
		checkKey(KEY_BACK);
		checkKey(KEY_BACKSLASH);
		checkKey(KEY_C);
		checkKey(KEY_CAPITAL);
		checkKey(KEY_CIRCUMFLEX);
		checkKey(KEY_CLEAR);
		checkKey(KEY_COLON);
		checkKey(KEY_COMMA);
		checkKey(KEY_CONVERT);
		checkKey(KEY_D);
		checkKey(KEY_DECIMAL);
		checkKey(KEY_DELETE);
		checkKey(KEY_DIVIDE);
		checkKey(KEY_DOWN);
		checkKey(KEY_E);
		checkKey(KEY_END);
		checkKey(KEY_EQUALS);
		checkKey(KEY_ESCAPE);
		checkKey(KEY_F);
		checkKey(KEY_F1);
		checkKey(KEY_F10);
		checkKey(KEY_F11);
		checkKey(KEY_F12);
		checkKey(KEY_F13);
		checkKey(KEY_F14);
		checkKey(KEY_F15);
		checkKey(KEY_F16);
		checkKey(KEY_F17);
		checkKey(KEY_F18);
		checkKey(KEY_F19);
		checkKey(KEY_F2);
		checkKey(KEY_F3);
		checkKey(KEY_F4);
		checkKey(KEY_F5);
		checkKey(KEY_F6);
		checkKey(KEY_F7);
		checkKey(KEY_F8);
		checkKey(KEY_F9);
		checkKey(KEY_FUNCTION);
		checkKey(KEY_G);
		checkKey(KEY_GRAVE);
		checkKey(KEY_H);
		checkKey(KEY_HOME);
		checkKey(KEY_I);
		checkKey(KEY_INSERT);
		checkKey(KEY_J);
		checkKey(KEY_K);
		checkKey(KEY_KANA);
		checkKey(KEY_KANJI);
		checkKey(KEY_L);
		checkKey(KEY_LBRACKET);
		checkKey(KEY_LCONTROL);
		checkKey(KEY_LEFT);
		checkKey(KEY_LMENU);
		checkKey(KEY_LMETA);
		checkKey(KEY_LSHIFT);
		checkKey(KEY_M);
		checkKey(KEY_MINUS);
		checkKey(KEY_MULTIPLY);
		checkKey(KEY_N);
		checkKey(KEY_NEXT);
		checkKey(KEY_NOCONVERT);
		checkKey(KEY_NONE);
		checkKey(KEY_NUMLOCK);
		checkKey(KEY_NUMPAD0);
		checkKey(KEY_NUMPAD1);
		checkKey(KEY_NUMPAD2);
		checkKey(KEY_NUMPAD3);
		checkKey(KEY_NUMPAD4);
		checkKey(KEY_NUMPAD5);
		checkKey(KEY_NUMPAD6);
		checkKey(KEY_NUMPAD7);
		checkKey(KEY_NUMPAD8);
		checkKey(KEY_NUMPAD9);
		checkKey(KEY_NUMPADCOMMA);
		checkKey(KEY_NUMPADENTER);
		checkKey(KEY_NUMPADEQUALS);
		checkKey(KEY_O);
		checkKey(KEY_P);
		checkKey(KEY_PAUSE);
		checkKey(KEY_PERIOD);
		checkKey(KEY_POWER);
		checkKey(KEY_PRIOR);
		checkKey(KEY_Q);
		checkKey(KEY_R);
		checkKey(KEY_RBRACKET);
		checkKey(KEY_RCONTROL);
		checkKey(KEY_RETURN);
		checkKey(KEY_RIGHT);
		checkKey(KEY_RMENU);
		checkKey(KEY_RMETA);
		checkKey(KEY_RSHIFT);
		checkKey(KEY_S);
		checkKey(KEY_SCROLL);
		checkKey(KEY_SECTION);
		checkKey(KEY_SEMICOLON);
		checkKey(KEY_SLASH);
		checkKey(KEY_SLEEP);
		checkKey(KEY_SPACE);
		checkKey(KEY_STOP);
		checkKey(KEY_SUBTRACT);
		checkKey(KEY_SYSRQ);
		checkKey(KEY_T);
		checkKey(KEY_TAB);
		checkKey(KEY_U);
		checkKey(KEY_UNDERLINE);
		checkKey(KEY_UNLABELED);
		checkKey(KEY_UP);
		checkKey(KEY_V);
		checkKey(KEY_W);
		checkKey(KEY_X);
		checkKey(KEY_Y);
		checkKey(KEY_YEN);
		checkKey(KEY_Z);
	}


	private static void checkKey(final int key) {
		if(key>=0 && key<keysInThisFrame.length)
			keysInThisFrame[key] = Keyboard.isKeyDown(key);
	}

	private static int swingToLwjglConstant(final int key) {
		switch (key) {
		case (KeyEvent.VK_0): return KEY_0;
		case (KeyEvent.VK_1): return KEY_1;
		case (KeyEvent.VK_2): return KEY_2;
		case (KeyEvent.VK_3): return KEY_3;
		case (KeyEvent.VK_4): return KEY_4;
		case (KeyEvent.VK_5): return KEY_5;
		case (KeyEvent.VK_6): return KEY_6;
		case (KeyEvent.VK_7): return KEY_7;
		case (KeyEvent.VK_8): return KEY_8;
		case (KeyEvent.VK_9): return KEY_9;
		case (KeyEvent.VK_A): return KEY_A;
		case (KeyEvent.VK_ADD): return KEY_ADD;
		case (KeyEvent.VK_AT): return KEY_AT;
		case (KeyEvent.VK_B): return KEY_B;
		case (KeyEvent.VK_BACK_SLASH): return KEY_BACKSLASH;
		case (KeyEvent.VK_BACK_SPACE): return KEY_BACK;
		case (KeyEvent.VK_C): return KEY_C;
		case (KeyEvent.VK_CAPS_LOCK): return KEY_CAPITAL;
		case (KeyEvent.VK_CLEAR): return KEY_CLEAR;
		case (KeyEvent.VK_COLON): return KEY_COLON;
		case (KeyEvent.VK_COMMA): return KEY_COMMA;
		case (KeyEvent.VK_CONTROL): return KEY_LCONTROL;
		case (KeyEvent.VK_CONVERT): return KEY_CONVERT;
		case (KeyEvent.VK_D): return KEY_D;
		case (KeyEvent.VK_DECIMAL): return KEY_DECIMAL;
		case (KeyEvent.VK_DELETE): return KEY_DELETE;
		case (KeyEvent.VK_DIVIDE): return KEY_DIVIDE;
		case (KeyEvent.VK_DOWN): return KEY_DOWN;
		case (KeyEvent.VK_E): return KEY_E;
		case (KeyEvent.VK_END): return KEY_END;
		case (KeyEvent.VK_ENTER): return KEY_RETURN;
		case (KeyEvent.VK_EQUALS): return KEY_EQUALS;
		case (KeyEvent.VK_ESCAPE): return KEY_ESCAPE;
		case (KeyEvent.VK_F): return KEY_F;
		case (KeyEvent.VK_F1): return KEY_F1;
		case (KeyEvent.VK_F10): return KEY_F10;
		case (KeyEvent.VK_F11): return KEY_F11;
		case (KeyEvent.VK_F12): return KEY_F12;
		case (KeyEvent.VK_F13): return KEY_F13;
		case (KeyEvent.VK_F14): return KEY_F14;
		case (KeyEvent.VK_F15): return KEY_F15;
		case (KeyEvent.VK_F16): return KEY_F16;
		case (KeyEvent.VK_F17): return KEY_F17;
		case (KeyEvent.VK_F18): return KEY_F18;
		case (KeyEvent.VK_F19): return KEY_F19;
		case (KeyEvent.VK_F2): return KEY_F2;
		case (KeyEvent.VK_F3): return KEY_F3;
		case (KeyEvent.VK_F4): return KEY_F4;
		case (KeyEvent.VK_F5): return KEY_F5;
		case (KeyEvent.VK_F6): return KEY_F6;
		case (KeyEvent.VK_F7): return KEY_F7;
		case (KeyEvent.VK_F8): return KEY_F8;
		case (KeyEvent.VK_F9): return KEY_F9;
		case (KeyEvent.VK_G): return KEY_G;
		case (KeyEvent.VK_H): return KEY_H;
		case (KeyEvent.VK_HOME): return KEY_HOME;
		case (KeyEvent.VK_I): return KEY_I;
		case (KeyEvent.VK_INSERT): return KEY_INSERT;
		case (KeyEvent.VK_J): return KEY_J;
		case (KeyEvent.VK_K): return KEY_K;
		case (KeyEvent.VK_KANA): return KEY_KANA;
		case (KeyEvent.VK_KANJI): return KEY_KANJI;
		case (KeyEvent.VK_L): return KEY_L;
		case (KeyEvent.VK_LEFT): return KEY_LEFT;
		case (KeyEvent.VK_M): return KEY_M;
		case (KeyEvent.VK_MINUS): return KEY_MINUS;
		case (KeyEvent.VK_MULTIPLY): return KEY_MULTIPLY;
		case (KeyEvent.VK_N): return KEY_N;
		case (KeyEvent.VK_NUM_LOCK): return KEY_NUMLOCK;
		case (KeyEvent.VK_NUMPAD0): return KEY_NUMPAD0;
		case (KeyEvent.VK_NUMPAD1): return KEY_NUMPAD1;
		case (KeyEvent.VK_NUMPAD2): return KEY_NUMPAD2;
		case (KeyEvent.VK_NUMPAD3): return KEY_NUMPAD3;
		case (KeyEvent.VK_NUMPAD4): return KEY_NUMPAD4;
		case (KeyEvent.VK_NUMPAD5): return KEY_NUMPAD5;
		case (KeyEvent.VK_NUMPAD6): return KEY_NUMPAD6;
		case (KeyEvent.VK_NUMPAD7): return KEY_NUMPAD7;
		case (KeyEvent.VK_NUMPAD8): return KEY_NUMPAD8;
		case (KeyEvent.VK_NUMPAD9): return KEY_NUMPAD9;
		case (KeyEvent.VK_O): return KEY_O;
		case (KeyEvent.VK_P): return KEY_P;
		case (KeyEvent.VK_PAUSE): return KEY_PAUSE;
		case (KeyEvent.VK_PERIOD): return KEY_PERIOD;
		case (KeyEvent.VK_Q): return KEY_Q;
		case (KeyEvent.VK_R): return KEY_R;
		case (KeyEvent.VK_RIGHT): return KEY_RIGHT;
		case (KeyEvent.VK_S): return KEY_S;
		case (KeyEvent.VK_SCROLL_LOCK): return KEY_SCROLL;
		case (KeyEvent.VK_SEMICOLON): return KEY_SEMICOLON;
		case (KeyEvent.VK_SHIFT): return KEY_LSHIFT;
		case (KeyEvent.VK_SLASH): return KEY_SLASH;
		case (KeyEvent.VK_SPACE): return KEY_SPACE;
		case (KeyEvent.VK_STOP): return KEY_STOP;
		case (KeyEvent.VK_SUBTRACT): return KEY_SUBTRACT;
		case (KeyEvent.VK_T): return KEY_T;
		case (KeyEvent.VK_TAB): return KEY_TAB;
		case (KeyEvent.VK_U): return KEY_U;
		case (KeyEvent.VK_UNDEFINED): return KEY_NONE;
		case (KeyEvent.VK_UNDERSCORE): return KEY_UNDERLINE;
		case (KeyEvent.VK_UP): return KEY_UP;
		case (KeyEvent.VK_V): return KEY_V;
		case (KeyEvent.VK_W): return KEY_W;
		case (KeyEvent.VK_WINDOWS): return KEY_LMETA;
		case (KeyEvent.VK_X): return KEY_X;
		case (KeyEvent.VK_Y): return KEY_Y;
		case (KeyEvent.VK_Z): return KEY_Z;

		default:
			return KEY_NONE;
		}
	}

	
	
	@Override
	public void keyPressed(final KeyEvent g) {
		System.out.println("keypress");
		int keyCode = swingToLwjglConstant(g.getKeyCode());
		if(keyCode>=0&&keyCode<keysInNextFrame.length){
			keysInNextFrame[keyCode]=true;
		}
	}

	@Override
	public void keyReleased(final KeyEvent g) {
		System.out.println("keyrelease");
	}
	
	
	
	
	
	
	
	
	
	
	
	//Unused

	@Override
	public void keyTyped(final KeyEvent arg0) {}
}
