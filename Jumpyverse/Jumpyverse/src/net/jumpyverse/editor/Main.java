package net.jumpyverse.editor;

import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import net.jumpyverse.LWJGLCore.JKeyboard;
import net.jumpyverse.LWJGLCore.Screen;
import net.jumpyverse.entities.BlockType;
import net.jumpyverse.entities.DecoObject;
import net.jumpyverse.entities.EnemyObject;
import net.jumpyverse.entities.Walker;
import net.jumpyverse.entities.interactives.Checkpoint;
import net.jumpyverse.entities.interactives.Coin;
import net.jumpyverse.entities.interactives.Goal;
import net.jumpyverse.entities.interactives.InteractiveObject;
import net.jumpyverse.entities.interactives.Pill;
import net.jumpyverse.entities.interactives.Portal;
import net.jumpyverse.entities.interactives.Sign;
import net.jumpyverse.entities.interactives.Trampoline;
import net.jumpyverse.enums.ClickMode;
import net.jumpyverse.enums.Shape;
import net.jumpyverse.exceptions.CorruptedFileException;
import net.jumpyverse.exceptions.UnsupportedFileException;
import net.jumpyverse.exceptions.UnsupportedVersionException;
import net.jumpyverse.game.IngameMain;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.game.Renderer;
import net.jumpyverse.gui.EditorMainGui;
import net.jumpyverse.saversNLoaders.Loader;
import net.jumpyverse.saversNLoaders.ResourceLoader;
import net.jumpyverse.saversNLoaders.Saver;
import net.jumpyverse.utils.FileHelper;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.TextureLoader;

import static net.jumpyverse.editor.Session.*;

public class Main {
	public static boolean zisdown = false;
	public static boolean zwasdown = false;
	public static boolean yisdown = false;
	public static boolean ywasdown = false;
	public static boolean sisdown = false;
	public static boolean swasdown = false;
	
	public static void main(final String[] args) {
		Session.viewMode = Session.VIEWMODE_EDITOR;
		Session.startupMode = Session.VIEWMODE_EDITOR;
		editorGUI = new EditorMainGui();
		
		
		try {
			Thread.sleep(50); //Give EDT time to start
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		
		//Load LWJGL, Canvas
		editorGUI.screenSetup();
		
		
		//Load BlockTypes from BlockTypeList.txt
		ResourceLoader.load();
		try {
			logoID = TextureLoader.getTexture("PNG", org.newdawn.slick.util.ResourceLoader.getResourceAsStream("res/editoricon_tricolor.png")).getTextureID();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		Session.load();
		editorGUI.loadMenuContent();
	
		Renderer.startup();

		long lastFrame = System.currentTimeMillis();
		
		
		
		while (true) {
			
			/*###############
			 *##TIMER STUFF##
			 *###############
			 */
			long thisFrame = System.currentTimeMillis();
			timeSinceLastFrame = ((float)(thisFrame-lastFrame))/1000f;
			lastFrame = thisFrame;
			
			timeSinceStart += timeSinceLastFrame;
			
			
			/*####################
			 *##UPDATE MOUSEINFO##
			 *####################
			 */
			mouseLastX = mouseX;
			mouseLastY = mouseY;
			mouseLastTileX = mouseTileX;
			mouseLastTileY = mouseTileY;
			
			mouseX = Mouse.getX();
			mouseY = -Mouse.getY() + Screen.height - 1;
			mouseDeltaX = mouseX - mouseLastX;
			mouseDeltaY = mouseY - mouseLastY;
			
			mouseTileX = (int) (mouseX + camX);
			mouseTileY = (int) (mouseY + camY);
			
			if(mouseTileX<0)mouseTileX-=LevelData.TILESIZE;
			if(mouseTileY<0)mouseTileY-=LevelData.TILESIZE;
			
			mouseTileX /= LevelData.TILESIZE;
			mouseTileY /= LevelData.TILESIZE;			
			
			mouseDeltaTileX = (mouseTileX-mouseLastTileX);
			mouseDeltaTileY = (mouseTileY-mouseLastTileY);			
			
			mouseLastLeftDown = mouseLeftDown;
			mouseLastRightDown = mouseRightDown;
			mouseLastMiddleDown = mouseMiddleDown;
			
			mouseLeftDown = Mouse.isButtonDown(0);
			mouseRightDown = Mouse.isButtonDown(1);
			mouseMiddleDown = Mouse.isButtonDown(2);
			
			/* ##############################
			 * ##Reset/Update/Poll Keyboard##
			 * ##############################
			 */
			
			zwasdown = zisdown;
			ywasdown = yisdown;
			swasdown = sisdown;
			JKeyboard.poll();
			zisdown = JKeyboard.isKeyDown(Keyboard.KEY_Z);
			yisdown = JKeyboard.isKeyDown(Keyboard.KEY_Y);
			sisdown = JKeyboard.isKeyDown(Keyboard.KEY_S);
			
			
			/* ###############
			 * ## SHORTCUTS ##
			 * ###############
			 */
			
			if(JKeyboard.isKeyDown(Keyboard.KEY_LCONTROL) || JKeyboard.isKeyDown(Keyboard.KEY_RCONTROL)){
				if(sisdown && !swasdown){
					SwingUtilities.invokeLater(new Runnable() {
						
						@Override
						public void run() {
							Session.editorGUI.save();
						}
					});
				}
				if(yisdown && !ywasdown){
					redo();
				}
				if(zisdown && !zwasdown){
					undo();
				}
			}
			
			
			/*#############################
			 *##      MOUSE CONTROL      ##
			 *## Creating/deleting Stuff ##
			 *#############################
			 */
			if(ClickMode.currentClickMode == ClickMode.block){
				if (mouseLeftDown || JKeyboard.isKeyDown(Keyboard.KEY_INSERT)) {
					for (int i = 1000; i >= 1; i--) {
						//placing elements
						int mouseTileX = (int) (mouseX + camX - mouseDeltaX*i/1000);
						int mouseTileY = (int) (mouseY + camY - mouseDeltaY*i/1000);
						
						if(mouseTileX<0)mouseTileX-=LevelData.TILESIZE;
						if(mouseTileY<0)mouseTileY-=LevelData.TILESIZE;
						
						mouseTileX /= LevelData.TILESIZE;
						mouseTileY /= LevelData.TILESIZE;
						for(int k = mouseTileX-brushSize+1; k<mouseTileX+brushSize; k++){
							for(int j = mouseTileY-brushSize+1; j<mouseTileY+brushSize; j++){
								if(brushShape == Shape.axisbox){
									tempLevel.setTile(k, j, Session.activeGround, BlockType.getBlockID(Session.leftClickTileIDBlock));
								}else if(brushShape == Shape.diagonalbox){
									if(Math.abs(k-mouseTileX)+Math.abs(j-mouseTileY) < brushSize){
										tempLevel.setTile(k, j, Session.activeGround, BlockType.getBlockID(Session.leftClickTileIDBlock));
									}
								}else if(brushShape == Shape.circle){
									int value = Math.abs(k-mouseTileX)*Math.abs(k-mouseTileX)+Math.abs(j-mouseTileY)*Math.abs(j-mouseTileY);
									if(value < brushSize * brushSize){
										tempLevel.setTile(k, j, Session.activeGround, BlockType.getBlockID(Session.leftClickTileIDBlock));
									}
								}
							}
						}
					}
				}
				if (mouseRightDown || JKeyboard.isKeyDown(Keyboard.KEY_DELETE)) {
					for (int i = 1000; i >= 1; i--) {
						//deleting elements
						int mouseTileX = (int) (mouseX + camX + mouseDeltaX*i/1000);
						int mouseTileY = (int) (mouseY + camY + mouseDeltaY*i/1000);
						
						if(mouseTileX<0)mouseTileX-=LevelData.TILESIZE;
						if(mouseTileY<0)mouseTileY-=LevelData.TILESIZE;
						
						mouseTileX /= LevelData.TILESIZE;
						mouseTileY /= LevelData.TILESIZE;
						for(int k = mouseTileX-brushSize+1; k<mouseTileX+brushSize; k++){
							for(int j = mouseTileY-brushSize+1; j<mouseTileY+brushSize; j++){
								if(brushShape == Shape.axisbox){
									tempLevel.setTile(k, j, Session.activeGround, BlockType.getBlockID(Session.rightClickTileIDBlock));
								}else if(brushShape == Shape.diagonalbox){
									if(Math.abs(k-mouseTileX)+Math.abs(j-mouseTileY) < brushSize){
										tempLevel.setTile(k, j, Session.activeGround, BlockType.getBlockID(Session.rightClickTileIDBlock));
									}
								}else if(brushShape == Shape.circle){
									int value = Math.abs(k-mouseTileX)*Math.abs(k-mouseTileX)+Math.abs(j-mouseTileY)*Math.abs(j-mouseTileY);
									if(value < brushSize * brushSize){
										tempLevel.setTile(k, j, Session.activeGround, BlockType.getBlockID(Session.rightClickTileIDBlock));
									}
								}
								
							}
						}
					}
				}
			}
			else if(ClickMode.currentClickMode == ClickMode.deco){
				if (mouseLeftDown && !mouseLastLeftDown){
					if(!snapToGrid){
						tempLevel.addDeco(new DecoObject(mouseX+camX-DecoObject.size, mouseY+camY-DecoObject.size, Session.leftClickTileIDDeco));
					}else{
						int x = (int) (mouseX+camX-DecoObject.size);
						int y = (int) (mouseY+camY-DecoObject.size);
						
						x /= LevelData.TILESIZE;
						y /= LevelData.TILESIZE;
						
						x++;
						y++;
						
						x *= LevelData.TILESIZE;
						y *= LevelData.TILESIZE;
						
						tempLevel.addDeco(new DecoObject(x, y, Session.leftClickTileIDDeco));
					}
				}
				if (mouseRightDown && !mouseLastRightDown && editorGUI.isActive()){
					Session.selectedDeco = tempLevel.getDeco(mouseX+camX, mouseY+camY);
				}
				if (mouseRightDown && mouseLastRightDown && Session.selectedDeco != null){
					Session.selectedDeco.addX(mouseDeltaX);
					Session.selectedDeco.addY(mouseDeltaY);
					Session.selectedDeco.addX(-camDeltaX);
					Session.selectedDeco.addY(-camDeltaY);
				}
				if (Session.selectedDeco != null && JKeyboard.isKeyDown(Keyboard.KEY_DELETE)){
					tempLevel.removeDeco(Session.selectedDeco);
					Session.selectedDeco = null;
					rs.commit(tempLevel);
				}
			}
			else if(ClickMode.currentClickMode == ClickMode.interactive){
				if (mouseLeftDown && !mouseLastLeftDown){
					InteractiveObject io = null;
					switch(leftClickTileIDInter){
					case(Trampoline.mainID):
						io = new Trampoline();
						break;
					case(Goal.mainID):
						io = new Goal();
						break;
					case(Coin.mainID):
						io = new Coin();
						break;
					case(Portal.mainID):
						io = new Portal();
						break;
					case(Checkpoint.mainID):
						io = new Checkpoint();
						break;
					case(Pill.mainID):
						io = new Pill();
						break;
					case(Sign.mainID):
						io = new Sign();
						break;
					default:
						System.err.println("Illegal Interactive ID! "+leftClickTileIDInter);
						break;
					}
					if(!snapToGrid){
						io.setX(mouseX+camX-io.getWidth());
						io.setY(mouseY+camY-io.getHeight());
					}else{
						int x = (int) (mouseX+camX);
						int y = (int) (mouseY+camY);
						
						x /= LevelData.TILESIZE;
						y /= LevelData.TILESIZE;
						
						x++;
						y++;
						
						x *= LevelData.TILESIZE;
						y *= LevelData.TILESIZE;
						
						io.setX(x - LevelData.TILESIZE * 0.5f - io.getWidth() * 0.5f);
						io.setY(y - LevelData.TILESIZE * 0.5f - io.getHeight() * 0.5f);
					}
					if(io != null)tempLevel.addInter(io);
				}
				if (mouseRightDown && !mouseLastRightDown && editorGUI.isActive()){   //Abfrage .isActive() muss sein, da sonst das Programm freezed. Grund unbekannt.
					Session.selectedInteractive = tempLevel.getInter(mouseX+camX, mouseY+camY);
					editorGUI.updateInteractiveObjectsAttributePanel();
				}
				if (mouseRightDown && mouseLastRightDown && Session.selectedInteractive != null){
					Session.selectedInteractive.addX(mouseDeltaX);
					Session.selectedInteractive.addY(mouseDeltaY);
					Session.selectedInteractive.addX(-camDeltaX);
					Session.selectedInteractive.addY(-camDeltaY);
				}
				if (Session.selectedInteractive != null && JKeyboard.isKeyDown(Keyboard.KEY_DELETE)){
					tempLevel.removeInter(Session.selectedInteractive);
					Session.selectedInteractive = null;
					rs.commit(tempLevel);
				}
			}
			else if(ClickMode.currentClickMode == ClickMode.fill){
				try{
					int mouseHoverTile = tempLevel.getTile(mouseTileX, mouseTileY, Session.activeGround) / BlockType.VARIATIONSPERTEXTURE;
					if(mouseLeftDown && !mouseLastLeftDown && mouseHoverTile != Session.leftClickTileIDBlock){
						tempLevel.fill(mouseTileX, mouseTileY, Session.activeGround, mouseHoverTile, Session.leftClickTileIDBlock);
					}
					if(mouseRightDown && !mouseLastRightDown && mouseHoverTile != Session.rightClickTileIDBlock){
						tempLevel.fill(mouseTileX, mouseTileY, Session.activeGround, mouseHoverTile, Session.rightClickTileIDBlock);
					}
				}catch(StackOverflowError e){
					if(canUndo()){
						Runnable r = new Runnable() {
							@Override
							public void run() {
								JOptionPane
								.showMessageDialog(
										editorGUI,
										"Woops! Stacksize was not big enough, we are working on it!",
										"Stackerror", JOptionPane.ERROR_MESSAGE);
							}
						};
						
						Thread t = new Thread(r);
						t.start();
					}
				}
			}
			else if(ClickMode.currentClickMode == ClickMode.player){
				if(mouseLeftDown || mouseRightDown){
					tempLevel.getPlayer().setX(mouseX + camX);
					tempLevel.getPlayer().setY(mouseY + camY);
				}
			}
			else if(ClickMode.currentClickMode == ClickMode.enemy){
				if (mouseLeftDown && !mouseLastLeftDown){
					int x = (int) (mouseX+camX);
					int y = (int) (mouseY+camY);
					if(snapToGrid){
						x /= LevelData.TILESIZE;
						y /= LevelData.TILESIZE;
						
						x++;
						y++;
						
						x *= LevelData.TILESIZE;
						y *= LevelData.TILESIZE;
						
					}
					
					switch(Session.leftClickTileIDEnemy){
					case(Walker.mainID):
						tempLevel.addEnemy(new Walker(x, y));
						break;
					default:
						System.err.println("Illegal EnemyID");
						break;
					}
				}
				if (mouseRightDown && !mouseLastRightDown && editorGUI.isActive()){  //Abfrage .isActive() muss sein, da sonst das Programm freezed. Grund unbekannt.
					Session.selectedEnemy = tempLevel.getEnemy(mouseX+camX, mouseY+camY);
					editorGUI.updateEnemieAttributePanel();
				}
				if (mouseRightDown && mouseLastRightDown && Session.selectedEnemy != null){
					Session.selectedEnemy.addX(mouseDeltaX);
					Session.selectedEnemy.addY(mouseDeltaY);
					Session.selectedEnemy.addX(-camDeltaX);
					Session.selectedEnemy.addY(-camDeltaY);
				}
				if (Session.selectedEnemy != null && JKeyboard.isKeyDown(Keyboard.KEY_DELETE)){
					tempLevel.removeEnemy(Session.selectedEnemy);
					Session.selectedEnemy = null;
					rs.commit(tempLevel);
				}
			}
			
			/*###################
			 *##ROLLBACK UPDATE##
			 *###################
			 */
			if(Session.mouseLastLeftDown && !Session.mouseLeftDown){
				rs.commit(tempLevel);
			}
			if(Session.mouseLastRightDown && !Session.mouseRightDown && ClickMode.currentClickMode != ClickMode.interactive && ClickMode.currentClickMode != ClickMode.enemy && ClickMode.currentClickMode != ClickMode.deco){
				rs.commit(tempLevel);
			}
			
			
			/*###############
			 *##CAM CONTROL##
			 *###############
			 */
			camLastX = camX;
			camLastY = camY;
			
			if (mouseMiddleDown||JKeyboard.isKeyDown(Keyboard.KEY_LSHIFT)){
				if(fixMousePointX==-10000&&fixMousePointY==-10000){
					fixMousePointX = Session.mouseX;
					fixMousePointY = Session.mouseY;
				}else{
					camX += (Session.mouseX - fixMousePointX) * timeSinceLastFrame * 3;
					camY += (Session.mouseY - fixMousePointY) * timeSinceLastFrame * 3;
				}
			}else{
				fixMousePointX = -10000;
				fixMousePointY = -10000;
			}

			if (JKeyboard.isKeyDown(Keyboard.KEY_UP)||JKeyboard.isKeyDown(Keyboard.KEY_W)) {
				camY -= CAMSPEED*timeSinceLastFrame;
			}
			if (JKeyboard.isKeyDown(Keyboard.KEY_DOWN)||(JKeyboard.isKeyDown(Keyboard.KEY_S) && !JKeyboard.isKeyDown(Keyboard.KEY_LCONTROL) && !JKeyboard.isKeyDown(Keyboard.KEY_RCONTROL))) {
				camY += CAMSPEED*timeSinceLastFrame;
			}
			if (JKeyboard.isKeyDown(Keyboard.KEY_LEFT)||JKeyboard.isKeyDown(Keyboard.KEY_A)) {
				camX -= CAMSPEED*timeSinceLastFrame;
			}
			if (JKeyboard.isKeyDown(Keyboard.KEY_RIGHT)||JKeyboard.isKeyDown(Keyboard.KEY_D)) {
				camX += CAMSPEED*timeSinceLastFrame;
			}
			
			if(camX < 0 ) camX = 0;
			if(camY < 0 ) camY = 0;
			
			if(camX > tempLevel.getWidth() * LevelData.TILESIZE - Screen.width) camX = tempLevel.getWidth() * LevelData.TILESIZE - Screen.width;
			if(camY > tempLevel.getHeight() * LevelData.TILESIZE - Screen.height) camY = tempLevel.getHeight() * LevelData.TILESIZE - Screen.height;
			
			camDeltaX = camLastX - camX;
			camDeltaY = camLastY - camY;
			
			/*################
			 *##UPDATE STUFF##
			 *################
			 */
			
			tempLevel.update(timeSinceLastFrame);	
			for(int i = 0; i<tempLevel.getInterSize(); i++){
				InteractiveObject io = tempLevel.getInter(i);
				io.calculateNeedDraw();
			}
			for(int i = 0; i<tempLevel.getEnemySize(); i++){
				EnemyObject eo = tempLevel.getEnemy(i);
				eo.calculateNeedDraw();
			}
			
			
			
			

			/*##############
			 *##DRAW STUFF##
			 *##############
			 */
			//every frame is deleted, then redrawn each cycle
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			Renderer.draw();
			
			frameUpdate();
			Display.update();
			
			
			
			
			

			/*############
			 *##Testmode##
			 *############
			 */
			if(inTestMode){
				editorState = tempLevel.clone();
//				for(int focustrys = 0; focustrys < 1000; focustrys++){
//					if(editorGUI.canvasHasFocus()){
//						break;
//					}
//					editorGUI.canvasRequestFocus();
//					try {
//						Thread.sleep(1);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
				int oldViewMode = viewMode;
				viewMode = VIEWMODE_GAME;
				System.gc();
				IngameMain.main(null);
				inTestMode = false;
				tempLevel = editorState;
				viewMode = oldViewMode;
				editorGUI.objectDropDownMenuRequestFocus();
			}
			
			
			
			
			/*############
			 *##NAP TIME##
			 *############
			 */
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
	
	
	/*###################
	 *##Save, Load, New##
	 *###################
	 */
	
	public static void save(final File f) throws IOException{
		Saver.save(f, Session.tempLevel);
	}
	
	public static void load(final File f) throws IOException, UnsupportedFileException, UnsupportedVersionException, CorruptedFileException{
		FileHelper.unZipIt(f.getAbsolutePath());
		File tempFile = new File(f.getAbsoluteFile() + ".temp");
		tempLevel = Loader.load(tempFile);
		tempFile.delete();
		rs.commit(tempLevel);
		camX = DEFAULT_CAMX;
		camY = DEFAULT_CAMY;
	}
	
	public static void _new(){
		tempLevel = new LevelData(DEFAULT_MAPWIDTH, DEFAULT_MAPHEIGHT);
		rs.commit(tempLevel);
		camX = DEFAULT_CAMX;
		camY = DEFAULT_CAMY;
	}
	
	public static void startTestMode(){
		inTestMode = true;
	}
	
	public static void stopTestMode(){
		inTestMode = false;
	}
	
	
	/*###################################
	 *##FUNCTIONALITY UnDo/ReDo Buttons##
	 *###################################
	 */
	
	public static void undo(){
		rs.goBack();
		tempLevel = rs.getCurrentState().clone();
	}
	
	public static void redo(){
		rs.goForward();
		tempLevel = rs.getCurrentState().clone();
	}
	
	public static boolean canUndo(){
		return rs.hasPrevious();
	}
	
	public static boolean canRedo(){
		return rs.hasNext();
	}
	
	public static void frameUpdate(){
		
		//enabling/disabling undo/redo buttons
		if (canUndo()){
			editorGUI.unDoEnable();
		}
		else editorGUI.unDoDisable();
		
		//enabling/disabling undo/redo buttons
		if (canRedo()){
			editorGUI.reDoEnable();
		}
		else editorGUI.reDoDisable();
	}
}
