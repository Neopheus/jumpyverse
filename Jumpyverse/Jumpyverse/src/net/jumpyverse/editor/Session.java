package net.jumpyverse.editor;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import net.jumpyverse.entities.DecoObject;
import net.jumpyverse.entities.EnemyObject;
import net.jumpyverse.entities.interactives.InteractiveObject;
import net.jumpyverse.enums.Shape;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.game.Renderer;
import net.jumpyverse.gui.EditorMainGui;
import net.jumpyverse.utils.ImageHelper;

/*
 * Settings which are only important in Editor
 */
public class Session {
	public final static Random random = new Random();	//Oft gebraucht. Hier, damit es nicht zu viele Objects gibt.
	
	public final static int DEFAULT_MAPWIDTH = 1024;
	public final static int DEFAULT_MAPHEIGHT = 128;
	public final static float DEFAULT_CAMX = 0;
	public final static float DEFAULT_CAMY = 1024*1024*32;
	
	public static int mouseX = 0;
	public static int mouseY = 0;
	public static int mouseLastX = 0;
	public static int mouseLastY = 0;
	public static int mouseDeltaX = 0;
	public static int mouseDeltaY = 0;
	public static int mouseTileX = 0;
	public static int mouseTileY = 0;
	public static int mouseLastTileX = 0;
	public static int mouseLastTileY = 0;
	public static int mouseDeltaTileX = 0;
	public static int mouseDeltaTileY = 0;
	public static boolean mouseLeftDown       = false;
	public static boolean mouseRightDown      = false;
	public static boolean mouseMiddleDown     = false;
	public static boolean mouseLastLeftDown   = false;
	public static boolean mouseLastRightDown  = false;
	public static boolean mouseLastMiddleDown = false;
	public static float fixMousePointX = -10000;
	public static float fixMousePointY = -10000;
	public static float camX = DEFAULT_CAMX;
	public static float camY = DEFAULT_CAMY;
	public static float camLastX = DEFAULT_CAMX;
	public static float camLastY = DEFAULT_CAMY;
	public static float camDeltaX = 0;
	public static float camDeltaY = 0;
	public static float CAMSPEED = 500f;
	
	
	public static int brushSize = 1;
	public static Shape brushShape = Shape.axisbox;
	
	public static boolean snapToGrid = true;
	public static short  leftClickTileIDBlock = 1;
	public static short rightClickTileIDBlock = 0;
	public static short  leftClickTileIDDeco = 0;
	public static short rightClickTileIDDeco = 0;
	public static short  leftClickTileIDInter = 1;
	public static short rightClickTileIDInter = 0;
	public static short  leftClickTileIDEnemy = 0;
	public static short rightClickTileIDEnemy = 0;
	public static DecoObject selectedDeco = null;
	public static InteractiveObject selectedInteractive = null;
	public static EnemyObject selectedEnemy = null;
	public static int   activeGround = LevelData.PLAYGROUND;
	public static RollbackSystem rs = new RollbackSystem(25, new LevelData(DEFAULT_MAPWIDTH, DEFAULT_MAPHEIGHT));
	public static LevelData tempLevel = rs.getCurrentState().clone();
	public static LevelData editorState = null;
	public static LevelData respawnState = null;
	
	
	public static float timeSinceStart = 0;
	public static float timeSinceLastFrame = 0;
	
	
	
	public static float logoRotation = 0;
	public static float logoAcceleration = 0;
	public static int logoID = 0;
	
	public static Renderer renderer = new Renderer();
	public static EditorMainGui editorGUI;
	public static Texture defaultTexture;
	public static BufferedImage defaultBiRepresentation;
	
	
	public static final int VIEWMODE_GAME = 1;
	public static final int VIEWMODE_EDITOR = 2;
	
	
	
	public static int viewMode = VIEWMODE_GAME;
	public static int startupMode = VIEWMODE_GAME;
	
	public static boolean inTestMode = false;
	
	
	
	public static Thread rendererThread;
	
	
	
	
	public static boolean showMousePosition = false;
	
	public static void load(){
		try {
			defaultTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/sprite_default.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		defaultBiRepresentation = new BufferedImage(32, 32, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = defaultBiRepresentation.getGraphics();

		g.drawImage(ImageHelper.loadImage("res/sprite_default.png"), 0, 0, null);
		
		g.dispose();
	}
}
