package net.jumpyverse.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.interactives.Pill;
import net.jumpyverse.entities.interactives.Sign;

public class SignPanel extends JPanel {
	// This class defines the panel for setting the attributes of the selected
	// sign

	private TitledBorder signBorder = BorderFactory
			.createTitledBorder("Sign attributes");

	private Sign sign;

	private String msg;

	private JPanel signPanel = new JPanel();

	// Labels and input fields
	private JLabel attributeLabel = new JLabel("Message: ");
	
	private JTextArea msgField;

	public SignPanel() {

		// get the current sign
		if (Session.selectedInteractive instanceof Sign) {
			sign = (Sign) Session.selectedInteractive;
		}

		signPanel.setBorder(signBorder);

		msg = sign.msg;

		// configuring the input fields
		msgField = new JTextArea(msg, 10, 4);
		msgField.setWrapStyleWord(true);
		msgField.setLineWrap(true);
		msgField.setColumns(10);
		
		// adding listeners
		msgField.addCaretListener(new FieldListener());

		// adding components
		signPanel.add(attributeLabel);
		signPanel.add(new JScrollPane(msgField));

		this.add(signPanel);

	}

	// listener for updating the attributes
	private class FieldListener implements CaretListener {
		@Override
		public void caretUpdate(CaretEvent ev) {
			JTextArea sourceField = (JTextArea) ev
					.getSource();
			// empty fields will not trigger an update
			if (!sourceField.getText().equals("")) {
				// catch bad user input
				try {
					// get the current sign, important due to commit
					// functionality
					sign = (Sign) Session.selectedInteractive;

					if (sourceField == msgField) {
						msg = sourceField.getText();
						sign.msg = msg;
						System.out.println("Massage: " + msg);
					}

				} catch (NumberFormatException e) {
					System.out.println(e);
				}
			}
		}
	}
}