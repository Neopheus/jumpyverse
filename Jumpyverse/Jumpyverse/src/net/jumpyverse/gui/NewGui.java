package net.jumpyverse.gui;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import net.jumpyverse.editor.Main;
import net.jumpyverse.editor.Session;
import net.jumpyverse.utils.ImageHelper;

//This class creates a new Level and asks for the size, when "New" is selected in the main editor window
public class NewGui extends JDialog {
	TitledBorder sizeBorder = BorderFactory
			.createTitledBorder("Set the level size");
	JPanel mainPanel = new JPanel();
	int width = 400;
	int height = 400;

	private JFrame frame;

	private static int worldWidth = Session.tempLevel.getWidth();
	private static int worldHeight = Session.tempLevel.getHeight();

	private JLabel widthLabel = new JLabel("Width: ");
	private JLabel heightLabel = new JLabel("Height: ");

	private NumberFormat nf = NumberFormat.getIntegerInstance();

	private JFormattedTextField widthField;
	private JFormattedTextField heightField;

	private JButton oB = new JButton("OK");
	private JButton cB = new JButton("Cancel");

	NewGui() {
		super(null, "Creating new level", Dialog.ModalityType.APPLICATION_MODAL);
		mainPanel.setLayout(new GridLayout(0, 2, 20, 20));

		// prevent dots from appearing in the returned number: "10500" instead
		// of "10.500"
		nf.setGroupingUsed(false);
		widthField = new JFormattedTextField(nf);
		heightField = new JFormattedTextField(nf);

		widthField.setText("" + worldWidth);
		heightField.setText("" + worldHeight);

		mainPanel.setBorder(sizeBorder);
		mainPanel.add(widthLabel);
		mainPanel.add(widthField);
		mainPanel.add(heightLabel);
		mainPanel.add(heightField);

		oB.addActionListener(new OkListener());
		mainPanel.add(oB);

		// cancel
		cB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				NewGui.this.dispose();
			}
		});
		mainPanel.add(cB);

		add(mainPanel);
		setLocationRelativeTo(getParent());
		setAutoRequestFocus(true);
		pack();
		setIconImage(ImageHelper.loadImage("resgui/editoricon.png"));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	// ActionListener for ok button
	class OkListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			EditorMainGui emg;
			worldWidth = Integer.parseInt(widthField.getText());
			worldHeight = Integer.parseInt(heightField.getText());

			// checking constraints
			if (worldWidth < 32 || worldHeight < 18
					|| worldWidth * worldHeight > 1048576) {
				JOptionPane
						.showMessageDialog(
								frame,
								"The world size you set is invalid.\n\n Minimum size:\n    Width:  32\n    Height: 18\n\n Maximum size:\n    Width*Height = 1048576 (1024*1024)\n\n Size you set:\n    Width:  "
										+ worldWidth
										+ "\n    Height: "
										+ worldHeight, "Invalid world size",
								JOptionPane.ERROR_MESSAGE);
			} else {
				// Telling the editor that a new world is created
				EditorMainGui.newControl = 1;
				// New world is created
				Main._new();

				Session.tempLevel.setWidthHeight(worldWidth, worldHeight, 0, 0);
				Session.rs.commit(Session.tempLevel);
				Session.tempLevel = Session.tempLevel.clone();
				NewGui.this.dispose();
			}

		}
	}
}
