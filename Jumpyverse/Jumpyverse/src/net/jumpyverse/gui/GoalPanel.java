package net.jumpyverse.gui;

import java.awt.GridLayout;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.interactives.Goal;

public class GoalPanel extends JPanel {
	private int exitInt = 0;
	private Goal goaly;

	private TitledBorder goalBorder = BorderFactory
			.createTitledBorder("Goal attributes");

	private JPanel goalPanel = new JPanel();

	private JLabel exitLabel = new JLabel("Exitcode: ");

	private NumberFormat nf = NumberFormat.getIntegerInstance();

	private JFormattedTextField exitField;

	public GoalPanel() {
		// prevent dots from appearing in the returned number: "10500" instead
		// of "10.500"
		nf.setGroupingUsed(false);
		exitField = new JFormattedTextField(nf);

		if (Session.selectedInteractive instanceof Goal) {
			goaly = (Goal) Session.selectedInteractive;
		}

		exitInt = (int) goaly.EXITCODE;

		exitField.setText("" + exitInt);

		exitField.addCaretListener(new ExitListener());

		goalPanel.setBorder(goalBorder);
		goalPanel.setLayout(new GridLayout(0, 2, 10, 10));
		goalPanel.add(exitLabel);
		goalPanel.add(exitField);
		this.add(goalPanel);
	}

	// listener for setting the exit code
	private class ExitListener implements CaretListener {

		@Override
		public void caretUpdate(CaretEvent arg0) {
			// empty fields will not trigger an update
			if (!exitField.getText().equals("")) {
				// catch bad user input
				try {
					exitInt = Integer.parseInt(exitField.getText());

					goaly.EXITCODE = exitInt;
					System.out.println("Exitcode: " + goaly.EXITCODE);
				} catch (NumberFormatException e) {
					System.out.println(e);
				}
			}
		}
	}

}
