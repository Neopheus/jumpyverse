package net.jumpyverse.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.Walker;
import net.jumpyverse.entities.interactives.Pill;
import net.jumpyverse.enums.Directions;

public class PillPanel extends JPanel {
	// This class defines the panel for setting the attributes of the selected
	// pill

	private TitledBorder pillBorder = BorderFactory
			.createTitledBorder("Pill attributes");

	private Pill pill;

	private int valueInt;
	private Pill.ChangeableAttributes att;

	private JPanel pillPanel = new JPanel();
	private NumberFormat nf = NumberFormat.getIntegerInstance();

	// Labels and input fields
	private JLabel attributeLabel = new JLabel("Attribute: ");
	private JLabel valueLabel = new JLabel("Add: ");

	private JComboBox<String> attComboBox = null;
	private JFormattedTextField valueField;
	
	private String[] attributes = {"Walk speed", "Run speed", "Accel.", "Air accel.", "Jump power", "Extra jumps"};

	public PillPanel() {

		// get the current pill
		if (Session.selectedInteractive instanceof Pill) {
			pill = (Pill) Session.selectedInteractive;
		}

		// prevent dots from appearing in the returned number: "10500" instead
		// of "10.500"
		nf.setGroupingUsed(false);

		pillPanel.setBorder(pillBorder);
		pillPanel.setLayout(new GridLayout(0, 2, 10, 10));

		valueInt = pill.VALUE;
		att = pill.att;

		// configuring the input fields
		attComboBox = new JComboBox<String>(attributes);
		valueField = new JFormattedTextField(nf);

		// initialize with current values
		if(att == Pill.ChangeableAttributes.maxspeedwalk){
			attComboBox.setSelectedIndex(0);
		}else if(att == Pill.ChangeableAttributes.maxspeedrun){
			attComboBox.setSelectedIndex(1);
		}else if(att == Pill.ChangeableAttributes.acceleration){
			attComboBox.setSelectedIndex(2);
		}else if(att == Pill.ChangeableAttributes.airacceleration){
			attComboBox.setSelectedIndex(3);
		}else if(att == Pill.ChangeableAttributes.jumppower){
			attComboBox.setSelectedIndex(4);
		}else if(att == Pill.ChangeableAttributes.extraJumps){
			attComboBox.setSelectedIndex(5);
		}
		valueField.setText("" + valueInt);

		// adding listeners
		attComboBox.addActionListener(new comboBoxListener());
		valueField.addCaretListener(new FieldListener());

		// adding components
		pillPanel.add(attributeLabel);
		pillPanel.add(attComboBox);
		pillPanel.add(valueLabel);
		pillPanel.add(valueField);

		this.add(pillPanel);

	}

	// listener for updating the attributes
	private class FieldListener implements CaretListener {
		@Override
		public void caretUpdate(CaretEvent ev) {
			JFormattedTextField sourceField = (JFormattedTextField) ev
					.getSource();
			// empty fields will not trigger an update
			if (!sourceField.getText().equals("")) {
				// catch bad user input
				try {
					// get the current pill, important due to commit
					// functionality
					pill = (Pill) Session.selectedInteractive;

					if (sourceField == valueField) {
						valueInt = Integer.parseInt(valueField.getText());
						pill.VALUE = valueInt;
						System.out.println("Value: " + valueInt);
					}

				} catch (NumberFormatException e) {
					System.out.println(e);
				}
			}
		}
	}
	
	private class comboBoxListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			int selection = attComboBox.getSelectedIndex();
			if(selection == 0){
				att = Pill.ChangeableAttributes.maxspeedwalk;
			}else if(selection == 1){
				att = Pill.ChangeableAttributes.maxspeedrun;
			}else if(selection == 2){
				att = Pill.ChangeableAttributes.acceleration;
			}else if(selection == 3){
				att = Pill.ChangeableAttributes.airacceleration;
			}else if(selection == 4){
				att = Pill.ChangeableAttributes.jumppower;
			}else if(selection == 5){
				att = Pill.ChangeableAttributes.extraJumps;
			}
			
			pill.att = att;
		}
		
	}
}