package net.jumpyverse.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.Walker;
import net.jumpyverse.entities.interactives.Trampoline;
import net.jumpyverse.enums.Directions;

public class WalkerPanel extends JPanel {
	// This class defines the panel for setting the attributes of the selected
	// enemy

	private TitledBorder walkerBorder = BorderFactory
			.createTitledBorder("Walker attributes");

	private Walker walker;

	private int healthInt;
	private int maxSpeedInt;
	private int walkSpeedInt;

	private JPanel walkPanel = new JPanel();
	private NumberFormat nf = NumberFormat.getIntegerInstance();

	// Labels and input fields
	private JLabel healthLabel = new JLabel("Health: ");
	private JLabel maxSpeedLabel = new JLabel("Max. speed: ");
	private JLabel walkSpeedLabel = new JLabel("Walk speed: ");
	private JLabel lookDirectionLabel = new JLabel("Look Direction: ");

	private JFormattedTextField healthField;
	private JFormattedTextField maxSpeedField;
	private JFormattedTextField walkSpeedField;
	private JComboBox<String> directionComboBox = null;
	
	private String[] directions = {"left", "right"};
	private Directions direction = null;

	public WalkerPanel() {

		// get the current walker
		if (Session.selectedEnemy instanceof Walker) {
			walker = (Walker) Session.selectedEnemy;
		}

		// prevent dots from appearing in the returned number: "10500" instead
		// of "10.500"
		nf.setGroupingUsed(false);

		walkPanel.setBorder(walkerBorder);
		walkPanel.setLayout(new GridLayout(0, 2, 10, 10));

		healthInt = walker.HEALTH;
		maxSpeedInt = (int) walker.MAXSPEEDHARD;
		walkSpeedInt = (int) walker.MAXSPEEDWALK;
		direction = walker.getLookDirection();

		// configuring the input fields
		healthField = new JFormattedTextField(nf);
		maxSpeedField = new JFormattedTextField(nf);
		walkSpeedField = new JFormattedTextField(nf);
		directionComboBox = new JComboBox<String>(directions);

		// initialize with current values
		healthField.setText("" + healthInt);
		maxSpeedField.setText("" + maxSpeedInt);
		walkSpeedField.setText("" + walkSpeedInt);
		if(direction == Directions.left){
			directionComboBox.setSelectedIndex(0);
		}else{
			directionComboBox.setSelectedIndex(1);
		}

		// adding listeners
		healthField.addCaretListener(new FieldListener());
		maxSpeedField.addCaretListener(new FieldListener());
		walkSpeedField.addCaretListener(new FieldListener());
		directionComboBox.addActionListener(new comboBoxListener());

		// adding components
		walkPanel.add(healthLabel);
		walkPanel.add(healthField);
		walkPanel.add(maxSpeedLabel);
		walkPanel.add(maxSpeedField);
		walkPanel.add(walkSpeedLabel);
		walkPanel.add(walkSpeedField);
		walkPanel.add(lookDirectionLabel);
		walkPanel.add(directionComboBox);

		this.add(walkPanel);

	}

	// listener for updating the attributes
	private class FieldListener implements CaretListener {
		@Override
		public void caretUpdate(CaretEvent ev) {
			JFormattedTextField sourceField = (JFormattedTextField) ev
					.getSource();
			// empty fields will not trigger an update
			if (!sourceField.getText().equals("")) {
				// catch bad user input
				try {
					// get the current player, important due to commit
					// functionality
					walker = (Walker) Session.selectedEnemy;

					if (sourceField == healthField) {
						healthInt = Integer.parseInt(healthField.getText());
						walker.HEALTH = healthInt;
						walker.health = healthInt;
						System.out.println("health: " + walker.HEALTH);
					}
					if (sourceField == maxSpeedField) {
						maxSpeedInt = Integer.parseInt(maxSpeedField.getText());
						walker.MAXSPEEDHARD = maxSpeedInt;
						System.out.println("maxSpeed: " + walker.MAXSPEEDHARD);
					}
					if (sourceField == walkSpeedField) {
						walkSpeedInt = Integer.parseInt(walkSpeedField
								.getText());
						walker.MAXSPEEDWALK = walkSpeedInt;
						System.out.println("walkSpeed: " + walker.MAXSPEEDWALK);
					}

				} catch (NumberFormatException e) {
					System.out.println(e);
				}
			}
		}
	}
	
	private class comboBoxListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(directionComboBox.getSelectedIndex() == 0){
				direction = Directions.left;
			}else{
				direction = Directions.right;
			}
			
			walker.setLookDirection(direction);
		}
		
	}
}