package net.jumpyverse.gui;

import java.awt.GridLayout;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.interactives.Portal;

public class PortalPanel extends JPanel{
	private int destinationX = 0;
	private int destinationY = 0;
	private Portal portaly;

	private TitledBorder portalBorder = BorderFactory
			.createTitledBorder("Portal attributes");

	private JPanel goalPanel = new JPanel();

	private JLabel destinationXLabel = new JLabel("Destination X: ");
	private JLabel destinationYLabel = new JLabel("Destination Y: ");

	private NumberFormat nf = NumberFormat.getIntegerInstance();

	private JFormattedTextField destinationXField;
	private JFormattedTextField destinationYField;

	public PortalPanel() {
		// prevent dots from appearing in the returned number: "10500" instead
		// of "10.500"
		nf.setGroupingUsed(false);
		destinationXField = new JFormattedTextField(nf);
		destinationYField = new JFormattedTextField(nf);

		if (Session.selectedInteractive instanceof Portal) {
			portaly = (Portal) Session.selectedInteractive;
		}

		destinationX = (int) portaly.DESTINATIONX;
		destinationY = (int) portaly.DESTINATIONY;

		destinationXField.setText("" + destinationX);
		destinationYField.setText("" + destinationY);

		destinationXField.addCaretListener(new DestinationListener());
		destinationYField.addCaretListener(new DestinationListener());

		goalPanel.setBorder(portalBorder);
		goalPanel.setLayout(new GridLayout(0, 2, 10, 10));
		goalPanel.add(destinationXLabel);
		goalPanel.add(destinationXField);
		goalPanel.add(destinationYLabel);
		goalPanel.add(destinationYField);
		this.add(goalPanel);
	}

	// listener for setting the destination
	private class DestinationListener implements CaretListener {

		@Override
		public void caretUpdate(CaretEvent arg0) {
			// empty fields will not trigger an update
			if (!destinationXField.getText().equals("") && !destinationYField.getText().equals("")) {
				// catch bad user input
				try {
					destinationX = Integer.parseInt(destinationXField.getText());
					destinationY = Integer.parseInt(destinationYField.getText());

					portaly.DESTINATIONX = destinationX;
					portaly.DESTINATIONY = destinationY;
				} catch (NumberFormatException e) {
					System.out.println(e);
				}
			}
		}
	}
}
