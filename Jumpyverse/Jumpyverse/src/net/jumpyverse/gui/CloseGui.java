package net.jumpyverse.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import net.jumpyverse.editor.Main;
import net.jumpyverse.utils.ImageHelper;

//This class handles the closing dialog of the editor window with the common option save/don't save/cancel

public class CloseGui extends JDialog {
	int width = 400;
	int height = 400;

	private JFrame frame = new JFrame();
	private JLabel textL = new JLabel("Save before closing?");
	private JPanel buttonP = new JPanel();
	private JButton yB = new JButton("Yes");
	private JButton nB = new JButton("No");
	private JButton cB = new JButton("Cancel");

	CloseGui() {
		super(null, "Confirm close", Dialog.ModalityType.APPLICATION_MODAL);
		setLayout(new BorderLayout());
		
		//save and close application
		yB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//open filechooser if level hasn't been saved before
				if (EditorMainGui.fileName.equals("Jumpyverse - Editor")) {
					EditorFileChooser chooser = new EditorFileChooser();
					int option = chooser.showSaveDialog(null);
					if (option == JFileChooser.APPROVE_OPTION) {
						try {
							File f = chooser.getSelectedFile();
							// appending ".jmpy" to filename
							String filePath = f.getPath();
							if (!filePath.toLowerCase().endsWith(".jmpy")) {
								f = new File(filePath + ".jmpy");
							}
							Main.save(f);
							System.exit(0);
							
						} catch (IOException ioE) {
							JOptionPane.showMessageDialog(frame,
									"An IO Exception occurred. Level could not be saved.",
									"Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				} else {
					try {
						Main.save(new File(EditorMainGui.fileName));
						System.exit(0);
					} catch (IOException ioE) {
						JOptionPane
								.showMessageDialog(
										frame,
										"An IO Exception occurred. Level could not be saved.",
										"Error", JOptionPane.ERROR_MESSAGE);
					}

				}
				
			}
		});
		
		//close without saving
		nB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		//cancel
		cB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				CloseGui.this.dispose();
			}
		});

		buttonP.add(yB);
		buttonP.add(nB);
		buttonP.add(cB);
		add(textL, BorderLayout.NORTH);
		add(buttonP);


		setLocationRelativeTo(getParent());
		setAutoRequestFocus(true);
		pack();
		setTitle("Confirm exit");
		setIconImage(ImageHelper.loadImage("resgui/editoricon.png"));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
	}

}
