package net.jumpyverse.wrapper;

/**
 * NOT THREAD SAFE!!!
 * @author Brotcrunsher
 *
 */
public class Point {
	public int x;
	public int y;
	
	public Point(final int x, final int y){
		this.x = x;
		this.y = y;
	}
}
