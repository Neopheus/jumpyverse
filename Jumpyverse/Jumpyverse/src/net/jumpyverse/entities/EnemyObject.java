package net.jumpyverse.entities;

import javax.swing.JPanel;

public abstract class EnemyObject extends CreatureObject{
	protected int HEALTH;
	protected int DAMAGE;
	public int getHealth(){
		return HEALTH;
	}
	public void setHealth(final int health){
		this.HEALTH = health;
	}
	public int getDamage(){
		return HEALTH;
	}
	public void setDamage(final int damage){
		this.DAMAGE = damage;
	}
	public void takeDamage(final int dmg){
		HEALTH -= dmg;
		if(HEALTH<=0){
			kill();
		}
	}
	public abstract void setIsJumping(boolean isJumping);
	public abstract int getID();
	public abstract void kill();
	
	
	public abstract JPanel getObjectPanel();
	
}
