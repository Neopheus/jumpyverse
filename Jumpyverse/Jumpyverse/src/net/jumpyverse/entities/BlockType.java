package net.jumpyverse.entities;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.imageio.ImageIO;

import net.jumpyverse.editor.Session;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class BlockType {
	public static final short VARIATIONSPERTEXTURE = 16;
	
	
	private static String locationOfJar = new File(BlockType.class.getProtectionDomain().getCodeSource() .getLocation().getPath()).getName();
	private static int nextLoadNumber = 1;
	private static Texture[] tex = new Texture[Short.MAX_VALUE];
	private static int[] availabletextures = new int[tex.length/VARIATIONSPERTEXTURE];
	private static BufferedImage[] biRepresentation = new BufferedImage[tex.length/VARIATIONSPERTEXTURE];
	private static String[] stringRepresentation = new String[tex.length/VARIATIONSPERTEXTURE];
	private static Random random = new Random();
	
	
	public static void loadBlockSheet(final String path, final int mainID) throws IOException{
		//mainID = the reference to a new Blocktype.
		//I STRONGLY RECOMMEND THAT YOU DON'T USE THIS METHOD IF YOU DONT KNOW WHAT IT DOES! USE loadBlockSheet(String) INSTEAD!
		//1 Blocksheet has 16 different subimages. Which one is drawn decides Brush.drawSprite
		//Additionaly 1 BlockType can have up to VARIATIONSPERTEXTURE different Spritesheets, which DONT have to load seperatly.
		//Just name all of them sprite_blabla_01.png where 01 goes up to 16.
		//Only a real ID is saved in the leveldata.
		//real ID = mainID * VARIATIONSPERTEXTURE + (the number of the .png file, for example 4)
		if(mainID<=0||mainID>=availabletextures.length) throw new IllegalArgumentException("id must be between 1 and "+availabletextures.length);
		
		BufferedImage loadedSheet = ImageIO.read(BlockType.class.getClassLoader().getResourceAsStream("res/sprite_"+path+"_01.png"));
		biRepresentation[mainID] = new BufferedImage(32, 32, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = biRepresentation[mainID].getGraphics();
		g.drawImage(loadedSheet, 0, 0, null);
		g.dispose();
		
		stringRepresentation[mainID] = path;
		
		for(int i = 0; i<VARIATIONSPERTEXTURE; i++){
			String localPath = "res/sprite_" + path + "_" + String.format("%02d", i+1) + ".png";
			File file1 = new File("src/"+localPath);		//ONLY FOR ECLIPSE!!!
			File file2 = new File(localPath);				//Jar Export from here (Fuck you Eclipse! ... jk, love u)
			JarEntry entry = null;
			if(!locationOfJar.equals("bin")){
				JarFile jar = new JarFile(locationOfJar);
				entry = jar.getJarEntry(localPath);
				jar.close();								//to here
			}
			availabletextures[mainID] = i;
			if(file1.exists()||file2.exists()||entry!=null){
				tex[VARIATIONSPERTEXTURE*mainID+i] = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(localPath));
			}else{
				break;
			}
		}
		
		if(mainID>=nextLoadNumber) nextLoadNumber = mainID + 1;
	}
	
	public static void loadBlockSheet(final String path) throws IOException{
		loadBlockSheet(path, nextLoadNumber);
	}
	
	
	
	public static boolean hasSameMainID(short a, short b){
		//Tests if 2 Blocks with maybe different "real IDs" have the same main ID.
		a/=VARIATIONSPERTEXTURE;
		b/=VARIATIONSPERTEXTURE;
		
		return a==b;
	}
	
	public static Texture getSpriteSheet(short tileID){
		return tex[tileID];
	}
	
	
	public static short getBlockID(final short mainID){
		if(mainID == 0)return 0;
		short b = (short)random.nextInt(availabletextures[mainID]);
		return getBlockID(mainID, b);
	}
	public static short getBlockID(final short mainID, final short variationID){
		//Returns a random available realID based on a mainID
		if(mainID == 0)return 0;
		if(variationID<0||variationID>availabletextures[mainID]) throw new IllegalArgumentException("variationID must be between 0 and "+(availabletextures[Session.leftClickTileIDBlock]));
		return (short) (mainID*VARIATIONSPERTEXTURE+variationID);
	}
	public static BufferedImage[] getBiRepresentations(){
		return biRepresentation;
	}
	public static String[] getStringRepresentations(){
		return stringRepresentation;
	}
}
