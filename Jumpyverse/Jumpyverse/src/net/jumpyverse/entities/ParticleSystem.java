package net.jumpyverse.entities;

import java.io.IOException;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.LWJGLCore.Screen;
import net.jumpyverse.editor.Session;
import net.jumpyverse.game.LevelData;

public class ParticleSystem{
	private Particle[] particles;
	private int size;
	private String particleName;
	private final static String PARTICLENAME_LEAF = "leaf";
	private final static String PARTICLENAME_SNOW = "snow";
	private final static String PARTICLENAME_RAIN = "raindrop";
	public final static String[] PARTICLENAMES = {PARTICLENAME_LEAF, PARTICLENAME_SNOW, PARTICLENAME_RAIN};
	private static Texture leaf1;
	private static Texture leaf2;
	private static Texture leaf3;
	private static Texture leaf4;
	
	private static Texture snow1;
	private static Texture snow2;
	private static Texture snow3;
	private static Texture snow4;
	
	private static Texture raindrop1;
	
	public ParticleSystem(int size, final String particleName){
		ini(size, particleName);
	}
	
	private void ini(){
		ini(size, particleName);
	}
	
	private void ini(int size, String particleName) {
		this.particleName = particleName;
		if(particleName != PARTICLENAME_LEAF && particleName != PARTICLENAME_SNOW && particleName !=PARTICLENAME_RAIN) throw new IllegalArgumentException("There is no ParticleName called "+particleName);
		
		this.size = size;
		
		size *= 9;
		particles = new Particle[size];
		for(int i = 0; i<size; i++){
			particles[i] = new Particle();
		}
	}

	public void update(float timeSinceLastFrame, LevelData leveldata) {
		for(int i = 0; i<particles.length; i++){
			particles[i].update(timeSinceLastFrame, leveldata);
		}
	}
	
	public void draw() {
		Brush.setColor(1, 1, 1, 1);
		for(int i = 0; i<particles.length; i++){
			particles[i].draw();
		}
	}
	
	
	public int getSize(){
		return size;
	}
	
	public void setSize(int size){
		if(size < 0) size = 0;
		if(size > 1024) size = 1024;
		this.size = size;
		ini();
	}
	
	public String getParticleName(){
		return particleName;
	}
	
	public void setParticleName(String particleName){
		this.particleName = particleName;
		ini();
	}
	
	
	
	public static void loadTexture(){
		try {
			leaf1 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/particle_leafe_01.png"));
			leaf2 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/particle_leafe_02.png"));
			leaf3 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/particle_leafe_03.png"));
			leaf4 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/particle_leafe_04.png"));
			
			snow1 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/particle_snowflake_01.png"));
			snow2 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/particle_snowflake_02.png"));
			snow3 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/particle_snowflake_03.png"));
			snow4 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/particle_snowflake_04.png"));
			
			raindrop1 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/particle_raindrop_01.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private class Particle{
		private Texture tex;
		private float rotation = 0;
		private final float ROTATIONSPEED;
		private int width;
		private int height;
		private float x;
		private float y;
		private final float speedX;
		private final float speedY;
		private boolean needsADraw = false;
		
		public Particle(){
			if(particleName == PARTICLENAME_LEAF){
				switch(Session.random.nextInt(4)){
				case(0):
					tex = leaf1;
					break;
				case(1):
					tex = leaf2;
					break;
				case(2):
					tex = leaf3;
					break;
				case(3):
					tex = leaf4;
					break;
				}
				width = 32;
				height = 32;
				
				x = Session.random.nextFloat()*(Screen.width * 3) - Screen.width;
				y = Session.random.nextFloat()*(Screen.height * 3) - Screen.height;
				speedX = Session.random.nextFloat()*1000 - 500;
				speedY = Session.random.nextFloat()*500 + 500;
				ROTATIONSPEED = Session.random.nextFloat()*90 - 45;
				rotation = Session.random.nextFloat()*360;
			}else if(particleName == PARTICLENAME_SNOW){
				switch(Session.random.nextInt(2)){
				case(0):
					tex = snow1;
					break;
				case(1):
					tex = snow2;
					break;
				}
				width = 8;
				height = 8;
				
				x = Session.random.nextFloat()*(Screen.width * 3) - Screen.width;
				y = Session.random.nextFloat()*(Screen.height * 3) - Screen.height;
				speedX = Session.random.nextFloat()*1000 - 500;
				speedY = Session.random.nextFloat()*500 + 500;
				ROTATIONSPEED = Session.random.nextFloat()*90 - 45;
				rotation = Session.random.nextFloat()*360;
			}else if(particleName == PARTICLENAME_RAIN){
				tex = raindrop1;
				
				width = 8;
				height = 16;
				
				x = Session.random.nextFloat()*(Screen.width * 3) - Screen.width;
				y = Session.random.nextFloat()*(Screen.height * 3) - Screen.height;
				speedX = Session.random.nextFloat()*100 - 50;
				speedY = Session.random.nextFloat()*2000 + 2000;
				ROTATIONSPEED = 0;
				rotation = 0;
			}else{
				throw new IllegalArgumentException("There is no ParticleName called "+particleName);
			}
		}
		
		public void update(final float timeSinceLastFrame, final LevelData leveldata) {
			x += speedX * timeSinceLastFrame / 10;
			y += speedY * timeSinceLastFrame / 10;
			
			x += Session.camDeltaX;
			y += Session.camDeltaY;
			
			if(y < -Screen.height)		y += Screen.height * 3;
			if(y > Screen.height * 2)	y -= Screen.height * 3;
			if(x < -Screen.width)		x += Screen.width * 3;
			if(x > Screen.width * 2)	x -= Screen.width * 3;
			
			rotation += ROTATIONSPEED * timeSinceLastFrame;
			if(rotation < 0) rotation+=360;
			if(rotation > 360) rotation -= 360;
			
			if(x>-width && x<Screen.width && y>-height && y<Screen.height)needsADraw = true;
			else needsADraw = false;
		}
		public void draw() {
			if(needsADraw) Brush.drawImage(tex, x, y, width, height, rotation);
		}
	}
}
