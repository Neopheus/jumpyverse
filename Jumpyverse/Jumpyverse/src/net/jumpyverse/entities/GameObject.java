package net.jumpyverse.entities;

import java.awt.Rectangle;

import net.jumpyverse.LWJGLCore.Screen;
import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.interactives.InteractiveObject;
import net.jumpyverse.game.LevelData;

public abstract class GameObject implements Cloneable{
	protected float x;
	protected float y;
	protected int width;
	protected int height;
	protected float speedX = 0;
	protected float speedY = 0;
	protected Rectangle bounding;
	protected boolean needsADraw = true;
	
	public abstract void update(final float timeSinceLastFrame, final LevelData leveldata);
	public abstract void draw();
	
	public float getX(){
		return x;
	}
	public float getY(){
		return y;
	}
	public int getWidth(){
		return width;
	}
	public int getHeight(){
		return height;
	}
	public float getSpeedX(){
		return speedX;
	}
	public float getSpeedY(){
		return speedY;
	}
	
	public boolean doesNeedADraw(){
		return needsADraw;
	}
	
	public void calculateNeedDraw(){
		if(x - Session.camX > -width && x  - Session.camX< Screen.width && y - Session.camY > -height && y - Session.camY < Screen.height) needsADraw = true;
		else needsADraw = false;
	}
	
	public void setX(final float x){
		this.x = x;
		if(bounding!=null){
			bounding.x = (int)x;
		}
	}
	public void setY(final float y){
		this.y = y;
		if(bounding != null){
			bounding.y = (int)y;
		}
	}
	public void setSpeedX(final float speedX){
		this.speedX = speedX;
	}
	public void setSpeedY(final float speedY){
		this.speedY = speedY;
	}
	public void addX(final float x){
		setX(this.x + x);
	}
	public void addY(final float y){
		setY(this.y + y);
	}
	public void addSpeedX(final float speedX){
		this.speedX += speedX;
	}
	public void addSpeedY(final float speedY){
		this.speedY += speedY;
	}
	
	public boolean intersects(final GameObject go){
		return bounding.intersects(go.bounding);
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	
	public void remove(){
		if(this instanceof EnemyObject){
			Session.tempLevel.removeEnemy((EnemyObject)this);
		}else if(this instanceof InteractiveObject){
			Session.tempLevel.removeInter((InteractiveObject)this);
		}else if(this instanceof DecoObject){
			Session.tempLevel.removeDeco((DecoObject)this);
		}else{
			throw new IllegalArgumentException("You can not remove a object of type "+this.getClass().getName());
		}
	}
	
}
