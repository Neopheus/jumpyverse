package net.jumpyverse.entities;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.LWJGLCore.Screen;
import net.jumpyverse.editor.Session;
import net.jumpyverse.game.LevelData;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class BackgroundType {
	private static Texture[] looks = new Texture[1024];
	private static BufferedImage[] biRepresentation = new BufferedImage[looks.length];
	private static String[] stringRepresentation = new String[looks.length];
	private static int nextLoadNumber = 1;
	
	public static void loadBackgroundTexture(final String path, final int mainID) throws IOException{
		//mainID = the reference to a new Background image.
		//I STRONGLY RECOMMEND THAT YOU DON'T USE THIS METHOD IF YOU DONT KNOW WHAT IT DOES! USE loadBackgroundTexte(String) INSTEAD!
		if(mainID<0||mainID>looks.length-1) throw new IllegalArgumentException("mainID must be between 0 and" + (looks.length-1));
		
		BufferedImage loadedSheet = ImageIO.read(BlockType.class.getClassLoader().getResourceAsStream("res/background_"+path+".png"));
		biRepresentation[mainID] = new BufferedImage(32, 32, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = biRepresentation[mainID].getGraphics();
		g.drawImage(loadedSheet, 0, 0, null);
		g.dispose();
		
		stringRepresentation[mainID] = path;
		
		
		try {
			looks[mainID] = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/background_" + path + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(mainID>=nextLoadNumber){
			nextLoadNumber = mainID+1;
		}
	}
	
	public static void loadBackgroundTexture(final String path) throws IOException{
		loadBackgroundTexture(path, nextLoadNumber);
	}
	
	public static void draw(final int mainID){
		float posX = -((Session.camX*Session.tempLevel.backgroundScrollFactor)%(4096));
		
		float ratio = (Session.camY)/(Session.tempLevel.getHeight()*LevelData.TILESIZE);
		float posY = ((Screen.height-1024) * ratio);
		
		Brush.drawImage(looks[mainID], posX,		posY, 4096, 1024);
		Brush.drawImage(looks[mainID], posX+4096,	posY, 4096, 1024);
		Brush.drawImage(looks[mainID], posX-4096, posY, 4096, 1024);
	}
	
	public static BufferedImage[] getBiRepresentations(){
		return biRepresentation;
	}
	public static String[] getStringRepresentations(){
		return stringRepresentation;
	}
}
