package net.jumpyverse.entities.interactives;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JPanel;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.GameObject;
import net.jumpyverse.entities.Player;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.gui.GoalPanel;
import net.jumpyverse.utils.ImageHelper;

public class Goal extends InteractiveObject{
	public final static short mainID = 2;
	
	/*
	 * JPANEL ATTRIBUTES FROM HERE
	 */
	
	public int EXITCODE = 0;
	
	/*
	 * TO HERE
	 */
	
	private static Texture flag;
	public static BufferedImage biRepresentation;
	
	static{
		try {
			flag = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/sprite_flag.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		biRepresentation = new BufferedImage(32, 32, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = biRepresentation.getGraphics();

		g.drawImage(ImageHelper.loadImage("res/sprite_flag.png"), 0, 0, null);
		
		g.dispose();
	}
	
	public Goal(){
		this.width = 32;
		this.height = 64;
		bounding = new Rectangle((int)x, (int)y, width, height);
	}
	
	
	@Override
	public void interact(final float timeSinceLastFrame, final GameObject target) {
		if(target instanceof Player){
			Player p = (Player) target;
			p.winning();
		}
	}

	@Override
	public JPanel getObjectPanel() {
		return new GoalPanel();
	}

	@Override
	public short getID() {
		return mainID;
	}

	@Override
	public void update(final float timeSinceLastFrame, final LevelData leveldata) {
		//DO NOTHING
	}

	@Override
	public void draw() {
		Brush.drawImage(flag, x-Session.camX, y-Session.camY, width, height);
	}

}
