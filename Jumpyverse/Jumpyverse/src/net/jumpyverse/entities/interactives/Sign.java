package net.jumpyverse.entities.interactives;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JPanel;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.GameObject;
import net.jumpyverse.entities.Player;
import net.jumpyverse.entities.interactives.Pill.ChangeableAttributes;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.gui.PillPanel;
import net.jumpyverse.gui.SignPanel;
import net.jumpyverse.utils.ImageHelper;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Sign extends InteractiveObject{
	public final static short mainID = 7;
	
	/*
	 * JPANEL ATTRIBUTES FROM HERE
	 */
	public String msg = "";
	/*
	 * TO HERE
	 */
	
	private float timeToShow = 0;
	
	private static Texture checkpoint;
	public static BufferedImage biRepresentation;
	
	static{
		try {
			checkpoint = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_sign_01.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		biRepresentation = new BufferedImage(32, 32, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = biRepresentation.getGraphics();

		g.drawImage(ImageHelper.loadImage("res/interactive_sign_01.png"), 0, 0, null);
		
		g.dispose();
	}
	
	public Sign(){
		this.width = 32;
		this.height = 32;
		bounding = new Rectangle((int)x, (int)y, width, height);
	}
	
	@Override
	public void interact(final float timeSinceLastFrame, final GameObject target) {
		if(target instanceof Player){
			Player p = (Player) target;
			timeToShow = 2;
		}
	}

	@Override
	public JPanel getObjectPanel() {
		return new SignPanel();
	}

	@Override
	public short getID() {
		return mainID;
	}

	@Override
	public void update(final float timeSinceLastFrame, final LevelData leveldata) {
		timeToShow -= timeSinceLastFrame;
	}

	@Override
	public void draw() {
		Brush.drawImage(checkpoint, (int)(x - Session.camX), (int)(y - Session.camY), width, height);
		
		if(timeToShow > 0){
			final int lettersize = 10;
			final int rowlength = 380 / lettersize;
			final int rows = msg.length() / rowlength + 1;
			System.out.println(rows);
			Brush.setColor(0, 0, 0, timeToShow);
			Brush.fillRect(50, 50, 400, rows * lettersize + 20);
			
			Brush.setColor(1, 1, 1, timeToShow);
			Brush.drawString(msg, "visitor", 60, 60, lettersize, lettersize, rowlength);
		}
	}
	
	public enum ChangeableAttributes{
		maxspeedwalk, maxspeedrun, acceleration, airacceleration, jumppower
	}
}