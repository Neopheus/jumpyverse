package net.jumpyverse.entities.interactives;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JPanel;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.GameObject;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.gui.PortalPanel;
import net.jumpyverse.utils.ImageHelper;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Portal extends InteractiveObject{
	public final static short mainID = 4;
	
	private static Texture[] portal = new Texture[17];
	public static BufferedImage biRepresentation;
	
	/*
	 * JPANEL ATTRIBUTES FROM HERE
	 */
	public int DESTINATIONX = 5;
	public int DESTINATIONY = 5;
	/*
	 * TO HERE
	 */
	
	private float animationTime = 0;
	private final static float ANIMATIONTIME = 1;
	
	static{
		for(int i = 0; i<portal.length; i++){
			try {
				portal[i] = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_portal_"+ String.format("%02d", i+1)+".png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		biRepresentation = new BufferedImage(32, 32, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = biRepresentation.getGraphics();

		g.drawImage(ImageHelper.loadImage("res/interactive_portal_01.png"), 0, -16, null);
		
		g.dispose();
	}
	
	public Portal(){
		this.width = 32;
		this.height = 64;
		bounding = new Rectangle((int)x, (int)y, width, height);
	}
	
	@Override
	public void interact(final float timeSinceLastFrame, final GameObject target) {
		target.setX(DESTINATIONX * LevelData.TILESIZE);
		target.setY(DESTINATIONY * LevelData.TILESIZE);
	}

	@Override
	public JPanel getObjectPanel() {
		return new PortalPanel();
	}

	@Override
	public short getID() {
		return mainID;
	}

	@Override
	public void update(final float timeSinceLastFrame, final LevelData leveldata) {
		animationTime += timeSinceLastFrame;
		if(animationTime>ANIMATIONTIME) animationTime-=ANIMATIONTIME;
	}

	@Override
	public void draw() {
		for(int i = 1; i<=portal.length; i++){
			if(animationTime<ANIMATIONTIME*i/(portal.length)){
				Brush.drawImage(portal[i-1], (int)(x - Session.camX), (int)(y - Session.camY), width, height);
				break;
			}
		}
	}

}
