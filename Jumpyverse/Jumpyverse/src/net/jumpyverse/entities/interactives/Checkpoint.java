package net.jumpyverse.entities.interactives;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JPanel;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.GameObject;
import net.jumpyverse.entities.Player;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.utils.ImageHelper;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Checkpoint extends InteractiveObject{
	public final static short mainID = 5;
	
	private static Texture checkpoint_checked;
	private static Texture checkpoint_unchecked;
	public static BufferedImage biRepresentation;
	
	private boolean checked = false;
	
	static{
		try {
			checkpoint_checked = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_checkpoint_checked_01.png"));
			checkpoint_unchecked = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_checkpoint_unchecked_01.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		biRepresentation = new BufferedImage(32, 32, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = biRepresentation.getGraphics();

		g.drawImage(ImageHelper.loadImage("res/interactive_checkpoint_unchecked_01.png"), 0, 0, null);
		
		g.dispose();
	}
	
	public Checkpoint(){
		this.width = 32;
		this.height = 32;
		bounding = new Rectangle((int)x, (int)y, width, height);
	}
	
	@Override
	public void interact(final float timeSinceLastFrame, final GameObject target) {
		if(!checked && target instanceof Player){
			checked = true;
			Session.respawnState = Session.tempLevel.clone();
		}
	}

	@Override
	public JPanel getObjectPanel() {
		return null;
	}

	@Override
	public short getID() {
		return mainID;
	}

	@Override
	public void update(final float timeSinceLastFrame, final LevelData leveldata) {
		//DO NOTHING
	}

	@Override
	public void draw() {
		Texture tex = null;
		if(checked) tex = checkpoint_checked;
		else tex = checkpoint_unchecked;
		
		Brush.drawImage(tex, (int)(x - Session.camX), (int)(y - Session.camY), width, height);
	}
}
