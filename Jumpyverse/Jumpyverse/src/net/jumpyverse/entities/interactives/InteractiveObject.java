package net.jumpyverse.entities.interactives;

import javax.swing.JPanel;

import net.jumpyverse.entities.GameObject;

public abstract class InteractiveObject extends GameObject{
	public abstract void interact(final float timeSinceLastFrame, final GameObject target);
	public abstract JPanel getObjectPanel();
	
	public abstract short getID();
}
