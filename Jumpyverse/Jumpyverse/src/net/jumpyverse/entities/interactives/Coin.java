package net.jumpyverse.entities.interactives;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JPanel;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.GameObject;
import net.jumpyverse.entities.Player;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.utils.ImageHelper;

public class Coin extends InteractiveObject{
	public final static short mainID = 3;
	
	private final float ANIMATIONTIME_ROTATING = 0.5f + Session.random.nextFloat() / 10;
	private float animationTime = Session.random.nextFloat() * ANIMATIONTIME_ROTATING;
	
	private static Texture coin1;
	private static Texture coin2;
	private static Texture coin3;
	private static Texture coin4;
	private static Texture coin5;
	private static Texture coin6;
	private static Texture coin7;
	private static Texture coin8;
	private static Texture coin9;
	private static Texture coin10;
	public static BufferedImage biRepresentation;
	
	static{
		try {
			coin1 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_coin_01.png"));
			coin2 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_coin_02.png"));
			coin3 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_coin_03.png"));
			coin4 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_coin_04.png"));
			coin5 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_coin_05.png"));
			coin6 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_coin_06.png"));
			coin7 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_coin_07.png"));
			coin8 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_coin_08.png"));
			coin9 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_coin_09.png"));
			coin10 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_coin_10.png"));

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		biRepresentation = new BufferedImage(32, 32, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = biRepresentation.getGraphics();

		g.drawImage(ImageHelper.loadImage("res/interactive_coin_02.png"), 8, 8, null);
		
		g.dispose();
	}
	
	public Coin(){
		this.width = 16;
		this.height = 16;
		bounding = new Rectangle((int)x, (int)y, width, height);
	}
	
	@Override
	public void interact(final float timeSinceLastFrame, final GameObject target) {
		if(target instanceof Player){
			Session.tempLevel.getPlayer().addPoints(100);
			Session.tempLevel.removeInter(this);
		}
	}

	@Override
	public JPanel getObjectPanel() {
		return null;
	}

	@Override
	public short getID() {
		return mainID;
	}

	@Override
	public void update(final float timeSinceLastFrame, final LevelData leveldata) {
		animationTime += timeSinceLastFrame;
		if(animationTime > ANIMATIONTIME_ROTATING) animationTime -= ANIMATIONTIME_ROTATING;
	}

	@Override
	public void draw() {
		Texture drawTex = coin1;
		
		if(animationTime>ANIMATIONTIME_ROTATING*9f/10f)
			drawTex = coin10;
		else if(animationTime>ANIMATIONTIME_ROTATING*8f/10f)
			drawTex = coin9;
		else if(animationTime>ANIMATIONTIME_ROTATING*7f/10f)
			drawTex = coin8;
		else if(animationTime>ANIMATIONTIME_ROTATING*6f/10f)
			drawTex = coin7;
		else if(animationTime>ANIMATIONTIME_ROTATING*5f/10f)
			drawTex = coin6;
		else if(animationTime>ANIMATIONTIME_ROTATING*4f/10f)
			drawTex = coin5;
		else if(animationTime>ANIMATIONTIME_ROTATING*3f/10f)
			drawTex = coin4;
		else if(animationTime>ANIMATIONTIME_ROTATING*2f/10f)
			drawTex = coin3;
		else if(animationTime>ANIMATIONTIME_ROTATING*1f/10f)
			drawTex = coin2;
		else
			drawTex = coin1;
		
		Brush.drawImage(drawTex, (int)(x - Session.camX), (int)(y - Session.camY), width, height);
	}

}
