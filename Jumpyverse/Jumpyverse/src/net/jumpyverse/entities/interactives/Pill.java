package net.jumpyverse.entities.interactives;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JPanel;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.GameObject;
import net.jumpyverse.entities.Player;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.gui.PillPanel;
import net.jumpyverse.utils.ImageHelper;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Pill extends InteractiveObject{
	public final static short mainID = 6;
	
	/*
	 * JPANEL ATTRIBUTES FROM HERE
	 */
	public int VALUE = 100;
	public ChangeableAttributes att = ChangeableAttributes.jumppower;
	/*
	 * TO HERE
	 */
	
	
	private static Texture pillUpperHalf;
	private static Texture pillLowerHalf;
	private static Texture pillShine1;
	private static Texture pillShine2;
	private static Texture pillShine3;
	public static BufferedImage biRepresentation;
	
	private final float ANIMATIONTIME = 0.5f;
	private float animationTime = 0;
	
	static{
		try {
			pillUpperHalf = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_pill_upperHalf_01.png"));
			pillLowerHalf = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_pill_lowerHalf_01.png"));
			pillShine1 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_pill_shine_01.png"));
			pillShine2 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_pill_shine_02.png"));
			pillShine3 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/interactive_pill_shine_03.png"));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		biRepresentation = new BufferedImage(32, 32, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = biRepresentation.getGraphics();

		g.drawImage(ImageHelper.loadImage("res/interactive_pill_upperHalf_01.png"), 0, 0, null);
		g.drawImage(ImageHelper.loadImage("res/interactive_pill_lowerHalf_01.png"), 0, 0, null);
		
		g.dispose();
	}
	
	public Pill(){
		this.width = 32;
		this.height = 32;
		bounding = new Rectangle((int)x, (int)y, width, height);
	}
	
	@Override
	public void interact(final float timeSinceLastFrame, final GameObject target) {
		if(target instanceof Player){
			Player p = (Player) target;
			
			if(att == ChangeableAttributes.maxspeedwalk)
				p.MAXSPEEDWALK += VALUE;
			else if(att == ChangeableAttributes.maxspeedrun)
				p.MAXSPEEDRUN += VALUE;
			else if(att == ChangeableAttributes.acceleration)
				p.ACCELERATION += VALUE;
			else if(att == ChangeableAttributes.airacceleration)
				p.AIRACCELERATION += VALUE;
			else if(att == ChangeableAttributes.jumppower)
				p.JUMPPOWER += VALUE;
			else if(att == ChangeableAttributes.extraJumps)
				p.EXTRAJUMPS += VALUE;
			
			Session.tempLevel.removeInter(this);
		}
	}

	@Override
	public JPanel getObjectPanel() {
		return new PillPanel();
	}

	@Override
	public short getID() {
		return mainID;
	}

	@Override
	public void update(final float timeSinceLastFrame, final LevelData leveldata) {
		animationTime += timeSinceLastFrame;
		if(animationTime > ANIMATIONTIME) animationTime-=ANIMATIONTIME;
	}

	@Override
	public void draw() {
		if(att == ChangeableAttributes.maxspeedwalk)		Brush.setColor(1f, 1f, 0f, 1f);
		else if(att == ChangeableAttributes.maxspeedrun)	Brush.setColor(1f, 0.5f, 0f, 1f);
		else if(att == ChangeableAttributes.acceleration)	Brush.setColor(1f, 0f, 0f, 1f);
		else if(att == ChangeableAttributes.airacceleration)Brush.setColor(1f, 0f, 1f, 1f);
		else if(att == ChangeableAttributes.jumppower)		Brush.setColor(0f, 0.82f, 0.82f, 1f);
		else if(att == ChangeableAttributes.extraJumps)		Brush.setColor(0f, 1f, 0f, 1f);
		
		Texture shine = null;
		if(animationTime < ANIMATIONTIME/6){
			//DO NOTHING
		}else if(animationTime < ANIMATIONTIME/6*2){
			shine = pillShine1;
		}else if(animationTime < ANIMATIONTIME/6*3){
			shine = pillShine2;
		}else if(animationTime < ANIMATIONTIME/6*4){
			shine = pillShine3;
		}else if(animationTime < ANIMATIONTIME/6*5){
			shine = pillShine2;
		}else if(animationTime < ANIMATIONTIME){
			shine = pillShine1;
		}
		if(shine != null){
			Brush.drawImage(shine, (int)(x - Session.camX), (int)(y - Session.camY), width, height);
		}
		Brush.drawImage(pillUpperHalf, (int)(x - Session.camX), (int)(y - Session.camY), width, height);
		Brush.setColor(1, 1, 1, 1);
		Brush.drawImage(pillLowerHalf, (int)(x - Session.camX), (int)(y - Session.camY), width, height);
	}
	
	public enum ChangeableAttributes{
		maxspeedwalk, maxspeedrun, acceleration, airacceleration, jumppower, extraJumps
	}
}