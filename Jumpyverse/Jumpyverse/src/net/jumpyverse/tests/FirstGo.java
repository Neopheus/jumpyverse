package net.jumpyverse.tests;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.LWJGLCore.Screen;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class FirstGo {
	public static Texture tex;
	public static void main(String[] args) throws IOException {
		try {
			Display.setDisplayMode(new DisplayMode(Screen.width, Screen.height));
			Display.create();
			Display.setVSyncEnabled(true);
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		BufferedImage bi = new BufferedImage(64, 64, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = bi.getGraphics();
		Color transparent = new Color(0, 0, 0, 0);
		for(int i = 0; i<64; i++){
			for(int k = 0; k<64; k++){
				bi.setRGB(i, k, transparent.getRGB());
			}
		}
		g.setColor(new Color(1, 1, 1, 0.5f));
		g.fillRect(0, 0, 64, 64);
		g.setColor(Color.BLACK);
		g.drawLine(10, 10, 60, 60);
		g.drawOval(0, 0, 64, 64);
		
		int imageID = Brush.loadTexture(bi);
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);               
        
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);          
        
        	// enable alpha blending
        	GL11.glEnable(GL11.GL_BLEND);
        	GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        
        	GL11.glViewport(0,0,Screen.width,Screen.height);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);

		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, Screen.width, Screen.height, 0, 1, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		
		tex = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/dirt_0000 Kopie.jpg"));
		
		while(!Display.isCloseRequested()){
			Brush.fillRect(0, 0, 1000, 1000);
			Brush.setColor(0, 0, 1, 1);
			Brush.fillRect(50, 50, 200, 200);
			Brush.setColor(1, 1, 1, 1);
			Brush.drawImage(imageID, 10, 10, 100, 64);
			Brush.drawImage(tex, 100, 10, 100, 100);
			
			
			Display.update();
		}
		
		Display.destroy();
	}
}
