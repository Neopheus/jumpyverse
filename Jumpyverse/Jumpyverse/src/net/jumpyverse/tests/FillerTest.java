package net.jumpyverse.tests;

public class FillerTest {
	
	private static void fill(int[][] ground, int x, int y, int oldValue, int newValue){
		if(x<0||y<0||x>ground.length-1||y>ground[0].length-1) return;
		
		
		
		setTile(ground, x, y, newValue);

		//print(ground);
		
		
		if(getTile(ground, x-1, y) == oldValue) fill(ground, x-1, y, oldValue, newValue);
		if(getTile(ground, x+1, y) == oldValue) fill(ground, x+1, y, oldValue, newValue);
		if(getTile(ground, x, y-1) == oldValue) fill(ground, x, y-1, oldValue, newValue);
		if(getTile(ground, x, y+1) == oldValue) fill(ground, x, y+1, oldValue, newValue);
		
	}
	
	private static void setTile(int[][] ground, int x, int y, int value){
		if(x<0||y<0||x>ground.length-1||y>ground[0].length-1) return;
		
		ground[x][y] = value;
	}
	
	private static int getTile(int[][] ground, int x, int y){
		if(x<0||y<0||x>ground.length-1||y>ground[0].length-1) return 0;
		
		return ground[x][y];
	}
	
	private static void print(int[][] ground){
		for(int i = 0; i<ground.length; i++){
			for(int k = 0; k<ground[i].length; k++){
				System.out.print("["+getTile(ground, k, i)+"]");
			}
			System.out.println();
		}
		
		System.out.println();
	}
	
	public static void main(String[] args) {
		int[][] ground = new int[1024][1024];
		
		
		long anfang = System.currentTimeMillis();
		fill(ground, 0, 0, 0, 2);
		long ende = System.currentTimeMillis();
		long diff = ende-anfang;
		System.out.println(diff);
		
		//print(ground);
	}
}
