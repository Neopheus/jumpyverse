package net.jumpyverse.exceptions;

public class UnsupportedFileException extends Exception{
	private static final long serialVersionUID = -5004920752476281565L;

	public UnsupportedFileException() {
		super("File is not supported!");
	}
}
