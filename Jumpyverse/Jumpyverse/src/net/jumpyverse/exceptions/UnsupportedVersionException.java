package net.jumpyverse.exceptions;

public class UnsupportedVersionException extends Exception{
	private static final long serialVersionUID = -5640490901305642568L;

	public UnsupportedVersionException() {
		super("Unsupported Version!");
	}
}
