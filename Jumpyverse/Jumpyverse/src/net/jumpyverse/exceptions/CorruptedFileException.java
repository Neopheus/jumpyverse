package net.jumpyverse.exceptions;

public class CorruptedFileException extends Exception{
	private static final long serialVersionUID = 2373341169183034376L;

	public CorruptedFileException(){
		super("File got corrupted!");
	}
}
