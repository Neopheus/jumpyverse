package net.jumpyverse.saversNLoaders;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import net.jumpyverse.game.LevelData;

public class Saver {
	public static void save(final File f, final LevelData ld) throws IOException{
		final LevelData savefile = ld.clone();
		Thread saverThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					File tempFile = new File(f.getAbsolutePath() + ".temp");
					Saver_0_0_3.save(tempFile, savefile);
					
			    	byte[] buffer = new byte[1024];
			    	 
			    	try{
			 
			    		FileOutputStream fos = new FileOutputStream(f);
			    		ZipOutputStream zos = new ZipOutputStream(fos);
			    		ZipEntry ze= new ZipEntry(f.getName()+".leveldata");
			    		zos.putNextEntry(ze);
			    		FileInputStream in = new FileInputStream(tempFile);
			 
			    		int len;
			    		while ((len = in.read(buffer)) > 0) {
			    			zos.write(buffer, 0, len);
			    		}
			 
			    		in.close();
			    		zos.closeEntry();
			 
			    		zos.close();
			    		
			    		tempFile.delete();
			    	}catch(IOException ex){
			    	   ex.printStackTrace();
			    	}

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		saverThread.start();
	}
	
	public static void save(String f, LevelData ld) throws IOException{
		save(new File(f), ld);
	}
}
