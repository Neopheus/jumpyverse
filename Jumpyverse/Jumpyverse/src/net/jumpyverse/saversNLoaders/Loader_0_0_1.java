package net.jumpyverse.saversNLoaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import net.jumpyverse.entities.DecoObject;
import net.jumpyverse.entities.ParticleSystem;
import net.jumpyverse.entities.Walker;
import net.jumpyverse.entities.interactives.Coin;
import net.jumpyverse.entities.interactives.Goal;
import net.jumpyverse.entities.interactives.Trampoline;
import net.jumpyverse.exceptions.CorruptedFileException;
import net.jumpyverse.exceptions.UnsupportedFileException;
import net.jumpyverse.exceptions.UnsupportedVersionException;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.utils.ByteReader;


/*
 * Da ich einmal einen Build rausgehauen habe ohne ein backup des loaders zu machen musste ich, um abw�rtskompatibel
 * zu bleiben das ganze aus der Jar extrahieren... dadurch ist die Formatierung aber sehr anders als normal ;)
 */












/*
 * #############################################
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * #############################################
 */
class Loader_0_0_1 {
  private static LevelData load(byte[] bytes)
    throws CorruptedFileException
  {
    ByteReader br = new ByteReader(bytes);
    try
    {
      if (bytes.length < 10) {
        throw new UnsupportedFileException();
      }
      checkMagicNumber(br.getString(10));
      checkVersion(br);
      
      int width = br.popInt();
      int height = br.popInt();
      int gravity = br.popInt();
      int backgroundImage = br.popInt();
      float backgroundScrollFactor = br.popFloat();
      float timeScale = br.popFloat();
      float windSpeed = br.popFloat();
      

      LevelData ld = new LevelData(width, height);
      
      ld.GRAVITY = gravity;
      ld.setBackgroundImage(backgroundImage);
      ld.backgroundScrollFactor = backgroundScrollFactor;
      ld.timeScale = timeScale;
      ld.windSpeed = windSpeed;
      
      ld.setParticle(new ParticleSystem(br.popInt(), ParticleSystem.PARTICLENAMES[br.popInt()]));
      
      ld.getPlayer().setX(br.popFloat());
      ld.getPlayer().setY(br.popFloat());
      ld.getPlayer().HEALTH = br.popInt();
      ld.getPlayer().DAMAGE = br.popInt();
      ld.getPlayer().MAXSPEEDHARD = br.popFloat();
      ld.getPlayer().MAXSPEEDWALK = br.popFloat();
      ld.getPlayer().MAXSPEEDRUN = br.popFloat();
      ld.getPlayer().MAXFALLSPEED = br.popFloat();
      ld.getPlayer().ACCELERATION = br.popFloat();
      ld.getPlayer().AIRACCELERATION = ld.getPlayer().ACCELERATION;
      ld.getPlayer().FRICTION = br.popFloat();
      ld.getPlayer().JUMPPOWER = br.popFloat();
      ld.getPlayer().MASS = br.popFloat();
      for (int i = 0; i < width; i++) {
        for (int k = 0; k < height; k++) {
          ld.setTile(i, k, 0, br.popShort());
        }
      }
      for (int i = 0; i < width; i++) {
        for (int k = 0; k < height; k++) {
          ld.setTile(i, k, 1, br.popShort());
        }
      }
      for (int i = 0; i < width; i++) {
        for (int k = 0; k < height; k++) {
          ld.setTile(i, k, 2, br.popShort());
        }
      }
      int decoSize = br.popInt();
      for (int i = 0; i < decoSize; i++) {
        ld.addDeco(new DecoObject(br.popFloat(), br.popFloat(), br.popInt()));
      }
      int interSize = br.popInt();
      for (int i = 0; i < interSize; i++)
      {
        int interID = br.popShort();
        switch (interID)
        {
        case 1: 
          Trampoline tramp = new Trampoline();
          
          tramp.setX(br.popFloat());
          tramp.setY(br.popFloat());
          tramp.setJumpPower(br.popFloat());
          tramp.setAngle(br.popFloat());
          
          ld.addInter(tramp);
          break;
        case 2: 
          Goal goal = new Goal();
          
          goal.setX(br.popFloat());
          goal.setY(br.popFloat());
          goal.EXITCODE = br.popInt();
          
          ld.addInter(goal);
          break;
        case 3: 
          Coin coin = new Coin();
          
          coin.setX(br.popFloat());
          coin.setY(br.popFloat());
          
          ld.addInter(coin);
          break;
        default: 
          System.err.println("Something went wrong with the loading of a Interactive Object. ID=" + interID);
          throw new CorruptedFileException();
        }
      }
      int enemySize = br.popInt();
      for (int i = 0; i < enemySize; i++)
      {
        int enemyID = br.popInt();
        switch (enemyID)
        {
        case 0: 
          float x = br.popFloat();
          float y = br.popFloat();
          float speedHard = br.popFloat();
          float speedWalk = br.popFloat();
          int health = br.popInt();
          
          Walker walker = new Walker(x, y);
          walker.MAXSPEEDHARD = speedHard;
          walker.MAXSPEEDWALK = speedWalk;
          walker.health = health;
          walker.HEALTH = health;
          
          ld.addEnemy(walker);
          break;
        default: 
          throw new CorruptedFileException();
        }
      }
      return ld;
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new CorruptedFileException();
    }
  }
  
  public static LevelData load(File f)
    throws IOException, UnsupportedFileException, UnsupportedVersionException, CorruptedFileException
  {
    try
    {
      byte[] bytes = new byte[(int)f.length()];
      
      FileInputStream fis = new FileInputStream(f);
      fis.read(bytes);
      fis.close();
      

      return load(bytes);
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new CorruptedFileException();
    }
  }
  
  private static void checkMagicNumber(String magicNumber)
    throws UnsupportedFileException
  {
    if (!magicNumber.equals("JSBMYPJump")) {
      throw new UnsupportedFileException();
    }
  }
  
  private static void checkVersion(ByteReader br)
    throws UnsupportedVersionException
  {
    int large = br.popInt();
    int major = br.popInt();
    int minor = br.popInt();
    if (large != 0) {
      throw new UnsupportedVersionException();
    }
    if (major != 0) {
      throw new UnsupportedVersionException();
    }
    if (minor != 1) {
      throw new UnsupportedVersionException();
    }
  }
}
