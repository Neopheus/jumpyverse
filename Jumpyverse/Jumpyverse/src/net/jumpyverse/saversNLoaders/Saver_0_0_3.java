package net.jumpyverse.saversNLoaders;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import net.jumpyverse.entities.DecoObject;
import net.jumpyverse.entities.EnemyObject;
import net.jumpyverse.entities.ParticleSystem;
import net.jumpyverse.entities.Player;
import net.jumpyverse.entities.Walker;
import net.jumpyverse.entities.interactives.Checkpoint;
import net.jumpyverse.entities.interactives.Coin;
import net.jumpyverse.entities.interactives.Goal;
import net.jumpyverse.entities.interactives.InteractiveObject;
import net.jumpyverse.entities.interactives.Pill;
import net.jumpyverse.entities.interactives.Portal;
import net.jumpyverse.entities.interactives.Sign;
import net.jumpyverse.entities.interactives.Trampoline;
import net.jumpyverse.enums.Directions;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.utils.ByteWriter;

public class Saver_0_0_3 {
	public static void save(final File f, final LevelData ld) throws IOException{
		ByteWriter bytes = new ByteWriter();
		
		writeMagicNumber(bytes);
		writeVersion(bytes, 0, 0, 3);
		writeMapInfo(bytes, ld);
		writeParticleSystem(bytes, ld.getParticle());
		writePlayerInfo(bytes, ld.getPlayer());
		
		writeGround(bytes, ld.getBackground());
		writeGround(bytes, ld.getPlayground());
		writeGround(bytes, ld.getForeground());
		
		writeDecoInfo(bytes, ld.getDecoSize());
		writeDeco(bytes, ld.getDecoList());
		
		writeInterInfo(bytes, ld.getInterSize());
		writeInter(bytes, ld.getInterList());
		
		writeEnemyInfo(bytes, ld.getEnemySize());
		writeEnemy(bytes, ld.getEnemyList());
		
		
		
		FileOutputStream fos = new FileOutputStream(f);
		fos.write(bytes.getBytes());
		fos.close();
	}
	
	private static void writeParticleSystem(ByteWriter bytes,
			ParticleSystem particle) {
		bytes.putInt(particle.getSize());
		for(int i = 0; i<ParticleSystem.PARTICLENAMES.length; i++){
			if(particle.getParticleName() == ParticleSystem.PARTICLENAMES[i]){
				bytes.putInt(i);
			}
		}
	}

	private static void writePlayerInfo(final ByteWriter bytes, final Player p) {
		bytes.putFloat(p.getX());
		bytes.putFloat(p.getY());
		bytes.putInt(p.HEALTH);
		bytes.putInt(p.DAMAGE);
		bytes.putFloat(p.MAXSPEEDHARD);
		bytes.putFloat(p.MAXSPEEDWALK);
		bytes.putFloat(p.MAXSPEEDRUN);
		bytes.putFloat(p.MAXFALLSPEED);
		bytes.putFloat(p.ACCELERATION);
		bytes.putFloat(p.AIRACCELERATION);
		bytes.putFloat(p.FRICTION);
		bytes.putFloat(p.JUMPPOWER);
		bytes.putInt(p.EXTRAJUMPS);
		bytes.putFloat(p.MASS);
	}

	private static void writeMagicNumber(final ByteWriter bytes){
		String magicNumber = "JSBMYPJump";
		bytes.writeByteArrayToList(magicNumber.getBytes());
	}
	
	
	private static void writeVersion(final ByteWriter bytes, final int large, final int major, final int minor){
		bytes.putInt(large);
		bytes.putInt(major);
		bytes.putInt(minor);
	}
	
	private static void writeMapInfo(final ByteWriter bytes, final LevelData ld){
		bytes.putInt(ld.getWidth());
		bytes.putInt(ld.getHeight());
		bytes.putInt(ld.GRAVITY);
		bytes.putInt(ld.getBackgroundImage());
		bytes.putFloat(ld.backgroundScrollFactor);
		bytes.putFloat(ld.timeScale);
		bytes.putFloat(ld.windSpeed);
	}
	
	private static void writeGround(final ByteWriter bytes, final short[][] ground){
		for(int i = 0; i<ground.length; i++){
			for(int k = 0; k<ground[i].length; k++){
				bytes.putShort(ground[i][k]);
			}
		}
	}
	
	private static void writeDecoInfo(final ByteWriter bytes, final int size){
		bytes.putInt(size);
	}
	
	private static void writeDeco(final ByteWriter bytes, final List<DecoObject> decos){
		for(int i = 0; i<decos.size(); i++){
			DecoObject d = decos.get(i);
			bytes.putFloat(d.getX());
			bytes.putFloat(d.getY());
			bytes.putInt(d.getID());
		}
	}
	
	private static void writeInterInfo(final ByteWriter bytes, final int size){
		bytes.putInt(size);
	}
	
	private static void writeInter(final ByteWriter bytes, final List<InteractiveObject> inters){
		for(int i = 0; i<inters.size(); i++){
			InteractiveObject io = inters.get(i);
			bytes.putShort(io.getID());
			bytes.putFloat(io.getX());
			bytes.putFloat(io.getY());
			switch(io.getID()){
			case(Trampoline.mainID):
				Trampoline tramp = (Trampoline) io;
				bytes.putFloat(tramp.getJumpPower());
				bytes.putFloat(tramp.getAngle());
				break;
			case(Goal.mainID):
				Goal goal = (Goal) io;
				bytes.putInt(goal.EXITCODE);
				break;
			case(Coin.mainID):
				Coin coin = (Coin) io;
				break;
			case(Portal.mainID):
				Portal portal = (Portal) io;
				bytes.putInt(portal.DESTINATIONX);
				bytes.putInt(portal.DESTINATIONY);
				break;
			case(Checkpoint.mainID):
				Checkpoint checkpoint = (Checkpoint) io;
				break;
			case(Pill.mainID):
				Pill pill = (Pill) io;
				bytes.putInt(pill.VALUE);
				
				byte batt = 0;
				
				Pill.ChangeableAttributes att = pill.att;
				
				if(att == Pill.ChangeableAttributes.maxspeedwalk){
					batt = 0;
				}else if(att == Pill.ChangeableAttributes.maxspeedrun){
					batt = 1;
				}else if(att == Pill.ChangeableAttributes.acceleration){
					batt = 2;
				}else if(att == Pill.ChangeableAttributes.airacceleration){
					batt = 3;
				}else if(att == Pill.ChangeableAttributes.jumppower){
					batt = 4;
				}else if(att == Pill.ChangeableAttributes.extraJumps){
					batt = 5;
				}else{
					System.err.println("Warning: Illegal Pill Attribute at Saver!");
				}
				
				bytes.putByte(batt);
				break;
			case(Sign.mainID):
				Sign sign = (Sign) io;
				bytes.putInt(sign.msg.length());
				bytes.putBytes(sign.msg.getBytes());
				break;
			default:
				System.err.println("Saver: Something went wrong at the Saver! Illegal Inter ID! "+io.getID());
				break;
			}
		}
	}
	
	private static void writeEnemyInfo(final ByteWriter bytes, final int size){
		bytes.putInt(size);
	}
	
	private static void writeEnemy(final ByteWriter bytes, final List<EnemyObject> enemys){
		for(int i = 0; i<enemys.size(); i++){
			EnemyObject eo = enemys.get(i);
			bytes.putFloat(eo.getID());
			bytes.putFloat(eo.getX());
			bytes.putFloat(eo.getY());
			switch(eo.getID()){
			case(Walker.mainID):
				Walker walker = (Walker) eo;
				bytes.putFloat(walker.MAXSPEEDHARD);
				bytes.putFloat(walker.MAXSPEEDWALK);
				bytes.putInt(walker.HEALTH);
				if(walker.getLookDirection() == Directions.right){
					bytes.putByte((byte)0);
				}else{
					bytes.putByte((byte)1);
				}
				break;
			default:
				System.err.println("Saver: Something went wrong at the Saver! Illegal Enemy ID!");
				break;
			}
		}
	}
}
