package net.jumpyverse.saversNLoaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import net.jumpyverse.entities.DecoObject;
import net.jumpyverse.entities.ParticleSystem;
import net.jumpyverse.entities.Walker;
import net.jumpyverse.entities.interactives.Checkpoint;
import net.jumpyverse.entities.interactives.Coin;
import net.jumpyverse.entities.interactives.Goal;
import net.jumpyverse.entities.interactives.Pill;
import net.jumpyverse.entities.interactives.Portal;
import net.jumpyverse.entities.interactives.Sign;
import net.jumpyverse.entities.interactives.Trampoline;
import net.jumpyverse.enums.Directions;
import net.jumpyverse.exceptions.CorruptedFileException;
import net.jumpyverse.exceptions.UnsupportedFileException;
import net.jumpyverse.exceptions.UnsupportedVersionException;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.utils.ByteReader;



/*
 * #############################################
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY LOADER!!!!    ###
 * #############################################
 */



public class Loader_0_0_2 {

	private static LevelData load(final byte[] bytes) throws CorruptedFileException{
		ByteReader br = new ByteReader(bytes);
		
		try{
			
			if(bytes.length<10){
				throw new UnsupportedFileException();
			}
			
			
			checkMagicNumber(br.getString(10));
			checkVersion(br);
			
			int width  = br.popInt();
			int height = br.popInt();
			int gravity = br.popInt();
			int backgroundImage = br.popInt();
			float backgroundScrollFactor = br.popFloat();
			float timeScale = br.popFloat();
			float windSpeed = br.popFloat();
			
			
			LevelData ld = new LevelData(width, height);
			
			ld.GRAVITY = gravity;
			ld.setBackgroundImage(backgroundImage);
			ld.backgroundScrollFactor = backgroundScrollFactor;
			ld.timeScale = timeScale;
			ld.windSpeed = windSpeed;
			
			ld.setParticle(new ParticleSystem(br.popInt(), ParticleSystem.PARTICLENAMES[br.popInt()]));
			
			ld.getPlayer().setX(br.popFloat());
			ld.getPlayer().setY(br.popFloat());
			ld.getPlayer().HEALTH = br.popInt();
			ld.getPlayer().DAMAGE = br.popInt();
			ld.getPlayer().MAXSPEEDHARD = br.popFloat();
			ld.getPlayer().MAXSPEEDWALK = br.popFloat();
			ld.getPlayer().MAXSPEEDRUN = br.popFloat();
			ld.getPlayer().MAXFALLSPEED = br.popFloat();
			ld.getPlayer().ACCELERATION = br.popFloat();
			ld.getPlayer().AIRACCELERATION = br.popFloat();
			ld.getPlayer().FRICTION = br.popFloat();
			ld.getPlayer().JUMPPOWER = br.popFloat();
			ld.getPlayer().EXTRAJUMPS = br.popInt();
			ld.getPlayer().MASS = br.popFloat();
			
			
			for(int i = 0; i<width; i++){
				for(int k = 0; k<height; k++){
					ld.setTile(i, k, LevelData.BACKGROUND, br.popShort());
				}
			}
			for(int i = 0; i<width; i++){
				for(int k = 0; k<height; k++){
					ld.setTile(i, k, LevelData.PLAYGROUND, br.popShort());
				}
			}
			for(int i = 0; i<width; i++){
				for(int k = 0; k<height; k++){
					ld.setTile(i, k, LevelData.FOREGROUND, br.popShort());
				}
			}
			
			int decoSize = br.popInt();
			
			for(int i = 0; i<decoSize; i++){
				ld.addDeco(new DecoObject(br.popFloat(), br.popFloat(), br.popInt()));
			}
			
			int interSize = br.popInt();
			for(int i = 0; i<interSize; i++){
				int interID = br.popShort();
				switch(interID){
				case(Trampoline.mainID):
					Trampoline tramp = new Trampoline();
					
					tramp.setX(br.popFloat());
					tramp.setY(br.popFloat());
					tramp.setJumpPower(br.popFloat());
					tramp.setAngle(br.popFloat());
					
					ld.addInter(tramp);
					break;
				case(Goal.mainID):
					Goal goal = new Goal();
					
					goal.setX(br.popFloat());
					goal.setY(br.popFloat());
					goal.EXITCODE = br.popInt();
					
					ld.addInter(goal);
					break;
				case(Coin.mainID):
					Coin coin = new Coin();
					
					coin.setX(br.popFloat());
					coin.setY(br.popFloat());
					
					ld.addInter(coin);
					break;
				case(Portal.mainID):
					Portal portal = new Portal();
					portal.setX(br.popFloat());
					portal.setY(br.popFloat());
					portal.DESTINATIONX = br.popInt();
					portal.DESTINATIONY = br.popInt();
					
					ld.addInter(portal);
					break;
				case(Checkpoint.mainID):
					Checkpoint checkpoint = new Checkpoint();
					checkpoint.setX(br.popFloat());
					checkpoint.setY(br.popFloat());
					
					ld.addInter(checkpoint);
					break;
				case(Pill.mainID):
					Pill pill = new Pill();
					pill.setX(br.popFloat());
					pill.setY(br.popFloat());
					pill.VALUE = br.popInt();
					
					byte att = br.popByte();
					
					switch(att){
					case(0):
						pill.att = Pill.ChangeableAttributes.maxspeedwalk;
						break;
					case(1):
						pill.att = Pill.ChangeableAttributes.maxspeedrun;
						break;
					case(2):
						pill.att = Pill.ChangeableAttributes.acceleration;
						break;
					case(3):
						pill.att = Pill.ChangeableAttributes.airacceleration;
						break;
					case(4):
						pill.att = Pill.ChangeableAttributes.jumppower;
						break;
					case(5):
						pill.att = Pill.ChangeableAttributes.extraJumps;
						break;
					default:
						System.err.println("Warning: Illegal Pill Attribute at Loader!");
						break;
					}
					
					ld.addInter(pill);
					break;
				case(Sign.mainID):
					Sign sign = new Sign();
					sign.setX(br.popFloat());
					sign.setY(br.popFloat());
					int length = br.popInt();
					sign.msg = new String(br.popBytes(length));
					ld.addInter(sign);
					break;
				default:
					System.err.println("Something went wrong with the loading of a Interactive Object. ID="+interID);
					throw new CorruptedFileException();
				}
			}
			
			int enemySize = br.popInt();
			for(int i = 0; i<enemySize; i++){
				int enemyID = br.popInt();
				switch(enemyID){
				case(Walker.mainID):
					
					float x = br.popFloat();
					float y = br.popFloat();
					float speedHard = br.popFloat();
					float speedWalk = br.popFloat();
					int health = br.popInt();
					byte lookDirection = br.popByte();
					
					Walker walker = new Walker(x, y);
					walker.MAXSPEEDHARD = speedHard;
					walker.MAXSPEEDWALK = speedWalk;
					walker.health = health;
					walker.HEALTH = health;
					if(lookDirection == 0){
						walker.setLookDirection(Directions.right);
					}else{
						walker.setLookDirection(Directions.left);
					}
					
					
					ld.addEnemy(walker);
					break;
				default:
					throw new CorruptedFileException();
				}
			}
			
			
			return ld;	
		}catch(Exception e){
			e.printStackTrace();
			throw new CorruptedFileException();
		}
	}
	
	
	public static LevelData load(final File f) throws IOException, UnsupportedFileException, UnsupportedVersionException, CorruptedFileException{
		try{
			byte[] bytes = new byte[(int) f.length()];
			
			FileInputStream fis = new FileInputStream(f);
			fis.read(bytes);
			fis.close();
			
			
			return load(bytes);
			
		}catch(Exception e){
			e.printStackTrace();
			throw new CorruptedFileException();
		}
	}
	
	private static void checkMagicNumber(final String magicNumber) throws UnsupportedFileException{
		if(!magicNumber.equals("JSBMYPJump")){
			throw new UnsupportedFileException();
		}
	}
	
	private static void checkVersion(final ByteReader br) throws UnsupportedVersionException{
		
		int large = br.popInt();
		int major = br.popInt();
		int minor = br.popInt();

		if(large!=0)throw new UnsupportedVersionException();
		if(major!=0)throw new UnsupportedVersionException();
		if(minor!=2)throw new UnsupportedVersionException();
	}
	
}
