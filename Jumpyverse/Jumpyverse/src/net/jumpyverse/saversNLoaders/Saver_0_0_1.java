package net.jumpyverse.saversNLoaders;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import net.jumpyverse.entities.DecoObject;
import net.jumpyverse.entities.EnemyObject;
import net.jumpyverse.entities.ParticleSystem;
import net.jumpyverse.entities.Player;
import net.jumpyverse.entities.Walker;
import net.jumpyverse.entities.interactives.Coin;
import net.jumpyverse.entities.interactives.Goal;
import net.jumpyverse.entities.interactives.InteractiveObject;
import net.jumpyverse.entities.interactives.Trampoline;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.utils.ByteWriter;



/*
 * Da ich einmal einen Build rausgehauen habe ohne ein backup des loaders zu machen musste ich, um abw�rtskompatibel
 * zu bleiben das ganze aus der Jar extrahieren... dadurch ist die Formatierung aber sehr anders als normal ;)
 */












/*
 * ############################################
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ###    DO NOT TOUCH! LEGACY SAVER!!!!    ###
 * ############################################
 */

class Saver_0_0_1 {
  public static void save(File f, LevelData ld)
    throws IOException
  {
    ByteWriter bytes = new ByteWriter();
    
    writeMagicNumber(bytes);
    writeVersion(bytes, 0, 0, 1);
    writeMapInfo(bytes, ld);
    writeParticleSystem(bytes, ld.getParticle());
    writePlayerInfo(bytes, ld.getPlayer());
    
    writeGround(bytes, ld.getBackground());
    writeGround(bytes, ld.getPlayground());
    writeGround(bytes, ld.getForeground());
    
    writeDecoInfo(bytes, ld.getDecoSize());
    writeDeco(bytes, ld.getDecoList());
    
    writeInterInfo(bytes, ld.getInterSize());
    writeInter(bytes, ld.getInterList());
    
    writeEnemyInfo(bytes, ld.getEnemySize());
    writeEnemy(bytes, ld.getEnemyList());
    


    FileOutputStream fos = new FileOutputStream(f);
    fos.write(bytes.getBytes());
    fos.close();
  }
  
  private static void writeParticleSystem(ByteWriter bytes, ParticleSystem particle)
  {
    bytes.putInt(particle.getSize());
    for (int i = 0; i < ParticleSystem.PARTICLENAMES.length; i++) {
      if (particle.getParticleName() == ParticleSystem.PARTICLENAMES[i]) {
        bytes.putInt(i);
      }
    }
  }
  
  private static void writePlayerInfo(ByteWriter bytes, Player p)
  {
    bytes.putFloat(p.getX());
    bytes.putFloat(p.getY());
    bytes.putInt(p.HEALTH);
    bytes.putInt(p.DAMAGE);
    bytes.putFloat(p.MAXSPEEDHARD);
    bytes.putFloat(p.MAXSPEEDWALK);
    bytes.putFloat(p.MAXSPEEDRUN);
    bytes.putFloat(p.MAXFALLSPEED);
    bytes.putFloat(p.ACCELERATION);
    bytes.putFloat(p.FRICTION);
    bytes.putFloat(p.JUMPPOWER);
    bytes.putFloat(p.MASS);
  }
  
  private static void writeMagicNumber(ByteWriter bytes)
  {
    String magicNumber = "JSBMYPJump";
    bytes.writeByteArrayToList(magicNumber.getBytes());
  }
  
  private static void writeVersion(ByteWriter bytes, int large, int major, int minor)
  {
    bytes.putInt(large);
    bytes.putInt(major);
    bytes.putInt(minor);
  }
  
  private static void writeMapInfo(ByteWriter bytes, LevelData ld)
  {
    bytes.putInt(ld.getWidth());
    bytes.putInt(ld.getHeight());
    bytes.putInt(ld.GRAVITY);
    bytes.putInt(ld.getBackgroundImage());
    bytes.putFloat(ld.backgroundScrollFactor);
    bytes.putFloat(ld.timeScale);
    bytes.putFloat(ld.windSpeed);
  }
  
  private static void writeGround(ByteWriter bytes, short[][] ground)
  {
    for (int i = 0; i < ground.length; i++) {
      for (int k = 0; k < ground[i].length; k++) {
        bytes.putShort(ground[i][k]);
      }
    }
  }
  
  private static void writeDecoInfo(ByteWriter bytes, int size)
  {
    bytes.putInt(size);
  }
  
  private static void writeDeco(ByteWriter bytes, List<DecoObject> decos)
  {
    for (int i = 0; i < decos.size(); i++)
    {
      DecoObject d = (DecoObject)decos.get(i);
      bytes.putFloat(d.getX());
      bytes.putFloat(d.getY());
      bytes.putInt(d.getID());
    }
  }
  
  private static void writeInterInfo(ByteWriter bytes, int size)
  {
    bytes.putInt(size);
  }
  
  private static void writeInter(ByteWriter bytes, List<InteractiveObject> inters)
  {
    for (int i = 0; i < inters.size(); i++)
    {
      InteractiveObject io = (InteractiveObject)inters.get(i);
      bytes.putShort(io.getID());
      bytes.putFloat(io.getX());
      bytes.putFloat(io.getY());
      Coin coin;
      switch (io.getID())
      {
      case 1: 
        Trampoline tramp = (Trampoline)io;
        bytes.putFloat(tramp.getJumpPower());
        bytes.putFloat(tramp.getAngle());
        break;
      case 2: 
        Goal goal = (Goal)io;
        bytes.putInt(goal.EXITCODE);
        break;
      case 3: 
        coin = (Coin)io;
        break;
      default: 
        System.err.println("Saver: Something went wrong at the Saver! Illegal Inter ID! " + io.getID());
      }
    }
  }
  
  private static void writeEnemyInfo(ByteWriter bytes, int size)
  {
    bytes.putInt(size);
  }
  
  private static void writeEnemy(ByteWriter bytes, List<EnemyObject> enemys)
  {
    for (int i = 0; i < enemys.size(); i++)
    {
      EnemyObject eo = (EnemyObject)enemys.get(i);
      bytes.putFloat(eo.getID());
      bytes.putFloat(eo.getX());
      bytes.putFloat(eo.getY());
      switch (eo.getID())
      {
      case 0: 
        Walker walker = (Walker)eo;
        bytes.putFloat(walker.MAXSPEEDHARD);
        bytes.putFloat(walker.MAXSPEEDWALK);
        bytes.putInt(walker.HEALTH);
        break;
      default: 
        System.err.println("Saver: Something went wrong at the Saver! Illegal Enemy ID!");
      }
    }
  }
}
