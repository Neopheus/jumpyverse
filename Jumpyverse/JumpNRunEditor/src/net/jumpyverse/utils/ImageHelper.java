package net.jumpyverse.utils;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;


public class ImageHelper {
	public static BufferedImage loadImage(String path){
		BufferedImage bi = null;
		try {
			bi = ImageIO.read(ImageHelper.class.getClassLoader().getResourceAsStream(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return bi;
	}
	
	
	public static ByteBuffer loadIconAsByteBuffer(String path, int width, int height) throws IOException {
	    BufferedImage image = loadImage(path);
	    
	    byte[] bytes = new byte[width * height * 4];	//*4 because RGBA
	    for (int x = 0; x < width; x++) {
	        for (int y = 0; y < height; y++) {
	            int rgbValue = image.getRGB(x, y);
	            for (int k = 0; k < 3; k++) // red, green, blue
	            	bytes[(y*width+x)*4 + k] = (byte)(((rgbValue>>(2-k)*8))&255);
	            bytes[(y*height+x)*4 + 3] = (byte)(((rgbValue>>(3)*8))&255); // alpha
	        }
	    } 
	    
	    ByteBuffer returnValue = ByteBuffer.wrap(bytes);
	    return returnValue;
	}
}
