package net.jumpyverse.utils;

public class BrotMath {
	public static float modulo(float value, float div){
		if(value>=0)return value%div;
		else{
			return ((value % div) + div ) % div;
		}
	}
}
