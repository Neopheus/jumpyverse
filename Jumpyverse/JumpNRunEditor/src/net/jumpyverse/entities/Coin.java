package net.jumpyverse.entities;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JPanel;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.editor.Session;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.utils.ImageHelper;

public class Coin extends InteractiveObject{
	public final static short mainID = 3;
	
	private static Texture coin;
	public static BufferedImage biRepresentation;
	
	static{
		try {
			coin = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/sprite_default.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		biRepresentation = new BufferedImage(32, 32, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = biRepresentation.getGraphics();

		g.drawImage(ImageHelper.loadImage("res/sprite_default.png"), 0, 0, null);
		
		g.dispose();
	}
	
	public Coin(){
		this.width = 16;
		this.height = 16;
		bounding = new Rectangle((int)x, (int)y, width, height);
	}
	
	@Override
	public void interact(float timeSinceLastFrame, GameObject target) {
		if(target instanceof Player){
			Session.tempLevel.getPlayer().addPoints(100);
			Session.tempLevel.removeInter(this);
		}
	}

	@Override
	public JPanel getObjectPanel() {
		return null;
	}

	@Override
	public short getID() {
		return mainID;
	}

	@Override
	public void update(float timeSinceLastFrame, LevelData leveldata) {
		//TODO Update Texture!
	}

	@Override
	public void draw() {
		Brush.drawImage(coin, x-Session.camX, y-Session.camY, width, height);
	}

}
