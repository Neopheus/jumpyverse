package net.jumpyverse.entities;

import java.awt.Rectangle;

import net.jumpyverse.editor.Session;
import net.jumpyverse.game.LevelData;

public abstract class GameObject implements Cloneable{
	protected float x;
	protected float y;
	protected int width;
	protected int height;
	protected float speedX = 0;
	protected float speedY = 0;
	protected Rectangle bounding;
	
	public abstract void update(float timeSinceLastFrame, LevelData leveldata);
	public abstract void draw();
	
	public float getX(){
		return x;
	}
	public float getY(){
		return y;
	}
	public int getWidth(){
		return width;
	}
	public int getHeight(){
		return height;
	}
	public float getSpeedX(){
		return speedX;
	}
	public float getSpeedY(){
		return speedY;
	}
	
	
	public void setX(float x){
		this.x = x;
		if(bounding!=null){
			bounding.x = (int)x;
		}
	}
	public void setY(float y){
		this.y = y;
		if(bounding != null){
			bounding.y = (int)y;
		}
	}
	public void setSpeedX(float speedX){
		this.speedX = speedX;
	}
	public void setSpeedY(float speedY){
		this.speedY = speedY;
	}
	public void addX(float x){
		setX(this.x + x);
	}
	public void addY(float y){
		setY(this.y + y);
	}
	public void addSpeedX(float speedX){
		this.speedX += speedX;
	}
	public void addSpeedY(float speedY){
		this.speedY += speedY;
	}
	
	public boolean intersects(GameObject go){
		return bounding.intersects(go.bounding);
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	
	public void remove(){
		if(this instanceof EnemyObject){
			Session.tempLevel.removeEnemy((EnemyObject)this);
		}else if(this instanceof InteractiveObject){
			Session.tempLevel.removeInter((InteractiveObject)this);
		}else if(this instanceof DecoObject){
			Session.tempLevel.removeDeco((DecoObject)this);
		}else{
			throw new IllegalArgumentException("You can not remove a object of type "+this.getClass().getName());
		}
	}
	
}
