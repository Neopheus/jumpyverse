package net.jumpyverse.entities;

import static net.jumpyverse.editor.Session.tempLevel;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.editor.Session;
import net.jumpyverse.enums.Animations;
import net.jumpyverse.enums.Directions;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.utils.ImageHelper;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Walker extends EnemyObject{
	private int damage = DAMAGE;
	
	
	/*
	 * JPANEL ATTRIBUTES FROM HERE
	 */
	public float MAXSPEEDHARD 	= 600f;
	public float MAXSPEEDWALK	= 200;
	public int   HEALTH			= 1;
	/*
	 * TO HERE
	 */
	
	public int health = HEALTH;
	
	public final float MAXFALLSPEED 	= 1000;
	public final float ACCELERATION 	= 1000;
	public final float MASS				= 100;
	
	
	private float accelerationX = 0;
		
	private float animationTime = 0;

	private static final float ANIMATIONTIME_STANDING = 3;
	private static final float ANIMATIONTIME_WALKING = 0.4f;
	private static final float ANIMATIONTIME_RUNNING = 0.2f;
	private static final float ANIMATIONTIME_JUMPING = 0.2f;
	private static final float ANIMATIONTIME_LANDING = 0.2f;
	private static final float ANIMATIONTIME_TAKENDAMAGE = 0.2f;
	
	public static Texture animation_tex_standing1 = null;
	private static Texture animation_tex_standing2 = null;
	
	private static Texture animation_tex_walking1 = null;
	private static Texture animation_tex_walking2 = null;
	private static Texture animation_tex_walking3 = null;
	private static Texture animation_tex_walking4 = null;
	private static Texture animation_tex_walking5 = null;
	private static Texture animation_tex_walking6 = null;
	private static Texture animation_tex_walking7 = null;
	private static Texture animation_tex_walking8 = null;
	
	private static Texture animation_tex_stopping1 = null;
	
	private static Texture animation_tex_jumping1 = null;
	private static Texture animation_tex_jumping2 = null;
	private static Texture animation_tex_jumping3 = null;
	private static Texture animation_tex_jumping4 = null;
	
	private static Texture animation_tex_landing1 = null;
	
	private static Texture animation_tex_takenDamage1 = null;
	
	private Directions lookDirection = Directions.right;
	
	private Animations animation = Animations.standing;
	
	private boolean isJumping = false;
	private boolean wasJumping = false;
	private boolean takenDamage = false;
	
	
	public static final short mainID = 0;
	
	public static BufferedImage biRepresentation;
	
	public Walker(float x, float y){
		this.x = x;
		this.y = y;
		this.height = 60;
		this.width = 30;
		bounding = new Rectangle((int)y, (int)y, width, height);
	}
	
	
	@Override
	public Walker clone() {
		Walker copy = new Walker(x, y);
		
		copy.MAXSPEEDHARD = MAXSPEEDHARD;
		copy.MAXSPEEDWALK = MAXSPEEDWALK;
		copy.health = health;
		copy.HEALTH = HEALTH; 
		
		return copy;
	}
	
	
	@Override
	public void setIsJumping(boolean isJumping){
		this.isJumping = isJumping;
	}

	
	@Override
	public void takeDamage(int dmg){
		health -= dmg;
		takenDamage = true;
		animationTime = 0;
		if(health <= 0){
			remove();
		}
	}

	
	@Override
	public void update(float timeSinceLastFrame, LevelData leveldata) {
		animationTime += timeSinceLastFrame;
		animation = Animations.standing;
		
		
		accelerationX = 0;
		speedY += Session.tempLevel.GRAVITY * MASS * timeSinceLastFrame;
		
		
		if(lookDirection == Directions.left){
			accelerationX -= ACCELERATION * timeSinceLastFrame;
		}
		if(lookDirection == Directions.right){
			accelerationX += ACCELERATION * timeSinceLastFrame;
		}	
		
		
		speedX += accelerationX;

		
		if(accelerationX!=0){
			animation = Animations.walking;
		}else if(accelerationX == 0 && speedX != 0){
			animation = Animations.stopping;
		}
		
		
		if(isJumping) {
			animation = Animations.jumping;
		}
		
		
		if(!isJumping && wasJumping){
			animation = Animations.landing;
		}
		
		if(takenDamage){
			animation = Animations.takenDamage;
		}
		
		
		/*
		 * ###############
		 * ###SPEEDCAPS###
		 * ###############
		 * 
		 */
		if(accelerationX != 0){
			if(speedX > MAXSPEEDWALK)
				speedX = MAXSPEEDWALK;
			if(speedX < -MAXSPEEDWALK)
				speedX = -MAXSPEEDWALK;
		}
		
		
		if(speedX > MAXSPEEDHARD){
			speedX = MAXSPEEDHARD;
		}
		
		if(speedX < -MAXSPEEDHARD){
			speedX = -MAXSPEEDHARD;
		}
		
		if(speedY > MAXFALLSPEED){
			speedY = MAXFALLSPEED;
		}
		
		
		x += speedX * timeSinceLastFrame;
		bounding.x = (int)x;
		int tileX = (int) (x / LevelData.TILESIZE);
		int tileY = (int) (y / LevelData.TILESIZE);
		
		
		//Calculate how many Blocks around this Entity to Check if there was a block Collision
		int tilesXToCheck = (int) (4 + Math.abs(speedX) * timeSinceLastFrame / LevelData.TILESIZE);
		int tilesYToCheck = (int) (4 + Math.abs(speedY) * timeSinceLastFrame / LevelData.TILESIZE);
		if(tilesXToCheck > 16) tilesXToCheck = 16;
		if(tilesYToCheck > 16) tilesYToCheck = 16;
		
		for(int i = tileX-tilesXToCheck; i<tileX+tilesXToCheck; i++){
			for(int k = tileY-tilesYToCheck; k<tileY+tilesYToCheck; k++){
				Rectangle block = leveldata.getBlock(i, k);
				if(block != null){
					if(bounding.intersects(block)){
						if(speedX>0){
							x = block.x - width;
							lookDirection = Directions.left;
						}else{
							x = block.x + LevelData.TILESIZE;
							lookDirection = Directions.right;
						}
						bounding.x = (int)x;
						tileX = (int) (x / LevelData.TILESIZE);
						speedX = 0;
					}
				}
			}
		}
		
		
		y += speedY * timeSinceLastFrame;
		bounding.y = (int)y;
		
		if(animationTime > ANIMATIONTIME_LANDING){
			wasJumping = false;
		}
		
		if(animationTime > ANIMATIONTIME_TAKENDAMAGE){
			takenDamage = false;
		}
		
		for(int i = tileX-tilesXToCheck; i<tileX+tilesXToCheck; i++){
			for(int k = tileY-tilesYToCheck; k<tileY+tilesYToCheck; k++){
				Rectangle block = leveldata.getBlock(i, k);
				if(block != null){
					if(bounding.intersects(block)){
						if(speedY>0){
							y = block.y - height;
							if(isJumping){
								wasJumping = true;
								animationTime = 0;
							}
							isJumping = false;
						}else{
							y = block.y + LevelData.TILESIZE;
						}
						bounding.y = (int)y;
						tileY = (int) (y / LevelData.TILESIZE);
						speedY = 0;
					}
				}
			}
		}
		
		
		//INTERACTIVES
		for(int i = 0; i<tempLevel.getInterSize(); i++){
			InteractiveObject io = tempLevel.getInter(i);
			if(intersects(io)){
				io.interact(timeSinceLastFrame, this);
			}
		}
		
				
		if(x < 0){
			x = 0;
			speedX = 0;
			lookDirection = Directions.right;
		}
		
		if(y < 0){
			y = 0;
			speedY = 0;
		}
		
		if(x > tempLevel.getWidth() * LevelData.TILESIZE - width){
			x = tempLevel.getWidth() * LevelData.TILESIZE - width;
			speedX = 0;
			lookDirection = Directions.left;
		}
		
		if(y > tempLevel.getHeight() * LevelData.TILESIZE){
			remove();
		}
		
		
		//Animation Stuff
		if(animation == Animations.standing && animationTime > ANIMATIONTIME_STANDING){
			animationTime -= ANIMATIONTIME_STANDING;
		}else if(animation == Animations.walking && animationTime > ANIMATIONTIME_WALKING){
			animationTime -= ANIMATIONTIME_WALKING;
		}else if(animation == Animations.running && animationTime > ANIMATIONTIME_RUNNING){
			animationTime -= ANIMATIONTIME_RUNNING;
		}else if(animation == Animations.jumping && animationTime > ANIMATIONTIME_JUMPING){
			animationTime -= ANIMATIONTIME_JUMPING;
		}
		
		
		if(speedX>0){
			lookDirection = Directions.right;
		}
		if(speedX<0){
			lookDirection = Directions.left;
		}
		
	}

	@Override
	public void draw() {
		Texture drawTex = animation_tex_standing1;
		float rotation = 0;
		
		if(animation == Animations.standing){							//What to draw while standing
			
			if(animationTime>2.75f)
				drawTex = animation_tex_standing2;
			else
				drawTex = animation_tex_standing1;
			
		}else if(animation == Animations.walking){						//What to draw while walking
			
			if(animationTime>ANIMATIONTIME_WALKING*3f/4f)
				drawTex = animation_tex_walking4;
			else if(animationTime>ANIMATIONTIME_WALKING*2f/4f)
				drawTex = animation_tex_walking3;
			else if(animationTime>ANIMATIONTIME_WALKING*1f/4f)
				drawTex = animation_tex_walking2;
			else
				drawTex = animation_tex_walking1;
			
		}else if(animation == Animations.running){						//What to draw while running
			
			if(animationTime>ANIMATIONTIME_RUNNING*7f/8f)
				drawTex = animation_tex_walking8;
			else if(animationTime>ANIMATIONTIME_RUNNING*6f/8f)
				drawTex = animation_tex_walking7;
			else if(animationTime>ANIMATIONTIME_RUNNING*5f/8f)
				drawTex = animation_tex_walking6;
			else if(animationTime>ANIMATIONTIME_RUNNING*4f/8f)
				drawTex = animation_tex_walking5;
			else if(animationTime>ANIMATIONTIME_RUNNING*3f/8f)
				drawTex = animation_tex_walking4;
			else if(animationTime>ANIMATIONTIME_RUNNING*2f/8f)
				drawTex = animation_tex_walking3;
			else if(animationTime>ANIMATIONTIME_RUNNING*1f/8f)
				drawTex = animation_tex_walking2;
			else
				drawTex = animation_tex_walking1;
			
		}else if(animation == Animations.stopping){						//What to draw while stopping
			
				drawTex = animation_tex_stopping1;
				
		}else if(animation == Animations.jumping){						//What to draw while jumping
			
			drawTex = animation_tex_jumping1;
			
			if(lookDirection == Directions.right){
				rotation = 360 * animationTime/ANIMATIONTIME_JUMPING;
			}else{
				rotation = -360 * animationTime/ANIMATIONTIME_JUMPING;
			}
			
//			// Old Version without rotation			
//			if(animationTime>ANIMATIONTIME_JUMPING*3f/4f)
//				drawTex = animation_tex_jumping4;
//			else if(animationTime>ANIMATIONTIME_JUMPING*2f/4f)
//				drawTex = animation_tex_jumping3;
//			else if(animationTime>ANIMATIONTIME_JUMPING*1f/4f)
//				drawTex = animation_tex_jumping2;
//			else
//				drawTex = animation_tex_jumping1;
			
		}else if(animation == Animations.landing){						//What to draw while landing
			drawTex = animation_tex_landing1;
			
		}else if(animation == Animations.takenDamage){					//What to draw after taken damage
			drawTex = animation_tex_takenDamage1;
		}
		
		Brush.setColor(1, 1, 1, 1);
		if(tempLevel.GRAVITY >= 0){
			if(lookDirection == Directions.right)
				Brush.drawImage(drawTex, (int)(x - Session.camX)-17, (int)(y - Session.camY)-56, 64, 128, rotation, 0, -22);
			else{
				Brush.drawImage(drawTex, (int)(x - Session.camX)-17+64, (int)(y - Session.camY)-56, -64, 128, rotation, 0, -22);
			}
		}else{
			if(lookDirection == Directions.right)
				Brush.drawImage(drawTex, (int)(x - Session.camX)-17, (int)(y - Session.camY)-56+172, 64, -128, rotation, 0, 22);
			else{
				Brush.drawImage(drawTex, (int)(x - Session.camX)-17+64, (int)(y - Session.camY)-56+172, -64, -128, rotation, 0, 22);
			}
		}
	}
	
	
	public void addSpeedX(float speedx){
		this.speedX += speedx;
	}
	public void addSpeedY(float speedy){
		this.speedY += speedy;
	}
	
	
	public static void loadTextures(){
		try {
			animation_tex_standing1 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_standing_01.png"));
			animation_tex_standing2 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_standing_02.png"));
			animation_tex_walking1  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_walking_01.png"));
			animation_tex_walking2  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_walking_02.png"));
			animation_tex_walking3  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_walking_03.png"));
			animation_tex_walking4  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_walking_04.png"));
//			animation_tex_walking5  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_walking_05.png"));
//			animation_tex_walking6  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_walking_06.png"));
//			animation_tex_walking7  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_walking_07.png"));
//			animation_tex_walking8  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_walking_08.png"));
			animation_tex_stopping1 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_stopping_01.png"));
			animation_tex_jumping1  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_jumping_01.png"));
			animation_tex_jumping2  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_jumping_02.png"));
			animation_tex_jumping3  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_jumping_03.png"));
			animation_tex_jumping4  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_jumping_04.png"));
			animation_tex_landing1  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_landing_01.png"));
			animation_tex_takenDamage1  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/enemy_standard_takenDamage_01.png"));
			
			biRepresentation = new BufferedImage(32, 32, BufferedImage.TYPE_4BYTE_ABGR);
			Graphics g = biRepresentation.getGraphics();

			g.drawImage(ImageHelper.loadImage("res/enemy_standard_standing_01.png"), -20, -54, null);
			
			g.dispose();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getID() {
		return mainID;
	}


	@Override
	public void kill() {
		remove();
	}
}
