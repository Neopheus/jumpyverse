package net.jumpyverse.entities;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.swing.JPanel;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.editor.Session;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.utils.BrotMath;
import net.jumpyverse.utils.ByteHelper;
import net.jumpyverse.utils.ImageHelper;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;


public class Trampoline extends InteractiveObject{
	/*
	 * Attributes for GUI:
	 * get/set Jumppower	(int)
	 * get/set Angle		(int)
	 */
	
	public static float defaultJumpPower = 500;
	public static float defaultAngle = -90;
	
	
	private float jumpPower = defaultJumpPower;
	private float jumpPowerX = 0;
	private float jumpPowerY = 0;
	private float angle = defaultAngle;
	
	
	private static Texture plate;
	private static Texture holding;
	public static BufferedImage biRepresentation;
	public static final short mainID = 1;
	
	@Override
	public short getID() {
		return mainID;
	}
	
	
	static{
		try {
			plate = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/sprite_bounce_plate_01.png"));
			holding = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/sprite_bounce_01.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		biRepresentation = new BufferedImage(32, 32, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = biRepresentation.getGraphics();

		g.drawImage(ImageHelper.loadImage("res/sprite_bounce_01.png"), 0, 0, null);
		g.drawImage(ImageHelper.loadImage("res/sprite_bounce_plate_01.png"), 0, 0, null);
		
		g.dispose();
	}
	
	public Trampoline(){
		this(defaultJumpPower, defaultAngle);
	}
	
	public Trampoline(float jumpPower){
		this(jumpPower, defaultAngle);
	}
	
	public Trampoline(float jumpPower, float angle){
		this.width = 32;
		this.height = 32;
		this.jumpPower = jumpPower;
		setAngle(angle);
		bounding = new Rectangle((int)x, (int)y, width, height);
	}
	
	@Override
	public void update(float timeSinceLastFrame, LevelData leveldata) {
		//NOTHING
	}

	@Override
	public void draw() {
		Brush.drawImage(holding, x-Session.camX, y-Session.camY, width, height);
		Brush.drawImage(plate.getTextureID(), x-Session.camX, y-Session.camY, width, height, angle+90);
	}
	
	@Override
	public void interact(float timeSinceLastFrame, GameObject target) {
		float scalar = target.getSpeedX()*jumpPowerX + target.getSpeedY()*jumpPowerY;
		if(scalar<0){
			target.addSpeedX(jumpPowerX);
			target.setSpeedY(jumpPowerY);
			if(angle<45){
				target.setX(x+width);
			}else if(angle<135){
				target.setY(y+height);
			}else if(angle<225){
				target.setX(x-target.getWidth());
			}else if(angle<315){
				target.setY(y-target.getHeight());
			}else{
				target.setX(x+width);
			}
			if(target instanceof Player){
				Player p = (Player)target;
				p.setIsJumping(true);
			}
		}
	}
	
	
	public void setAngle(float angle){
		this.angle = BrotMath.modulo(angle, 360);
		calculateJumpPower();
	}
	public void setJumpPower(float jumpPower){
		this.jumpPower = jumpPower;
		calculateJumpPower();
	}
	
	public float getAngle(){
		return angle;
	}
	public float getJumpPower(){
		return jumpPower;
	}
	
	
	
	private void calculateJumpPower(){
		float angleRadians = (float)Math.toRadians(angle);
		float jumpPowerX = jumpPower;
		float sin = (float)Math.sin(angleRadians);
		float cos = (float)Math.cos(angleRadians);
		
		this.jumpPowerX = jumpPowerX * cos;
		this.jumpPowerY = jumpPowerX * sin;
	}
	
	@Override
	public JPanel getObjectPanel() {
		return (new net.jumpyverse.gui.TrampolinePanel());
	}
}
