package net.jumpyverse.entities;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.imageio.ImageIO;

import net.jumpyverse.editor.Session;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class BlockType {
	public static final short VARIATIONSPERTEXTURE = 16;
	
	
	private static String locationOfJar = new File(BlockType.class.getProtectionDomain().getCodeSource() .getLocation().getPath()).getName();
	private static int nextLoadNumber = 1;
	private static Texture[] tex = new Texture[Short.MAX_VALUE];
	private static int[] availabletextures = new int[tex.length/VARIATIONSPERTEXTURE];
	private static BufferedImage[] biRepresentation = new BufferedImage[tex.length/VARIATIONSPERTEXTURE];
	private static String[] stringRepresentation = new String[tex.length/VARIATIONSPERTEXTURE];
	private static Random random = new Random();
	
	
	public static void loadBlockSheet(String path, int mainID) throws IOException{
		if(mainID<=0||mainID>=availabletextures.length) throw new IllegalArgumentException("id must be between 1 and "+availabletextures.length);
		
		BufferedImage loadedSheet = ImageIO.read(BlockType.class.getClassLoader().getResourceAsStream("res/sprite_"+path+"_01.png"));
		biRepresentation[mainID] = new BufferedImage(32, 32, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = biRepresentation[mainID].getGraphics();
		g.drawImage(loadedSheet, 0, 0, null);
		g.dispose();
		
		stringRepresentation[mainID] = path;
		
		for(int i = 0; i<VARIATIONSPERTEXTURE; i++){
			String localPath = "res/sprite_" + path + "_" + String.format("%02d", i+1) + ".png";
			File file1 = new File("src/"+localPath);		//ONLY FOR ECLIPSE!!!
			File file2 = new File(localPath);				//Jar Export from here (Fuck you Eclipse! ... jk, love u)
			JarEntry entry = null;
			if(!locationOfJar.equals("bin")){
				JarFile jar = new JarFile(locationOfJar);
				entry = jar.getJarEntry(localPath);
				jar.close();								//to here
			}
			availabletextures[mainID] = i;
			if(file1.exists()||file2.exists()||entry!=null){
				tex[VARIATIONSPERTEXTURE*mainID+i] = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(localPath));
			}else{
				break;
			}
		}
		
		if(mainID>=nextLoadNumber) nextLoadNumber = mainID + 1;
	}
	
	public static void loadBlockSheet(String path) throws IOException{
		loadBlockSheet(path, nextLoadNumber);
	}
	
	
	
	public static boolean hasSameMainID(short a, short b){
		a/=VARIATIONSPERTEXTURE;
		b/=VARIATIONSPERTEXTURE;
		
		return a==b;
	}
	
	public static Texture getSpriteSheet(short tileID){
		return tex[tileID];
	}
	
	
	public static short getBlockID(short tileID){
		if(tileID == 0)return 0;
		short b = (short)random.nextInt(availabletextures[tileID]);
		return getBlockID(tileID, b);
	}
	public static short getBlockID(short tileID, short variationID){
		if(tileID == 0)return 0;
		if(variationID<0||variationID>availabletextures[tileID]) throw new IllegalArgumentException("variationID must be between 0 and "+(availabletextures[Session.leftClickTileIDBlock]));
		return (short) (tileID*VARIATIONSPERTEXTURE+variationID);
	}
	public static BufferedImage[] getBiRepresentations(){
		return biRepresentation;
	}
	public static String[] getStringRepresentations(){
		return stringRepresentation;
	}
}
