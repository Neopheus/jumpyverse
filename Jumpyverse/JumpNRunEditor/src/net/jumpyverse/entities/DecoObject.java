package net.jumpyverse.entities;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.LWJGLCore.Screen;
import net.jumpyverse.editor.Session;
import net.jumpyverse.game.LevelData;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class DecoObject extends GameObject{
	private static Texture[] looks = new Texture[1024];
	private static BufferedImage[] biRepresentation = new BufferedImage[looks.length];
	private static String[] stringRepresentation = new String[looks.length];
	private static final int WIDTH = 32;
	private static final int HEIGHT = 32;
	public static final int size = 32;
	private final int textureID;
	private static int nextLoadNumber = 0;
	private boolean needsADraw = false;
	
	public DecoObject(float x, float y, int textureID){
		this.x = x;
		this.y = y;
		this.textureID = textureID;
	}
	
	public static void loadDecoTexture(String path, int mainID) throws IOException{
		if(mainID<0||mainID>looks.length-1) throw new IllegalArgumentException("mainID must be between 0 and" + (looks.length-1));
		
		BufferedImage loadedSheet = ImageIO.read(BlockType.class.getClassLoader().getResourceAsStream("res/sprite_deco_"+path+".png"));
		biRepresentation[mainID] = new BufferedImage(32, 32, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = biRepresentation[mainID].getGraphics();
		g.drawImage(loadedSheet, 0, 0, null);
		g.dispose();
		
		stringRepresentation[mainID] = path;
		
		
		try {
			looks[mainID] = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/sprite_deco_" + path + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(mainID>=nextLoadNumber){
			nextLoadNumber = mainID+1;
		}
	}
	
	public static void loadDecoTexture(String path) throws IOException{
		loadDecoTexture(path, nextLoadNumber);
	}
	
	@Override
	public void draw() {
		if(needsADraw) Brush.drawImage(looks[textureID].getTextureID(), (int)(x-Session.camX), (int)(y-Session.camY), WIDTH, HEIGHT);
	}
	
	public int getID(){
		return textureID;
	}
	
	public static void draw(float x, float y, int mainID){
		Brush.drawImage(looks[mainID], (int)(x-WIDTH), (int)(y-HEIGHT), WIDTH, HEIGHT);
	}

	@Override
	public void update(float timeSinceLastFrame, LevelData leveldata) {
		if(x>Session.camX-WIDTH && y>Session.camY-HEIGHT && x<Session.camX+WIDTH+Screen.width && y<Session.camY+HEIGHT+Screen.height) needsADraw = true;
		else needsADraw = false;
	}
	
	public static BufferedImage[] getBiRepresentations(){
		return biRepresentation;
	}
	public static String[] getStringRepresentations(){
		return stringRepresentation;
	}

	@Override
	public int getWidth() {
		return WIDTH;
	}

	@Override
	public int getHeight() {
		return HEIGHT;
	}
	
}
