package net.jumpyverse.entities;

import static net.jumpyverse.editor.Session.camX;
import static net.jumpyverse.editor.Session.camY;
import static net.jumpyverse.editor.Session.tempLevel;

import java.awt.Rectangle;
import java.io.IOException;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.LWJGLCore.JKeyboard;
import net.jumpyverse.LWJGLCore.Screen;
import net.jumpyverse.editor.Session;
import net.jumpyverse.enums.Animations;
import net.jumpyverse.enums.Directions;
import net.jumpyverse.game.LevelData;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;


public class Player extends CreatureObject{
	public final float RESPAWNX;
	public final float RESPAWNY;
	
	/*
	 * JPANEL ATTRIBUTES FROM HERE
	 */
	public int HEALTH = 1;
	public int DAMAGE = 1;
	public float MAXSPEEDHARD 	= 5000f;
	public float MAXSPEEDWALK	= 300;
	public float MAXSPEEDRUN	= 500;
	public float MAXFALLSPEED 	= 1000;
	public float ACCELERATION 	= 1000;
	public float FRICTION		= 1000;
	public float JUMPPOWER		= 525;
	public float MASS			= 100;
	/*
	 * TO HERE
	 */
	
	private float accelerationX = 0;
	
	private float preX = x;
	private float preY = y;
	
	private float animationTime = 0;

	private static final float ANIMATIONTIME_STANDING = 3;
	private static final float ANIMATIONTIME_WALKING = 0.4f;
	private static final float ANIMATIONTIME_RUNNING = 0.2f;
	private static final float ANIMATIONTIME_JUMPING = 0.2f;
	private static final float ANIMATIONTIME_LANDING = 0.2f;
	
	private static Texture animation_tex_standing1 = null;
	private static Texture animation_tex_standing2 = null;
	
	private static Texture animation_tex_walking1 = null;
	private static Texture animation_tex_walking2 = null;
	private static Texture animation_tex_walking3 = null;
	private static Texture animation_tex_walking4 = null;
	private static Texture animation_tex_walking5 = null;
	private static Texture animation_tex_walking6 = null;
	private static Texture animation_tex_walking7 = null;
	private static Texture animation_tex_walking8 = null;
	
	private static Texture animation_tex_stopping1 = null;
	
	private static Texture animation_tex_jumping = null;
	
	private static Texture animation_tex_landing1 = null;
	
	private Directions lookDirection = Directions.right;
	private Directions moveDirection = Directions.right;
	
	private Animations animation = Animations.standing;
	
	private boolean isControllingLeft = false;
	private boolean isControllingRight = false;
	private boolean isControlling = false;
	private boolean isControllingLeftStartedThisFrame = false;
	private boolean isControllingRightStartedThisFrame = false;
	private boolean isControllingStartedThisFrame = false;
	private boolean wasControllingLeft = false;
	private boolean wasControllingRight = false;
	private boolean wasControlling = false;
	
	private boolean canJump = false;
	private boolean isJumping = false;
	private boolean wasJumping = false;
	private boolean isRunning = false;
	private boolean wasRunning = false;
	
	private boolean wasFasterThanWalk = false;
	private boolean wasFasterThanRun  = false;
	private boolean wasFasterThanHard = false;
	
	private boolean jumpButtonIsPressed = false;
	private boolean jumpButtonWasPressed = false;
	
	private Rectangle jumpCheckBox;
	
	private int points = 0;
	
	
	public Player(float x, float y){
		this.x = x;
		this.y = y;
		RESPAWNX = x;
		RESPAWNY = y;
		this.width = 30;
		this.height = 60;
		bounding = new Rectangle((int)x, (int)y, width, height);
		jumpCheckBox = new Rectangle((int)x, (int)y+height, width, 5);
	}
	
	@Override
	public Player clone() {
		Player copy = new Player(x, y);
		
		copy.MAXSPEEDHARD = MAXSPEEDHARD;
		copy.ACCELERATION = ACCELERATION;
		copy.FRICTION = FRICTION;
		copy.JUMPPOWER = JUMPPOWER;
		
		return copy;
	}
	
	
	public void setIsJumping(boolean isJumping){
		this.isJumping = isJumping;
	}
	
	
	@Override
	public void update(float timeSinceLastFrame, LevelData leveldata) {
		
		preX = x;
		preY = y;
		jumpButtonWasPressed = jumpButtonIsPressed;
		wasRunning = isRunning;
		
		animationTime += timeSinceLastFrame;
		animation = Animations.standing;
		
		accelerationX = 0;
		speedY += Session.tempLevel.GRAVITY * MASS * timeSinceLastFrame;
		
		jumpButtonIsPressed = (JKeyboard.isKeyDown(Keyboard.KEY_W) || JKeyboard.isKeyDown(Keyboard.KEY_SPACE) || JKeyboard.isKeyDown(Keyboard.KEY_UP));
		
		if(leveldata.GRAVITY >= 0){
			if(canJump && jumpButtonIsPressed && !jumpButtonWasPressed){
				speedY = -JUMPPOWER;
				isJumping = true;
			}
		}else{
			if(canJump && jumpButtonIsPressed && !jumpButtonWasPressed){
				speedY = JUMPPOWER;
				isJumping = true;
			}
		}
		
		wasControllingLeft = isControllingLeft;
		wasControllingRight = isControllingRight;
		wasControlling = isControlling;
		
		isControllingLeft = JKeyboard.isKeyDown(Keyboard.KEY_A) || JKeyboard.isKeyDown(Keyboard.KEY_LEFT);
		isControllingRight = JKeyboard.isKeyDown(Keyboard.KEY_D) || JKeyboard.isKeyDown(Keyboard.KEY_RIGHT);
		isControlling = isControllingLeft || isControllingRight;
		
		isControllingLeftStartedThisFrame = isControllingLeft && !wasControllingLeft;
		isControllingRightStartedThisFrame = isControllingRight && !wasControllingRight;
		isControllingStartedThisFrame = isControlling && !wasControlling;
		
		if(isControllingLeftStartedThisFrame || (isControllingLeft && !isControllingRight)){
			moveDirection = Directions.left;
		}
		if(isControllingRightStartedThisFrame || (isControllingRight && !isControllingLeft)){
			moveDirection = Directions.right;
		}
		
		if(isControlling){
			if(moveDirection == Directions.left){
				accelerationX -= ACCELERATION * timeSinceLastFrame;
			}
			if(moveDirection == Directions.right){
				accelerationX += ACCELERATION * timeSinceLastFrame;
			}
		}
		
		wasFasterThanWalk = speedX > MAXSPEEDWALK || speedX < -MAXSPEEDWALK;
		wasFasterThanRun  = speedX > MAXSPEEDRUN  || speedX < -MAXSPEEDRUN;
		wasFasterThanHard = speedX > MAXSPEEDHARD || speedX < -MAXSPEEDHARD;
		
		if(JKeyboard.isKeyDown(Keyboard.KEY_LSHIFT)){
			speedX += accelerationX * 2;
			isRunning = true;
		}else{
			speedX += accelerationX;
			isRunning = false;
		}
		
		if(isRunning){
			if(!wasFasterThanRun || !wasRunning) {
				if(speedX > MAXSPEEDRUN) speedX = MAXSPEEDRUN;
				if(speedX < -MAXSPEEDRUN) speedX = -MAXSPEEDRUN;
			}
		}else{
			if(!wasFasterThanWalk || wasRunning) {
				if(speedX > MAXSPEEDWALK) speedX = MAXSPEEDWALK;
				if(speedX < -MAXSPEEDWALK) speedX = -MAXSPEEDWALK;
			}
		}
		
		if(speedX > MAXSPEEDHARD){
			speedX = MAXSPEEDHARD;
		}
		if(speedX < -MAXSPEEDHARD){
			speedX = -MAXSPEEDHARD;
		}
		
		if(leveldata.GRAVITY >= 0){
			if(speedY > MAXFALLSPEED){
				speedY = MAXFALLSPEED;
			}
		}else{
			if(speedY < -MAXFALLSPEED){
				speedY = -MAXFALLSPEED;
			}
		}
		
		
		
		if(accelerationX!=0){
			if(!isRunning)
				animation = Animations.walking;
			else
				animation = Animations.running;
		}else if(accelerationX == 0 && speedX != 0){
			animation = Animations.stopping;
		}
		
		
		if(isJumping) {
			animation = Animations.jumping;
		}
		
		
		if(!isJumping && wasJumping && accelerationX == 0){
			animation = Animations.landing;
		}
		
		if(isJumping)FRICTION /= 10;
		if(accelerationX == 0){
			if(speedX < FRICTION / 10 && speedX > -FRICTION /10){
				speedX = 0;
			}else{
				if(speedX > 0){
					speedX = speedX - FRICTION * timeSinceLastFrame;
				}else{
					speedX = speedX + FRICTION * timeSinceLastFrame;
				}
			}
		}
		if(isJumping)FRICTION *= 10;
		
		
		speedX += leveldata.windSpeed;
		
		int tilesXToCheck = (int) (4 + Math.abs(speedX) * timeSinceLastFrame / LevelData.TILESIZE);
		int tilesYToCheck = (int) (4 + Math.abs(speedY) * timeSinceLastFrame / LevelData.TILESIZE);
		if(tilesXToCheck > 16) tilesXToCheck = 16;
		if(tilesYToCheck > 16) tilesYToCheck = 16;
		int tileX = (int) (x / LevelData.TILESIZE);
		int tileY = (int) (y / LevelData.TILESIZE);
		
		final int STEPSIZE = 15;
		
		
		float distanceToMoveX = speedX * timeSinceLastFrame;
		while(distanceToMoveX!=0){
			if(distanceToMoveX > STEPSIZE){
				distanceToMoveX -= STEPSIZE;
				x += STEPSIZE;
			}else if(distanceToMoveX < -STEPSIZE){
				distanceToMoveX += STEPSIZE;
				x -= STEPSIZE;
			}else{
				x += distanceToMoveX;
				distanceToMoveX = 0;
			}
			
			
			bounding.x = (int)x;
			jumpCheckBox.x = (int)x;
			tileX = (int) (x / LevelData.TILESIZE);
			tileY = (int) (y / LevelData.TILESIZE);

			
			outer_loop: for(int i = tileX-tilesXToCheck; i<tileX+tilesXToCheck; i++){
				for(int k = tileY-tilesYToCheck; k<tileY+tilesYToCheck; k++){
					Rectangle block = leveldata.getBlock(i, k);
					if(block != null){
						if(bounding.intersects(block)){
							if(speedX>0){
								x = block.x - width;
							}else{
								x = block.x + LevelData.TILESIZE;
							}
							bounding.x = (int)x;
							jumpCheckBox.x = (int)x;
							tileX = (int) (x / LevelData.TILESIZE);
							speedX = leveldata.windSpeed;
							distanceToMoveX = 0;
							break outer_loop;
						}
					}
				}
			}
		}
		
		speedX -= leveldata.windSpeed;
		
		
		float distanceToMoveY = speedY * timeSinceLastFrame;
		
		if(animationTime>ANIMATIONTIME_LANDING){
			wasJumping = false;
		}
		
		while(distanceToMoveY!=0){
			if(distanceToMoveY > STEPSIZE){
				distanceToMoveY -= STEPSIZE;
				y += STEPSIZE;
			}else if(distanceToMoveY < -STEPSIZE){
				distanceToMoveY += STEPSIZE;
				y -= STEPSIZE;
			}else{
				y += distanceToMoveY;
				distanceToMoveY = 0;
			}
			bounding.y = (int)y;
			jumpCheckBox.y = (int)y+height;
			
			outer_loop: for(int i = tileX-tilesXToCheck; i<tileX+tilesXToCheck; i++){
				for(int k = tileY-tilesYToCheck; k<tileY+tilesYToCheck; k++){
					Rectangle block = leveldata.getBlock(i, k);
					if(block != null){
						if(bounding.intersects(block)){
							if(speedY>0){
								y = block.y - height;
								if(leveldata.GRAVITY >= 0){
									if(isJumping){
										wasJumping = true;
										animationTime = 0;
									}
									isJumping = false;
								}
							}else{
								y = block.y + LevelData.TILESIZE;
								if(leveldata.GRAVITY < 0){
									if(isJumping){
										wasJumping = true;
										animationTime = 0;
									}
									isJumping = false;
								}
							}
							bounding.y = (int)y;
							if(leveldata.GRAVITY >= 0){
								jumpCheckBox.y = (int)y+height;
							}else{
								jumpCheckBox.y = (int)y-jumpCheckBox.height;
							}
							tileY = (int) (y / LevelData.TILESIZE);
							speedY = 0;
							distanceToMoveY = 0;
							break outer_loop;
						}
					}
				}
			}
		}
		
		
		canJump = false;
		for(int i = tileX-16; i<tileX+16; i++){
			for(int k = tileY-16; k<tileY+16; k++){
				Rectangle block = leveldata.getBlock(i, k);
				if(block != null){
					if(jumpCheckBox.intersects(block)){
						canJump = true;
					}
				}
			}
		}
		
		//ENEMYS
		for(int i = 0; i<tempLevel.getEnemySize(); i++){
			EnemyObject eo = tempLevel.getEnemy(i);
			if(intersects(eo)){
				if(leveldata.GRAVITY >= 0){
					if(preY + height - 10 <= eo.y){
						eo.takeDamage(DAMAGE);
						setIsJumping(true);
						speedY = -JUMPPOWER;
						y = eo.y - height;
						bounding.y = (int)y;
						jumpCheckBox.y = (int)y+height;
					}else{
						respawn();
					}
				}else{
					if(eo.y + eo.getHeight() -10 <= preY){
						eo.takeDamage(DAMAGE);
						setIsJumping(true);
						speedY = JUMPPOWER;
						y = eo.y + eo.getHeight();
						bounding.y = (int)y;
						jumpCheckBox.y = (int)y-jumpCheckBox.height;
					}else{
						respawn();
					}
				}
			}
		}
		
		
		//INTERACTIVES
		for(int i = 0; i<tempLevel.getInterSize(); i++){
			InteractiveObject io = tempLevel.getInter(i);
			if(intersects(io)){
				io.interact(timeSinceLastFrame, this);
			}
		}
		
		
		Session.camX =  ((int)x + width/2 - Screen.width/2);
		Session.camY =  ((int)y + height/2 - Screen.height/2);
		
		if(camX < 0 ) camX = 0;
		if(camY < 0 ) camY = 0;
		
		if(camX > tempLevel.getWidth() * LevelData.TILESIZE - Screen.width) camX = tempLevel.getWidth() * LevelData.TILESIZE - Screen.width;
		if(camY > tempLevel.getHeight() * LevelData.TILESIZE - Screen.height) camY = tempLevel.getHeight() * LevelData.TILESIZE - Screen.height;
		
		
		if(x < 0){
			x = 0;
			speedX = 0;
		}
		if(leveldata.GRAVITY >= 0){
			if(y < 0){
				y = 0;
				speedY = 0;
			}
		}else{
			if(y < -height){
				respawn();
			}
		}
		if(x > tempLevel.getWidth() * LevelData.TILESIZE - width){
			x = tempLevel.getWidth() * LevelData.TILESIZE - width;
			speedX = 0;
		}
		
		if(leveldata.GRAVITY >= 0){
			if(y > tempLevel.getHeight() * LevelData.TILESIZE){
				respawn();
			}
		}else{
			if(y > tempLevel.getHeight() * LevelData.TILESIZE - height){
				y = tempLevel.getHeight() * LevelData.TILESIZE - height;
				speedY = 0;
			}
		}
		
		//Animation Stuff
		if(animation == Animations.standing && animationTime > ANIMATIONTIME_STANDING){
			animationTime -= ANIMATIONTIME_STANDING;
		}else if(animation == Animations.walking && animationTime > ANIMATIONTIME_WALKING){
			animationTime -= ANIMATIONTIME_WALKING;
		}else if(animation == Animations.running && animationTime > ANIMATIONTIME_RUNNING){
			animationTime -= ANIMATIONTIME_RUNNING;
		}else if(animation == Animations.jumping && animationTime > ANIMATIONTIME_JUMPING){
			animationTime -= ANIMATIONTIME_JUMPING;
		}
		
		
		if(speedX>0){
			lookDirection = Directions.right;
		}
		if(speedX<0){
			lookDirection = Directions.left;
		}
	}

	@Override
	public void draw() {
		Texture drawTex = animation_tex_standing1;
		float rotation = 0;
		
		if(animation == Animations.standing){							//What to draw while standing
			
			if(animationTime>2.75f)
				drawTex = animation_tex_standing2;
			else
				drawTex = animation_tex_standing1;
			
		}else if(animation == Animations.walking){						//What to draw while walking
			
			if(animationTime>ANIMATIONTIME_WALKING*7f/8f)
				drawTex = animation_tex_walking8;
			else if(animationTime>ANIMATIONTIME_WALKING*6f/8f)
				drawTex = animation_tex_walking7;
			else if(animationTime>ANIMATIONTIME_WALKING*5f/8f)
				drawTex = animation_tex_walking6;
			else if(animationTime>ANIMATIONTIME_WALKING*4f/8f)
				drawTex = animation_tex_walking5;
			else if(animationTime>ANIMATIONTIME_WALKING*3f/8f)
				drawTex = animation_tex_walking4;
			else if(animationTime>ANIMATIONTIME_WALKING*2f/8f)
				drawTex = animation_tex_walking3;
			else if(animationTime>ANIMATIONTIME_WALKING*1f/8f)
				drawTex = animation_tex_walking2;
			else
				drawTex = animation_tex_walking1;
			
		}else if(animation == Animations.running){						//What to draw while running
			
			if(animationTime>ANIMATIONTIME_RUNNING*7f/8f)
				drawTex = animation_tex_walking8;
			else if(animationTime>ANIMATIONTIME_RUNNING*6f/8f)
				drawTex = animation_tex_walking7;
			else if(animationTime>ANIMATIONTIME_RUNNING*5f/8f)
				drawTex = animation_tex_walking6;
			else if(animationTime>ANIMATIONTIME_RUNNING*4f/8f)
				drawTex = animation_tex_walking5;
			else if(animationTime>ANIMATIONTIME_RUNNING*3f/8f)
				drawTex = animation_tex_walking4;
			else if(animationTime>ANIMATIONTIME_RUNNING*2f/8f)
				drawTex = animation_tex_walking3;
			else if(animationTime>ANIMATIONTIME_RUNNING*1f/8f)
				drawTex = animation_tex_walking2;
			else
				drawTex = animation_tex_walking1;
			
		}else if(animation == Animations.stopping){						//What to draw while stopping
			
				drawTex = animation_tex_stopping1;
				
		}else if(animation == Animations.jumping){						//What to draw while jumping
			
			drawTex = animation_tex_jumping;
			
			if(lookDirection == Directions.right){
				rotation = 360 * animationTime/ANIMATIONTIME_JUMPING;
			}else{
				rotation = -360 * animationTime/ANIMATIONTIME_JUMPING;
			}
			
			
//			if(animationTime>ANIMATIONTIME_JUMPING*3f/4f)
//				drawTex = animation_tex_jumping4;
//			else if(animationTime>ANIMATIONTIME_JUMPING*2f/4f)
//				drawTex = animation_tex_jumping3;
//			else if(animationTime>ANIMATIONTIME_JUMPING*1f/4f)
//				drawTex = animation_tex_jumping2;
//			else
//				drawTex = animation_tex_jumping1;
			
		}else if(animation == Animations.landing){						//What to draw while landing
			drawTex = animation_tex_landing1;
		}
		
		Brush.setColor(1, 1, 1, 1);
		if(tempLevel.GRAVITY >= 0){
			if(lookDirection == Directions.right)
				Brush.drawImage(drawTex, (int)(x - Session.camX)-17, (int)(y - Session.camY)-56, 64, 128, rotation, 0, -22);
			else{
				Brush.drawImage(drawTex, (int)(x - Session.camX)-17+64, (int)(y - Session.camY)-56, -64, 128, rotation, 0, -22);
			}
		}else{
			if(lookDirection == Directions.right)
				Brush.drawImage(drawTex, (int)(x - Session.camX)-17, (int)(y - Session.camY)-56+172, 64, -128, rotation, 0, 22);
			else{
				Brush.drawImage(drawTex, (int)(x - Session.camX)-17+64, (int)(y - Session.camY)-56+172, -64, -128, rotation, 0, 22);
			}
		}
	}
	
	public void respawn(){
		tempLevel = Session.editorState.clone();
		tempLevel.loadIngame();
	}
	
	public void winning(){
		System.out.println("Wow, such win, much fame, very flag, wow!");
		respawn();
	}
	
	
	public void addSpeedX(float speedx){
		this.speedX += speedx;
	}
	public void addSpeedY(float speedy){
		this.speedY += speedy;
	}
	
	
	public static void loadTextures(){
		try {
			animation_tex_standing1 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/player_standard_standing_01.png"));
			animation_tex_standing2 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/player_standard_standing_02.png"));
			animation_tex_walking1  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/player_standard_walking_01.png"));
			animation_tex_walking2  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/player_standard_walking_02.png"));
			animation_tex_walking3  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/player_standard_walking_03.png"));
			animation_tex_walking4  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/player_standard_walking_04.png"));
			animation_tex_walking5  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/player_standard_walking_05.png"));
			animation_tex_walking6  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/player_standard_walking_06.png"));
			animation_tex_walking7  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/player_standard_walking_07.png"));
			animation_tex_walking8  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/player_standard_walking_08.png"));
			animation_tex_stopping1 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/player_standard_stopping_01.png"));
			animation_tex_jumping  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/player_standard_jumping_01.png"));
			animation_tex_landing1  = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/player_standard_landing_01.png"));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public int getPoints(){
		return points;
	}
	public void setPoints(int points){
		this.points = points;
	}
	public void addPoints(int points){
		this.points += points;
	}
	public void subPoints(int points){
		this.points -= points;
	}
}
