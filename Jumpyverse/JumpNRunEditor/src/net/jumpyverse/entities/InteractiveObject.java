package net.jumpyverse.entities;

import javax.swing.JPanel;

public abstract class InteractiveObject extends GameObject{
	public abstract void interact(float timeSinceLastFrame, GameObject target);
	public abstract JPanel getObjectPanel();
	
	public abstract short getID();
}
