package net.jumpyverse.entities;

import net.jumpyverse.editor.Session;

public abstract class EnemyObject extends CreatureObject{
	protected int HEALTH;
	protected int DAMAGE;
	public int getHealth(){
		return HEALTH;
	}
	public void setHealth(int health){
		this.HEALTH = health;
	}
	public int getDamage(){
		return HEALTH;
	}
	public void setDamage(int damage){
		this.DAMAGE = damage;
	}
	public void takeDamage(int dmg){
		HEALTH -= dmg;
		if(HEALTH<=0){
			kill();
		}
	}
	public abstract void setIsJumping(boolean isJumping);
	public abstract int getID();
	public abstract void kill();
}
