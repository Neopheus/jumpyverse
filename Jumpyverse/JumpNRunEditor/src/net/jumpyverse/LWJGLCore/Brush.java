package net.jumpyverse.LWJGLCore;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;

import javax.swing.text.StyledEditorKit.FontSizeAction;

import net.jumpyverse.game.LevelData;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL12;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import static org.lwjgl.opengl.GL11.*;

public class Brush {
	private static HashMap<String, Texture> fonts = new HashMap<String, Texture>();
	
	static{
		try {
			fonts.put("karmatic_arcade", TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/sprite_symboles_01.png")));
			fonts.put("visitor", TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("res/sprite_symboles_02.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	private static float transformX(float x){
		x/=Screen.width;
		x*=2;
		x--;
		return x;
	}
	private static float transformY(float y){
		y/=Screen.height;
		y*=2;
		y*=-1;
		y++;
		return y;
	}
	
	public static void setColor(Color c){
		setColor((float)c.getRed()/255f, (float)c.getGreen()/255f, (float)c.getBlue()/255f, (float)c.getAlpha()/255f);
	}
	public static void setColor(float r, float g, float b, float a){
		glColor4f(r, g, b, a);
	}
	
	
	public static void drawLine(float startX, float startY, float stopX, float stopY){
		glBegin(GL_LINE_STRIP);
			glVertex2d(startX, startY);
			glVertex2d(stopX, stopY);
		glEnd();
	}
	
	public static void drawRect(float x, float y, float width, float height){
		drawLine(x, y, x+width, y);
		drawLine(x, y, x, y+height);
		drawLine(x, y+height, x+width, y+height);
		drawLine(x+width, y, x+width, y+height);
	}
	
	public static void drawRect(Rectangle r){
		drawRect(r.x, r.y, r.width, r.height);
	}
	
	public static void drawOval(float x, float y, float width, float height) {
		glBegin(GL_LINE_LOOP);
			for (int i = 0; i<360; i++) {
				float alpha = i * (float) Math.PI/180;
				glVertex2f((float) Math.cos(alpha)*width/2+x+width/2,
						   (float) Math.sin(alpha)*height/2+y+height/2);
			}
		glEnd();
	}
	
	public static void drawPixel(float x, float y){
		drawRect(x, y, 1, 1);
	}
	
	
	
	
	public static void fillRect(float x, float y, float width, float height){
		glBegin(GL_QUADS);
			glVertex2f(x, y);
			glVertex2f(x+width, y);
			glVertex2f(x+width, y+height);
			glVertex2f(x, y+height);
		glEnd();
	}
	public static void fillRect(Rectangle r){
		fillRect(r.x, r.y, r.width, r.height);
	}
	
	
	
	
	
	
	public static void drawImage(Texture tex, float x, float y, float width, float height){
		drawImage(tex.getTextureID(), x, y, width, height);
	}
	
	public static void drawImage(int textureID, float x, float y, float width, float height){
		glBindTexture(GL_TEXTURE_2D, textureID);
		
		glBegin(GL_QUADS);
			glTexCoord2f(0,0);
			glVertex2f(x, y);
			glTexCoord2f(1,0);
			glVertex2f(x+width, y);
			glTexCoord2f(1, 1);
			glVertex2f(x+width, y+height);
			glTexCoord2f(0,1);
			glVertex2f(x, y+height);
		glEnd();
		
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	public static void drawImage(Texture tex, float x, float y, float width, float height, float rotation){
		drawImage(tex.getTextureID(), x, y, width, height, rotation);
	}
	
	public static void drawImage(int textureID, float x, float y, float width, float height, float rotation){
		drawImage(textureID, x, y, width, height, rotation, 0, 0);
	}
	
	public static void drawImage(Texture tex, float x, float y, float width, float height, float rotation, int rotationOffsetX, int rotationOffsetY){
		drawImage(tex.getTextureID(), x, y, width, height, rotation, rotationOffsetX, rotationOffsetY);
	}
	
	public static void drawImage(int textureID, float x, float y, float width, float height, float rotation, int rotationOffsetX, int rotationOffsetY){
		rotation = (float) Math.toRadians(rotation);
		
		float sin = (float) Math.sin(rotation);
		float cos = (float) Math.cos(rotation);
		
		float topleftX = (-width)/2 + rotationOffsetX;
		float topleftY = (-height/2 + rotationOffsetY);
		float toprightX = (+width)/2 + rotationOffsetX;
		float toprightY = (-height)/2 + rotationOffsetY;
		float bottomleftX = (-width)/2 + rotationOffsetX;
		float bottomleftY = (+height)/2 + rotationOffsetY;
		float bottomrightX = (+width)/2 + rotationOffsetX;
		float bottomrightY = (+height)/2 +rotationOffsetY;
		
		float rotatedtopleftX = cos*topleftX - sin*topleftY + width/2 + x - rotationOffsetX;
		float rotatedtopleftY = sin*topleftX + cos*topleftY + height/2 + y - rotationOffsetY;
		float rotatedtoprightX = cos*toprightX - sin*toprightY - width/2 +x - rotationOffsetX;
		float rotatedtoprightY = sin*toprightX + cos*toprightY + height/2 + y - rotationOffsetY;
		float rotatedbottomleftX = cos*bottomleftX - sin*bottomleftY + width/2 + x - rotationOffsetX;
		float rotatedbottomleftY = sin*bottomleftX + cos*bottomleftY -height/2 + y - rotationOffsetY;
		float rotatedbottomrightX = cos*bottomrightX - sin*bottomrightY - width/2 + x - rotationOffsetX;
		float rotatedbottomrightY = sin*bottomrightX + cos*bottomrightY - height/2 + y - rotationOffsetY;
		
		
		glBindTexture(GL_TEXTURE_2D, textureID);
		
		glBegin(GL_QUADS);
			glTexCoord2f(0,0);
			glVertex2f(rotatedtopleftX, rotatedtopleftY);
			glTexCoord2f(1,0);
			glVertex2f(rotatedtoprightX+width, rotatedtoprightY);
			glTexCoord2f(1, 1);
			glVertex2f(rotatedbottomrightX+width, rotatedbottomrightY+height);
			glTexCoord2f(0,1);
			glVertex2f(rotatedbottomleftX, rotatedbottomleftY+height);
		glEnd();
		
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	public static void drawSprite(Texture tex, float x, float y, int spritenumber){
		drawSprite(tex.getTextureID(), x, y, spritenumber);
	}
	
	public static void drawSprite(int textureID, float x, float y, int spritenumber){
		if(spritenumber>15||spritenumber<0)throw new IllegalArgumentException("Spritenumber must be between 0 and 15!");
		glBindTexture(GL_TEXTURE_2D, textureID);
		
		float xstart = (float)(spritenumber    *LevelData.TILESIZE)/512f;
		float xend   = (float)((spritenumber+1)*LevelData.TILESIZE)/512f;
		
		x = (int) x;
		y = (int) y;
		
		glBegin(GL_QUADS);
			glTexCoord2f(xstart,0);
			glVertex2f(x, y);
			glTexCoord2f(xend,0);
			glVertex2f(x+LevelData.TILESIZE, y);
			glTexCoord2f(xend,1);
			glVertex2f(x+LevelData.TILESIZE, y+LevelData.TILESIZE);
			glTexCoord2f(xstart,1);
			glVertex2f(x, y+LevelData.TILESIZE);
		glEnd();
		
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	public static void drawMiniRepresentation(int textureID, float x, float y){
		glBindTexture(GL_TEXTURE_2D, textureID);
		
		float xstart = (float)(0    *LevelData.TILESIZE)/512f;
		float xend   = (float)((0+1)*LevelData.TILESIZE)/512f;
		
		x = (int) x;
		y = (int) y;
		
		glBegin(GL_QUADS);
			glTexCoord2f(xstart,0);
			glVertex2f(x, y);
			glTexCoord2f(xend,0);
			glVertex2f(x+16, y);
			glTexCoord2f(xend,1);
			glVertex2f(x+16, y+16);
			glTexCoord2f(xstart,1);
			glVertex2f(x, y+16);
		glEnd();
		
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	public static void drawString(String text, String font, int x, int y, int width, int height){
		drawString(text, font, x, y, width, height, Integer.MAX_VALUE);
	}
	
	public static void drawString(String text, String font, int x, int y, int width, int height, int lettersTillNewLine){
		Texture texFont = fonts.get(font);
		if(texFont == null)throw new IllegalArgumentException("Illegal Font used! Check Brush.getFontNames()");
		final float texWidth = 1f/26f;
		final float texHeight = 1f/4f;
		glBindTexture(GL_TEXTURE_2D, texFont.getTextureID());
		
		
		for(int i = 0; i<text.length(); i++){
			float textureY = 0f;
			float textureX = 0f;
			
			switch(text.charAt(i)){
			case('a'):
				textureY = 0;
				textureX = 0f/26f;
				break;
			case('b'):
				textureY = 0;
				textureX = 1f/26f;
				break;
			case('c'):
				textureY = 0;
				textureX = 2f/26f;
				break;
			case('d'):
				textureY = 0;
				textureX = 3f/26f;
				break;
			case('e'):
				textureY = 0;
				textureX = 4f/26f;
				break;
			case('f'):
				textureY = 0;
				textureX = 5f/26f;
				break;
			case('g'):
				textureY = 0;
				textureX = 6f/26f;
				break;
			case('h'):
				textureY = 0;
				textureX = 7f/26f;
				break;
			case('i'):
				textureY = 0;
				textureX = 8f/26f;
				break;
			case('j'):
				textureY = 0;
				textureX = 9f/26f;
				break;
			case('k'):
				textureY = 0;
				textureX = 10f/26f;
				break;
			case('l'):
				textureY = 0;
				textureX = 11f/26f;
				break;
			case('m'):
				textureY = 0;
				textureX = 12f/26f;
				break;
			case('n'):
				textureY = 0;
				textureX = 13f/26f;
				break;
			case('o'):
				textureY = 0;
				textureX = 14f/26f;
				break;
			case('p'):
				textureY = 0;
				textureX = 15f/26f;
				break;
			case('q'):
				textureY = 0;
				textureX = 16f/26f;
				break;
			case('r'):
				textureY = 0;
				textureX = 17f/26f;
				break;
			case('s'):
				textureY = 0;
				textureX = 18f/26f;
				break;
			case('t'):
				textureY = 0;
				textureX = 19f/26f;
				break;
			case('u'):
				textureY = 0;
				textureX = 20f/26f;
				break;
			case('v'):
				textureY = 0;
				textureX = 21f/26f;
				break;
			case('w'):
				textureY = 0;
				textureX = 22f/26f;
				break;
			case('x'):
				textureY = 0;
				textureX = 23f/26f;
				break;
			case('y'):
				textureY = 0;
				textureX = 24f/26f;
				break;
			case('z'):
				textureY = 0;
				textureX = 25f/26f;
				break;
			case('A'):
				textureY = 1f/4f;
				textureX = 0f/26f;
				break;
			case('B'):
				textureY = 1f/4f;
				textureX = 1f/26f;
				break;
			case('C'):
				textureY = 1f/4f;
				textureX = 2f/26f;
				break;
			case('D'):
				textureY = 1f/4f;
				textureX = 3f/26f;
				break;
			case('E'):
				textureY = 1f/4f;
				textureX = 4f/26f;
				break;
			case('F'):
				textureY = 1f/4f;
				textureX = 5f/26f;
				break;
			case('G'):
				textureY = 1f/4f;
				textureX = 6f/26f;
				break;
			case('H'):
				textureY = 1f/4f;
				textureX = 7f/26f;
				break;
			case('I'):
				textureY = 1f/4f;
				textureX = 8f/26f;
				break;
			case('J'):
				textureY = 1f/4f;
				textureX = 9f/26f;
				break;
			case('K'):
				textureY = 1f/4f;
				textureX = 10f/26f;
				break;
			case('L'):
				textureY = 1f/4f;
				textureX = 11f/26f;
				break;
			case('M'):
				textureY = 1f/4f;
				textureX = 12f/26f;
				break;
			case('N'):
				textureY = 1f/4f;
				textureX = 13f/26f;
				break;
			case('O'):
				textureY = 1f/4f;
				textureX = 14f/26f;
				break;
			case('P'):
				textureY = 1f/4f;
				textureX = 15f/26f;
				break;
			case('Q'):
				textureY = 1f/4f;
				textureX = 16f/26f;
				break;
			case('R'):
				textureY = 1f/4f;
				textureX = 17f/26f;
				break;
			case('S'):
				textureY = 1f/4f;
				textureX = 18f/26f;
				break;
			case('T'):
				textureY = 1f/4f;
				textureX = 19f/26f;
				break;
			case('U'):
				textureY = 1f/4f;
				textureX = 20f/26f;
				break;
			case('V'):
				textureY = 1f/4f;
				textureX = 21f/26f;
				break;
			case('W'):
				textureY = 1f/4f;
				textureX = 22f/26f;
				break;
			case('X'):
				textureY = 1f/4f;
				textureX = 23f/26f;
				break;
			case('Y'):
				textureY = 1f/4f;
				textureX = 24f/26f;
				break;
			case('Z'):
				textureY = 1f/4f;
				textureX = 25f/26f;
				break;
			case(' '):
				textureY = 2f/4f;
				textureX = 1f/26f;
				break;
			case('0'):
				textureY = 2f/4f;
				textureX = 2f/26f;
				break;
			case('1'):
				textureY = 2f/4f;
				textureX = 3f/26f;
				break;
			case('2'):
				textureY = 2f/4f;
				textureX = 4f/26f;
				break;
			case('3'):
				textureY = 2f/4f;
				textureX = 5f/26f;
				break;
			case('4'):
				textureY = 2f/4f;
				textureX = 6f/26f;
				break;
			case('5'):
				textureY = 2f/4f;
				textureX = 7f/26f;
				break;
			case('6'):
				textureY = 2f/4f;
				textureX = 8f/26f;
				break;
			case('7'):
				textureY = 2f/4f;
				textureX = 9f/26f;
				break;
			case('8'):
				textureY = 2f/4f;
				textureX = 10f/26f;
				break;
			case('9'):
				textureY = 2f/4f;
				textureX = 11f/26f;
				break;
			case('.'):
				textureY = 2f/4f;
				textureX = 12f/26f;
				break;
			case(':'):
				textureY = 2f/4f;
				textureX = 13f/26f;
				break;
			case(','):
				textureY = 2f/4f;
				textureX = 14f/26f;
				break;
			case(';'):
				textureY = 2f/4f;
				textureX = 15f/26f;
				break;
			case('-'):
				textureY = 2f/4f;
				textureX = 16f/26f;
				break;
			case('_'):
				textureY = 2f/4f;
				textureX = 17f/26f;
				break;
			case('#'):
				textureY = 2f/4f;
				textureX = 18f/26f;
				break;
			case('\''):
				textureY = 2f/4f;
				textureX = 19f/26f;
				break;
			case('+'):
				textureY = 2f/4f;
				textureX = 20f/26f;
				break;
			case('*'):
				textureY = 2f/4f;
				textureX = 21f/26f;
				break;
			case('~'):
				textureY = 2f/4f;
				textureX = 22f/26f;
				break;
			case('!'):
				textureY = 2f/4f;
				textureX = 23f/26f;
				break;
			case('"'):
				textureY = 2f/4f;
				textureX = 24f/26f;
				break;
			case('�'):
				textureY = 2f/4f;
				textureX = 25f/26f;
				break;
			case('$'):
				textureY = 3f/4f;
				textureX = 0f/26f;
				break;
			case('&'):
				textureY = 3f/4f;
				textureX = 1f/26f;
				break;
			case('/'):
				textureY = 3f/4f;
				textureX = 2f/26f;
				break;
			case('('):
				textureY = 3f/4f;
				textureX = 3f/26f;
				break;
			case(')'):
				textureY = 3f/4f;
				textureX = 4f/26f;
				break;
			case('['):
				textureY = 3f/4f;
				textureX = 5f/26f;
				break;
			case(']'):
				textureY = 3f/4f;
				textureX = 6f/26f;
				break;
			case('{'):
				textureY = 3f/4f;
				textureX = 7f/26f;
				break;
			case('}'):
				textureY = 3f/4f;
				textureX = 8f/26f;
				break;
			case('='):
				textureY = 3f/4f;
				textureX = 9f/26f;
				break;
			case('?'):
				textureY = 3f/4f;
				textureX = 10f/26f;
				break;
			case('�'):
				textureY = 3f/4f;
				textureX = 11f/26f;
				break;
			case('`'):
				textureY = 3f/4f;
				textureX = 12f/26f;
				break;
			case('^'):
				textureY = 3f/4f;
				textureX = 13f/26f;
				break;
			case('�'):
				textureY = 3f/4f;
				textureX = 14f/26f;
				break;
			case('<'):
				textureY = 3f/4f;
				textureX = 15f/26f;
				break;
			case('>'):
				textureY = 3f/4f;
				textureX = 16f/26f;
				break;
			case('|'):
				textureY = 3f/4f;
				textureX = 17f/26f;
				break;
			case('@'):
				textureY = 3f/4f;
				textureX = 18f/26f;
				break;
			case('�'):
				textureY = 3f/4f;
				textureX = 19f/26f;
				break;
			case('%'):
				textureY = 3f/4f;
				textureX = 20f/26f;
				break;
			default:
				textureY = 2f/4f;
				textureX = 0f/26f;
				break;
			}
			
			
			glBegin(GL_QUADS);
				glTexCoord2f(textureX, textureY);
				glVertex2f(x, y);
				
				glTexCoord2f(textureX+texWidth, textureY);
				glVertex2f(x+width, y);
				
				glTexCoord2f(textureX+texWidth, textureY+texHeight);
				glVertex2f(x+width, y+height);
				
				glTexCoord2f(textureX, textureY+texHeight);
				glVertex2f(x, y+height);
			glEnd();
			
			x += width;
			if((i+1)%lettersTillNewLine==0){
				y += height;
				x -= width*lettersTillNewLine;
			}
			
		}
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	
	public static Object[] getFontNames(){
		return fonts.values().toArray();
	}
	
	
	
	
	public static int loadTexture(BufferedImage image){
		int width = image.getWidth();
		int height = image.getHeight();
		
		int[] pixelArray = new int[width*height];
		image.getRGB(0, 0, width, height, pixelArray, 0, width);
		
		ByteBuffer buffer = BufferUtils.createByteBuffer(width*height*4); //RGBA
		
		for(int i = 0; i<height; i++){
			for(int k = 0; k<width; k++){
				int pixel = pixelArray[k*width+i];
				//|ALPHA|ROT|GR�N|BLAU| <- jeweils 1 byte
				//Die >BITWEI�EN< Und Verkn�pfungen sind da
				buffer.put((byte)((pixel>>16)&0x000000FF));	//Rot ist im 2. Byte
				buffer.put((byte)((pixel>>8) &0x000000FF));	//Gr�n ist im 3. Byte
				buffer.put((byte)(pixel      &0x000000FF));	//Blau ist im 4. Byte
				buffer.put((byte)((pixel>>24)&0x000000FF)); //Alpha ist im 1. Byte
			}
		}
		
		buffer.flip();
		
		int textureID = glGenTextures();			//Noch nicht benutzte ID bitte
		glBindTexture(GL_TEXTURE_2D, textureID);	//und die dann hier rein
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
		
		return textureID;
	}
}
