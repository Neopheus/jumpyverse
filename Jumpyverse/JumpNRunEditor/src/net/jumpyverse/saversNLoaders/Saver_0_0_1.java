package net.jumpyverse.saversNLoaders;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import net.jumpyverse.entities.Coin;
import net.jumpyverse.entities.DecoObject;
import net.jumpyverse.entities.EnemyObject;
import net.jumpyverse.entities.Goal;
import net.jumpyverse.entities.InteractiveObject;
import net.jumpyverse.entities.Player;
import net.jumpyverse.entities.Trampoline;
import net.jumpyverse.entities.Walker;
import net.jumpyverse.game.LevelData;

class Saver_0_0_1 {
	public static void save(File f, LevelData ld) throws IOException{
		List<Byte> bytes = new ArrayList<Byte>();
		
		writeMagicNumber(bytes);
		writeVersion(bytes, 0, 0, 1);
		writeMapInfo(bytes, ld.getWidth(), ld.getHeight(), ld.GRAVITY, ld.getBackgroundImage(), ld.backgroundScrollFactor, ld.timeScale, ld.windSpeed);
		writePlayerInfo(bytes, ld.getPlayer());
		
		writeGround(bytes, ld.getBackground());
		writeGround(bytes, ld.getPlayground());
		writeGround(bytes, ld.getForeground());
		
		writeDecoInfo(bytes, ld.getDecoSize());
		writeDeco(bytes, ld.getDecoList());
		
		writeInterInfo(bytes, ld.getInterSize());
		writeInter(bytes, ld.getInterList());
		
		writeEnemyInfo(bytes, ld.getEnemySize());
		writeEnemy(bytes, ld.getEnemyList());
		
		
		
		FileOutputStream fos = new FileOutputStream(f);
		byte[] byteArray = new byte[bytes.size()];
		for(int i = 0; i<byteArray.length; i++){
			byteArray[i] = bytes.get(i);
		}
		fos.write(byteArray);
		fos.close();
	}
	
	private static void writePlayerInfo(List<Byte> bytes, Player p) {
		byte[] playerx = ByteBuffer.allocate(4).putFloat(p.getX()).array();
		byte[] playery = ByteBuffer.allocate(4).putFloat(p.getY()).array();
		byte[] playerhealth = ByteBuffer.allocate(4).putInt(p.HEALTH).array();
		byte[] playerdamage = ByteBuffer.allocate(4).putInt(p.DAMAGE).array();
		byte[] playermspeedh = ByteBuffer.allocate(4).putFloat(p.MAXSPEEDHARD).array();
		byte[] playermspeedw = ByteBuffer.allocate(4).putFloat(p.MAXSPEEDWALK).array();
		byte[] playermspeedr = ByteBuffer.allocate(4).putFloat(p.MAXSPEEDRUN).array();
		byte[] playermspeedf = ByteBuffer.allocate(4).putFloat(p.MAXFALLSPEED).array();
		byte[] playeracceleration = ByteBuffer.allocate(4).putFloat(p.ACCELERATION).array();
		byte[] playerfriction = ByteBuffer.allocate(4).putFloat(p.FRICTION).array();
		byte[] playerjumppower = ByteBuffer.allocate(4).putFloat(p.JUMPPOWER).array();
		byte[] playermass = ByteBuffer.allocate(4).putFloat(p.MASS).array();
		
		writeByteArrayToList(bytes, playerx);
		writeByteArrayToList(bytes, playery);
		writeByteArrayToList(bytes, playerhealth);
		writeByteArrayToList(bytes, playerdamage);
		writeByteArrayToList(bytes, playermspeedh);
		writeByteArrayToList(bytes, playermspeedw);
		writeByteArrayToList(bytes, playermspeedr);
		writeByteArrayToList(bytes, playermspeedf);
		writeByteArrayToList(bytes, playeracceleration);
		writeByteArrayToList(bytes, playerfriction);
		writeByteArrayToList(bytes, playerjumppower);
		writeByteArrayToList(bytes, playermass);
	}

	private static void writeMagicNumber(List<Byte> bytes){
		String magicNumber = "JSBMYPJump";
		writeByteArrayToList(bytes, magicNumber.getBytes());
	}
	
	
	private static void writeVersion(List<Byte> bytes, int large, int major, int minor){
		byte[] largeBytes = ByteBuffer.allocate(4).putInt(large).array();
		byte[] majorBytes = ByteBuffer.allocate(4).putInt(major).array();
		byte[] minorBytes = ByteBuffer.allocate(4).putInt(minor).array();
		
		writeByteArrayToList(bytes, largeBytes);
		writeByteArrayToList(bytes, majorBytes);
		writeByteArrayToList(bytes, minorBytes);
	}
	
	private static void writeMapInfo(List<Byte> bytes, int width, int height, int gravity, int backgroundImage, float backgroundScrollFactor, float timeScale, float windSpeed){
		writeByteArrayToList(bytes, ByteBuffer.allocate(4).putInt(width).array());
		writeByteArrayToList(bytes, ByteBuffer.allocate(4).putInt(height).array());
		writeByteArrayToList(bytes, ByteBuffer.allocate(4).putInt(gravity).array());
		writeByteArrayToList(bytes, ByteBuffer.allocate(4).putInt(backgroundImage).array());
		writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(backgroundScrollFactor).array());
		writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(timeScale).array());
		writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(windSpeed).array());
	}
	
	private static void writeGround(List<Byte> bytes, short[][] ground){
		for(int i = 0; i<ground.length; i++){
			for(int k = 0; k<ground[i].length; k++){
				writeByteArrayToList(bytes, ByteBuffer.allocate(2).putShort(ground[i][k]).array());
			}
		}
	}
	
	private static void writeDecoInfo(List<Byte> bytes, int size){
		writeByteArrayToList(bytes, ByteBuffer.allocate(4).putInt(size).array());
	}
	
	private static void writeDeco(List<Byte> bytes, List<DecoObject> decos){
		for(int i = 0; i<decos.size(); i++){
			DecoObject d = decos.get(i);
			writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(d.getX()).array());
			writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(d.getY()).array());
			writeByteArrayToList(bytes, ByteBuffer.allocate(4).putInt(d.getID()).array());
		}
	}
	
	private static void writeInterInfo(List<Byte> bytes, int size){
		writeByteArrayToList(bytes, ByteBuffer.allocate(4).putInt(size).array());
	}
	
	private static void writeInter(List<Byte> bytes, List<InteractiveObject> inters){
		for(int i = 0; i<inters.size(); i++){
			InteractiveObject io = inters.get(i);
			writeByteArrayToList(bytes, ByteBuffer.allocate(2).putShort(io.getID()).array());
			switch(io.getID()){
			case(Trampoline.mainID):
				Trampoline tramp = (Trampoline) io;
				writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(tramp.getX()).array());
				writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(tramp.getY()).array());
				writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(tramp.getJumpPower()).array());
				writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(tramp.getAngle()).array());
				break;
			case(Goal.mainID):
				Goal goal = (Goal) io;
				writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(goal.getX()).array());
				writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(goal.getY()).array());
				writeByteArrayToList(bytes, ByteBuffer.allocate(4).putInt(goal.EXITCODE).array());
				break;
			case(Coin.mainID):
				Coin coin = (Coin) io;
				writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(coin.getX()).array());
				writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(coin.getY()).array());
				break;
			default:
				System.err.println("Saver: Something went wrong at the Saver! Illegal Inter ID! "+io.getID());
				break;
			}
		}
	}
	
	private static void writeEnemyInfo(List<Byte> bytes, int size){
		writeByteArrayToList(bytes, ByteBuffer.allocate(4).putInt(size).array());
	}
	
	private static void writeEnemy(List<Byte> bytes, List<EnemyObject> enemys){
		for(int i = 0; i<enemys.size(); i++){
			EnemyObject eo = enemys.get(i);
			writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(eo.getID()).array());
			switch(eo.getID()){
			case(Walker.mainID):
				Walker walker = (Walker) eo;
				writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(walker.getX()).array());
				writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(walker.getY()).array());
				writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(walker.MAXSPEEDHARD).array());
				writeByteArrayToList(bytes, ByteBuffer.allocate(4).putFloat(walker.MAXSPEEDWALK).array());
				writeByteArrayToList(bytes, ByteBuffer.allocate(4).putInt(walker.HEALTH).array());
				break;
			default:
				System.err.println("Saver: Something went wrong at the Saver! Illegal Enemy ID!");
				break;
			}
		}
	}
	
	private static void writeByteArrayToList(List<Byte> bytes, byte[] bytearray){
		for(int i = 0; i<bytearray.length; i++){
			bytes.add(bytearray[i]);
		}
	}
}
