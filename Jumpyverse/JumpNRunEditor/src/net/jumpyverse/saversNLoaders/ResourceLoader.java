package net.jumpyverse.saversNLoaders;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import net.jumpyverse.entities.BackgroundType;
import net.jumpyverse.entities.BlockType;
import net.jumpyverse.entities.DecoObject;
import net.jumpyverse.entities.Player;
import net.jumpyverse.entities.Walker;

import org.lwjgl.opengl.Display;

public class ResourceLoader {
	public static void load(){
		try {
			File f = new File("BlockTypeList.txt");
			Scanner s = new Scanner(f);
			while(s.hasNext()){
				String path = s.nextLine();
				if(!path.startsWith("#") && !path.trim().equals("")){
					BlockType.loadBlockSheet(path.trim());
				}
			}
			s.close();
		} catch (IOException e1) {
			System.out.println("FATAL ERROR: Problem with BlockTypeList");
			e1.printStackTrace();
			Display.destroy();
			System.exit(5);
		}
		
		//Load Deco
		try {
			File f = new File("DecorativesList.txt");
			Scanner s = new Scanner(f);
			while(s.hasNext()){
				String path = s.nextLine();
				if(!path.startsWith("#") && !path.trim().equals("")){
					DecoObject.loadDecoTexture(path.trim());
				}
			}
			s.close();
		} catch (IOException e1) {
			System.err.println("FATAL ERROR: Problem with DecorativesList");
			e1.printStackTrace();
			Display.destroy();
			System.exit(5);
		}
		
		//Load Backgrounds
		try {
			File f = new File("BackgroundList.txt");
			Scanner s = new Scanner(f);
			while(s.hasNext()){
				String path = s.nextLine();
				if(!path.startsWith("#") && !path.trim().equals("")){
					BackgroundType.loadBackgroundTexture(path.trim());
				}
			}
			s.close();
		} catch (IOException e1) {
			System.err.println("FATAL ERROR: Problem with BackgroundList");
			e1.printStackTrace();
			Display.destroy();
			System.exit(5);
		}
		
		Player.loadTextures();
		Walker.loadTextures();
	}
	
	
}
