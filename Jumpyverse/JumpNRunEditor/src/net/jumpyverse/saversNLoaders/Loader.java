package net.jumpyverse.saversNLoaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import net.jumpyverse.exceptions.CorruptedFileException;
import net.jumpyverse.exceptions.UnsupportedFileException;
import net.jumpyverse.exceptions.UnsupportedVersionException;
import net.jumpyverse.game.LevelData;

public class Loader {
	public static LevelData load(String f) throws IOException, UnsupportedFileException, UnsupportedVersionException, CorruptedFileException{
		return load(new File(f));
	}
	
	public static LevelData load(File f) throws IOException, UnsupportedFileException, UnsupportedVersionException, CorruptedFileException{
		byte[] bytes = new byte[(int) f.length()];
		
		FileInputStream fis = new FileInputStream(f);
		fis.read(bytes);
		fis.close();
		
		int pointer = 10;
		byte[] byteLarge = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
		byte[] byteMajor = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
		byte[] byteMinor = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
		
		int large = ByteBuffer.wrap(byteLarge).getInt();
		int major = ByteBuffer.wrap(byteMajor).getInt();
		int minor = ByteBuffer.wrap(byteMinor).getInt();
		
		switch(large){
		case(0):
			switch(major){
			case(0):
				switch(minor){
				case(1):
					return Loader_0_0_1.load(f);
				default: 
					throw new UnsupportedFileException();
				}
			default:
				throw new UnsupportedVersionException();
			}
		default:
			for(int i = 0; i<byteLarge.length; i++){
				System.out.println(byteLarge[i]);
			}
			System.out.println(large);
			throw new UnsupportedVersionException();
		}
	}
}
