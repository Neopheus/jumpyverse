package net.jumpyverse.saversNLoaders;

import java.io.File;
import java.io.IOException;

import net.jumpyverse.game.LevelData;

public class Saver {
	public static void save(File f, LevelData ld) throws IOException{
		Saver_0_0_1.save(f, ld);
	}
	
	public static void save(String f, LevelData ld) throws IOException{
		save(new File(f), ld);
	}
}
