package net.jumpyverse.saversNLoaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import net.jumpyverse.entities.Coin;
import net.jumpyverse.entities.DecoObject;
import net.jumpyverse.entities.Goal;
import net.jumpyverse.entities.Trampoline;
import net.jumpyverse.entities.Walker;
import net.jumpyverse.exceptions.CorruptedFileException;
import net.jumpyverse.exceptions.UnsupportedFileException;
import net.jumpyverse.exceptions.UnsupportedVersionException;
import net.jumpyverse.game.LevelData;

class Loader_0_0_1 {
	private static int pointer = 0;
	
	public static LevelData load(byte[] bytes) throws CorruptedFileException{
		pointer = 10;
		
		try{
			
			if(bytes.length<10){
				throw new UnsupportedFileException();
			}
			
			
			checkMagicNumber(bytes);
			checkVersion(bytes);

			byte[] byteWidth =  {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] byteHeight = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] byteGravity = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] byteBackgroundImage = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] byteBackgroundScrollFactor = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] byteTimeScale = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] byteWindSpeed = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			
			
			
			int width  = ByteBuffer.wrap(byteWidth).getInt();
			int height = ByteBuffer.wrap(byteHeight).getInt();
			int gravity = ByteBuffer.wrap(byteGravity).getInt();
			int backgroundImage = ByteBuffer.wrap(byteBackgroundImage).getInt();
			float backgroundScrollFactor = ByteBuffer.wrap(byteBackgroundScrollFactor).getFloat();
			float timeScale = ByteBuffer.wrap(byteTimeScale).getFloat();
			float windSpeed = ByteBuffer.wrap(byteWindSpeed).getFloat();
			
			
			LevelData ld = new LevelData(width, height);
			
			ld.GRAVITY = gravity;
			ld.setBackgroundImage(backgroundImage);
			ld.backgroundScrollFactor = backgroundScrollFactor;
			ld.timeScale = timeScale;
			ld.windSpeed = windSpeed;
			
			byte[] playerx = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] playery = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] playerhealth = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] playerdamage = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] playermspeedh = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] playermspeedw = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] playermspeedr = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] playermspeedf = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] playeracceleration = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] playerfriction = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] playerjumppower = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			byte[] playermass = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			
			ld.getPlayer().setX(ByteBuffer.wrap(playerx).getFloat());
			ld.getPlayer().setY(ByteBuffer.wrap(playery).getFloat());
			ld.getPlayer().HEALTH = ByteBuffer.wrap(playerhealth).getInt();
			ld.getPlayer().DAMAGE = ByteBuffer.wrap(playerdamage).getInt();
			ld.getPlayer().MAXSPEEDHARD = ByteBuffer.wrap(playermspeedh).getFloat();
			ld.getPlayer().MAXSPEEDWALK = ByteBuffer.wrap(playermspeedw).getFloat();
			ld.getPlayer().MAXSPEEDRUN = ByteBuffer.wrap(playermspeedr).getFloat();
			ld.getPlayer().MAXFALLSPEED = ByteBuffer.wrap(playermspeedf).getFloat();
			ld.getPlayer().ACCELERATION = ByteBuffer.wrap(playeracceleration).getFloat();
			ld.getPlayer().FRICTION = ByteBuffer.wrap(playerfriction).getFloat();
			ld.getPlayer().JUMPPOWER = ByteBuffer.wrap(playerjumppower).getFloat();
			ld.getPlayer().MASS = ByteBuffer.wrap(playermass).getFloat();
			
			
			for(int i = 0; i<width; i++){
				for(int k = 0; k<height; k++){
					byte[] byteTile = {bytes[pointer], bytes[pointer+1]};
					ld.setTile(i, k, LevelData.BACKGROUND, ByteBuffer.wrap(byteTile).getShort());
					pointer += 2;
				}
			}
			for(int i = 0; i<width; i++){
				for(int k = 0; k<height; k++){
					byte[] byteTile = {bytes[pointer], bytes[pointer+1]};
					ld.setTile(i, k, LevelData.PLAYGROUND, ByteBuffer.wrap(byteTile).getShort());
					pointer += 2;
				}
			}
			for(int i = 0; i<width; i++){
				for(int k = 0; k<height; k++){
					byte[] byteTile = {bytes[pointer], bytes[pointer+1]};
					ld.setTile(i, k, LevelData.FOREGROUND, ByteBuffer.wrap(byteTile).getShort());
					pointer += 2;
				}
			}
			
			byte[] byteDecoSize = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			int decoSize = ByteBuffer.wrap(byteDecoSize).getInt();
			
			for(int i = 0; i<decoSize; i++){
				byte[] byteX = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
				byte[] byteY = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
				byte[] byteID = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
				ld.addDeco(new DecoObject(ByteBuffer.wrap(byteX).getFloat(), ByteBuffer.wrap(byteY).getFloat(), ByteBuffer.wrap(byteID).getInt()));
			}
			
			byte[] byteInterSize = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			int interSize = ByteBuffer.wrap(byteInterSize).getInt();
			for(int i = 0; i<interSize; i++){
				byte[] byteInterID = {bytes[pointer++], bytes[pointer++]};
				int interID = ByteBuffer.wrap(byteInterID).getShort();
				switch(interID){
				case(Trampoline.mainID):
					Trampoline tramp = new Trampoline();
					byte[] byteXTramp = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
					byte[] byteYTramp = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
					byte[] byteJumpPower = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
					byte[] byteAngle = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
					
					tramp.setX(ByteBuffer.wrap(byteXTramp).getFloat());
					tramp.setY(ByteBuffer.wrap(byteYTramp).getFloat());
					tramp.setJumpPower(ByteBuffer.wrap(byteJumpPower).getFloat());
					tramp.setAngle(ByteBuffer.wrap(byteAngle).getFloat());
					
					ld.addInter(tramp);
					break;
				case(Goal.mainID):
					Goal goal = new Goal();
					byte[] byteXGoal = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
					byte[] byteYGoal = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
					byte[] byteExitCodeGoal = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
					
					goal.setX(ByteBuffer.wrap(byteXGoal).getFloat());
					goal.setY(ByteBuffer.wrap(byteYGoal).getFloat());
					goal.EXITCODE = ByteBuffer.wrap(byteExitCodeGoal).getInt();
					
					ld.addInter(goal);
					break;
				case(Coin.mainID):
					Coin coin = new Coin();
					byte[] byteXCoin = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
					byte[] byteYCoin = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
					
					coin.setX(ByteBuffer.wrap(byteXCoin).getFloat());
					coin.setY(ByteBuffer.wrap(byteYCoin).getFloat());
					
					ld.addInter(coin);
					break;
				default:
					System.err.println("Something went wrong with the loading of a Interactive Object. ID="+interID);
					throw new CorruptedFileException();
				}
			}
			
			byte[] byteEnemySize = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
			int enemySize = ByteBuffer.wrap(byteEnemySize).getInt();
			for(int i = 0; i<enemySize; i++){
				byte[] byteEnemyID = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
				int enemyID = ByteBuffer.wrap(byteEnemyID).getInt();
				switch(enemyID){
				case(Walker.mainID):
					byte[] byteX = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
					byte[] byteY = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
					byte[] byteSpeedHard = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
					byte[] byteSpeedWalk = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
					byte[] byteHealth = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
					
					float x = ByteBuffer.wrap(byteX).getFloat();
					float y = ByteBuffer.wrap(byteY).getFloat();
					float speedHard = ByteBuffer.wrap(byteSpeedHard).getFloat();
					float speedWalk = ByteBuffer.wrap(byteSpeedWalk).getFloat();
					int health = ByteBuffer.wrap(byteHealth).getInt();
					
					Walker walker = new Walker(x, y);
					walker.MAXSPEEDHARD = speedHard;
					walker.MAXSPEEDWALK = speedWalk;
					walker.health = health;
					walker.HEALTH = health;
					
					ld.addEnemy(walker);
					break;
				default:
					throw new CorruptedFileException();
				}
			}
			
			
			return ld;	
		}catch(Exception e){
			e.printStackTrace();
			throw new CorruptedFileException();
		}
	}
	
	
	public static LevelData load(File f) throws IOException, UnsupportedFileException, UnsupportedVersionException, CorruptedFileException{
		try{
			byte[] bytes = new byte[(int) f.length()];
			pointer = 10;
			
			FileInputStream fis = new FileInputStream(f);
			fis.read(bytes);
			fis.close();
			
			
			return load(bytes);
			
		}catch(Exception e){
			e.printStackTrace();
			throw new CorruptedFileException();
		}
	}
	
	private static void checkMagicNumber(byte[] bytes) throws UnsupportedFileException{
		byte[] byteMagicNumber = new byte[10];
		for(int i = 0; i<byteMagicNumber.length; i++){
			byteMagicNumber[i] = bytes[i];
		}
		String magicNumber = new String(byteMagicNumber);
		
		if(!magicNumber.equals("JSBMYPJump")){
			throw new UnsupportedFileException();
		}
	}
	
	private static void checkVersion(byte[] bytes) throws UnsupportedVersionException{
		byte[] byteLarge = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
		byte[] byteMajor = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
		byte[] byteMinor = {bytes[pointer++], bytes[pointer++], bytes[pointer++], bytes[pointer++]};
		
		int large = ByteBuffer.wrap(byteLarge).getInt();
		int major = ByteBuffer.wrap(byteMajor).getInt();
		int minor = ByteBuffer.wrap(byteMinor).getInt();

		if(large!=0)throw new UnsupportedVersionException();
		if(major!=0)throw new UnsupportedVersionException();
		if(minor!=1)throw new UnsupportedVersionException();
	}
}
