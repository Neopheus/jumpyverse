package net.jumpyverse.exceptions;

public class UnsupportedFileException extends Exception{
	public UnsupportedFileException() {
		super("File is not supported!");
	}
}
