package net.jumpyverse.exceptions;

public class UnsupportedVersionException extends Exception{
	public UnsupportedVersionException() {
		super("Unsupported Version!");
	}
}
