package net.jumpyverse.exceptions;

public class CorruptedFileException extends Exception{
	public CorruptedFileException(){
		super("File got corrupted!");
	}
}
