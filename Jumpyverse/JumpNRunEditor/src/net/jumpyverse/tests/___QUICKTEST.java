package net.jumpyverse.tests;

import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.Trampoline;

public class ___QUICKTEST {
	public static void main(String[] args) {
		for(int i = -10; i<10; i++){
			System.out.println(i + "\t" + modulo(i, 3));
		}

	}
	
	public static float modulo(float value, float div){
		if(value>=0)return value%div;
		else{
			return ((value % div) + div ) % div;
		}
	}
}