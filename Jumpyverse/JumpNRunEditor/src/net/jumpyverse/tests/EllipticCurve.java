package net.jumpyverse.tests;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Random;

import net.jumpyverse.LWJGLCore.Brush;
import net.jumpyverse.LWJGLCore.Screen;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

public class EllipticCurve {
	public static void main(String[] args) {
		try {
			Display.setDisplayMode(new DisplayMode(Screen.width, Screen.height));
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, Screen.width, Screen.height, 0, 1, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);	//Transparenz zulassen
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		
		float a = -1;
		float b = 1f;
		float c = 1;
		float d = 1;
		Brush.setColor(Color.GREEN);
		Random random = new Random();
		
		float change = 1f;
		
		while(!Display.isCloseRequested()){
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
//			if(random.nextBoolean())	a+=change;
//			else						a-=change;
//			if(random.nextBoolean())	b+=change;
//			else						b-=change;
			if(random.nextBoolean())	c+=change;
			else						c-=change;
			if(random.nextBoolean())	d+=change;
			else						d-=change;
			
			a = Mouse.getX()/1000f-0.1f;
			b = Mouse.getY()/1000f-0.1f;
			
//			if(System.currentTimeMillis()%100000<50000){
//				a-=change;
//				b-=change;
//				c-=change;
//				d-=change;
//			}
//			else{
//				a+=change;
//				b+=change;
//				c+=change;
//				d+=change;
//			}
			
			
			
			Brush.setColor(0, 0, 0, 1);
			Brush.fillRect(0, 0, Screen.width, Screen.height);
			for(int pixelx = 0; pixelx<Screen.width; pixelx++){
				for(int pixely = 0; pixely<Screen.height; pixely++){
					float x = pixelx;
					float y = pixely;
					
					x-=Screen.width/2;
					y-=Screen.height/2;
					
					x/=200;
					y/=200;
					
					y*=-1;
					
					float abs = 10000;
//					abs = x*x*x+a*x+b-y*y;
//					abs = x*x*x*x*x+a*x*x*x+b*x+c-y*y;
//					abs = a*x*x+b*x+c-y;
//					abs = (float)Math.sin(a*x)+(float)Math.sin(b*y);
					abs = x*x+y*y-1;
					
					
					abs = Math.abs(abs);
					if(abs<0.1f){
						Brush.setColor(0, -abs*10+1, 0, 1);
						Brush.drawPixel(pixelx, pixely);
					}
				}
			}
			
			Display.update();
		}
		
		Display.destroy();
	}
}
