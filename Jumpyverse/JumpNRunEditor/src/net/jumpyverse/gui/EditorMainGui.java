package net.jumpyverse.gui;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import net.jumpyverse.LWJGLCore.JKeyboard;
import net.jumpyverse.LWJGLCore.Screen;
import net.jumpyverse.editor.Main;
import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.*;
import net.jumpyverse.enums.ClickMode;
import net.jumpyverse.exceptions.CorruptedFileException;
import net.jumpyverse.exceptions.UnsupportedFileException;
import net.jumpyverse.exceptions.UnsupportedVersionException;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.utils.ImageHelper;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

public class EditorMainGui extends JFrame {
	// This class provides the main GUI for the level editor.

	public static String fileName = "Jumpyverse - Editor";
	private JFrame frame;
	private static JPanel selectedP;
	public static JPanel objP;
	private static JKeyboard keyB = new JKeyboard();
	// newControl is 1 if a new world has just been created
	public static int newControl = 0;

	// ##############
	// ##COMPONENTS##
	// ##############

	// panels
	private JPanel menuPanel = new JPanel();
	private JPanel sidePanel = new JPanel();
	private JPanel selectionPanel = new JPanel();
	private JPanel attributePanel = new JPanel();
	private JPanel canvasPanel = new JPanel();

	// panels for element selection
	private JPanel holderPanel = new JPanel();
	private JPanel blocksPanel = new JPanel();
	private JPanel activeBlocksPanel = new JPanel();
	private JPanel interactivesPanel = new JPanel();
	private JPanel decorationPanel = new JPanel();
	private JPanel enemiesPanel = new JPanel();
	private JPanel playerSelecPanel = new JPanel();

	// dropdown menu for object selection
	private final String[] objectsForSelection = { "Blocks", "Interactives",
			"Decoration", "Enemies", "Player" };
	private JComboBox<String> objectSelection = new JComboBox<String>(
			objectsForSelection);

	// canvas for world editing
	private Canvas canvas = new Canvas();

	// menubars, later added to menuPanel
	private JMenuBar menuBar = new JMenuBar();
	private JToolBar toolBar = new JToolBar();

	// components for toolbar
	private JCheckBox snapBox = new JCheckBox("Snap to grid", true);
	private JCheckBox viewBox = new JCheckBox("Ingame view");

	private JButton runTestButton = new JButton(new ImageIcon(
			ImageHelper.loadImage("resgui/runtest.png")));

	private ButtonGroup rbuttons = new ButtonGroup();
	private JRadioButton rb1 = new JRadioButton("Background");
	private JRadioButton rb2 = new JRadioButton("Playground");
	private JRadioButton rb3 = new JRadioButton("Foreground");

	private JToggleButton fillerToolButton = new JToggleButton(new ImageIcon(
			ImageHelper.loadImage("resgui/fillerIcon.png")));

	private JButton brushMinus = new JButton(new ImageIcon(
			ImageHelper.loadImage("resgui/brushMinus.png")));
	private JLabel brushLabel = new JLabel();
	private JButton brushPlus = new JButton(new ImageIcon(
			ImageHelper.loadImage("resgui/brushPlus.png")));

	// menus for "File" and "Setting"
	private JMenu fileMenu = new JMenu("File");
	private JMenu optionsMenu = new JMenu("Options");

	// ###############
	// ##CONSTRUCTOR##
	// ###############

	public EditorMainGui() {
		BorderLayout borderLayout = new BorderLayout();
		setLayout(borderLayout);

		selectedP = blocksPanel;

		// adding components to menu
		fileMenu.add(newMenu);
		fileMenu.add(saveMenu);
		fileMenu.add(saveAsMenu);
		fileMenu.add(openMenu);

		optionsMenu.add(openSettings);
		optionsMenu.add(openHelp);

		// // adding components to toolbar
		toolBar.add(saveMenu);

		toolBar.add(new JLabel("   "));

		toolBar.add(unDoTool);
		toolBar.add(reDoTool);

		toolBar.add(new JLabel("   "));

		snapBox.setToolTipText("Place selected Elements according to grid");
		snapBox.addActionListener(new snapListener());

		toolBar.add(snapBox);
		viewBox.addActionListener(new ViewSelectionListener());
		toolBar.add(viewBox);

		toolBar.add(new JLabel("   "));

		runTestButton.setToolTipText("Test your Level");
		runTestButton.addActionListener(new runTestListener());
		toolBar.add(runTestButton);

		toolBar.add(new JLabel("   "));

		fillerToolButton.setToolTipText("Fill area with selected block");
		fillerToolButton.addItemListener(new ItemListener() {
			// fillertool is selected if button is toggled
			public void itemStateChanged(ItemEvent ev) {
				if (ev.getStateChange() == ItemEvent.SELECTED) {
					ClickMode.currentClickMode = ClickMode.fill;
				} else if (ev.getStateChange() == ItemEvent.DESELECTED) {
					ClickMode.currentClickMode = ClickMode.block;
				}
			}
		});
		toolBar.add(fillerToolButton);

		toolBar.add(new JLabel("      "));

		brushMinus.addActionListener(new BrushSizeListener());
		toolBar.add(brushMinus);
		brushLabel.setText("Brushsize: " + Session.brushSize + " ");
		toolBar.add(brushLabel);
		brushPlus.addActionListener(new BrushSizeListener());
		toolBar.add(brushPlus);

		toolBar.add(new JLabel("      "));

		rb1.addActionListener(new GroundSelectionListener());
		rb2.addActionListener(new GroundSelectionListener());
		rb3.addActionListener(new GroundSelectionListener());
		rbuttons.add(rb1);
		rbuttons.add(rb2);
		rbuttons.add(rb3);
		toolBar.add(rb1);
		toolBar.add(rb2);
		toolBar.add(rb3);
		rb2.setSelected(true);

		// adding menus for menubar and toolbar
		menuBar.add(fileMenu);
		menuBar.add(optionsMenu);

		menuPanel.setLayout(new GridLayout(2, 1, 1, 1));
		menuPanel.add(menuBar);
		menuPanel.add(toolBar);

		// adding elements to selectionpanel
		selectionPanel.setLayout(new BorderLayout());
		selectionPanel.setBorder(BorderFactory.createEtchedBorder());
		objectSelection.addActionListener(new objectSelectionListener());
		selectionPanel.add(objectSelection, BorderLayout.NORTH);

		// configurations for element selection panels
		GridLayout gridlayout = new GridLayout(0, 5, 1, 1);

		// configuring the panels which hold the selectable elements
		blocksPanel.setLayout(gridlayout);
		blocksPanel.setVisible(true);

		activeBlocksPanel.setLayout(gridlayout);
		activeBlocksPanel.setVisible(false);

		interactivesPanel.setLayout(gridlayout);
		interactivesPanel.setVisible(false);

		decorationPanel.setLayout(gridlayout);
		decorationPanel.setVisible(false);

		enemiesPanel.setLayout(gridlayout);
		enemiesPanel.setVisible(false);

		playerSelecPanel.setLayout(gridlayout);
		playerSelecPanel.setVisible(false);

		// adding element selection panels to holderPanel
		holderPanel.add(blocksPanel);
		holderPanel.add(activeBlocksPanel);
		holderPanel.add(interactivesPanel);
		holderPanel.add(decorationPanel);
		holderPanel.add(enemiesPanel);
		holderPanel.add(playerSelecPanel);
		selectionPanel.add(new JScrollPane(holderPanel), BorderLayout.CENTER);

		// adding elements to attributepanel
		attributePanel.setBorder(BorderFactory.createEtchedBorder());

		// adding selectionpanel and attributepanel to sidepanel
		sidePanel.setLayout(new GridLayout(2, 1, 1, 1));
		sidePanel.add(selectionPanel);
		sidePanel.add(attributePanel);
		sidePanel.setPreferredSize(new Dimension(224, 10000));

		// adding canvasPanel
		canvasPanel.setLayout(null);
		canvas.setSize(new Dimension(Screen.width, Screen.height));
		canvas.setPreferredSize(new Dimension(Screen.width, Screen.height));
		canvasPanel.add(canvas);

		// adding menupanel, sidepanel and canvas
		add(menuPanel, BorderLayout.NORTH);
		add(sidePanel, BorderLayout.WEST);
		add(canvasPanel, BorderLayout.CENTER);

		// JKeyboard setup
		new JKeyboard();

		// adding WindowListener
		addWindowListener(new CloseListener());

		int width = Screen.width + 240;
		int height = Screen.height + 105;
		setPreferredSize(new Dimension(width, height));
		setSize(width, height);
		setResizable(false);
		setTitle("Jumpyverse - Editor");
		setVisible(true);
		setIconImage(ImageHelper.loadImage("resgui/editoricon.png"));
		giveJKeyboard(EditorMainGui.this);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}

	// #############
	// ##LISTENERS##
	// #############

	class CloseListener extends WindowAdapter {
		public void windowClosing(WindowEvent e) {
			new CloseGui();
		}
	}

	// action for "New"
	AbstractAction newMenu = new AbstractAction("New", new ImageIcon(
			ImageHelper.loadImage("resgui/new.png"))) {
		public void actionPerformed(ActionEvent e) {
			// TODO
			// if a new level has been created, newControl is set to 1 to reset
			// the title
			new NewGui();
			if (newControl == 1) {
				resetTitle();
			}
			newControl = 0;
		}
	};

	// action for "Save as", saves level to specified file
	AbstractAction saveAsMenu = new AbstractAction("Save as", new ImageIcon(
			ImageHelper.loadImage("resgui/saveAs.png"))) {
		public void actionPerformed(ActionEvent e) {
			saveLevel();
		}
	};

	// action for "Save"
	AbstractAction saveMenu = new AbstractAction("Save", new ImageIcon(
			ImageHelper.loadImage("resgui/save.png"))) {
		public void actionPerformed(ActionEvent e) {
			// open filechooser if level hasn't been saved before
			if (fileName.equals("Jumpyverse - Editor")) {
				saveLevel();
			} else {
				try {
					Main.save(new File(fileName));
				} catch (IOException ioE) {
					ioE.printStackTrace();
					JOptionPane
							.showMessageDialog(
									frame,
									"An IO Exception occurred. Level could not be saved.",
									"Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	};

	// action for "Open", loads Level from a file into the editor
	AbstractAction openMenu = new AbstractAction("Open", new ImageIcon(
			ImageHelper.loadImage("resgui/open.png"))) {
		public void actionPerformed(ActionEvent e) {
			// JFileChooser chooser = new JFileChooser();
			EditorFileChooser chooser = new EditorFileChooser();
			int option = chooser.showOpenDialog(null);
			if (option == JFileChooser.APPROVE_OPTION) {
				try {
					Main.load(chooser.getSelectedFile());
					fileName = chooser.getSelectedFile().getAbsolutePath();
					changeTitle();
				} catch (IOException ex) {
					ex.printStackTrace();
					JOptionPane
							.showMessageDialog(
									frame,
									"An IO Exception occurred. Level could not be loaded.",
									"Error", JOptionPane.ERROR_MESSAGE);
				} catch (UnsupportedFileException ex) {
					JOptionPane
							.showMessageDialog(
									frame,
									"File is not supported. Level could not be loaded.",
									"Error", JOptionPane.ERROR_MESSAGE);
				} catch (UnsupportedVersionException ex) {
					JOptionPane
							.showMessageDialog(
									frame,
									"File version is not supported. Level could not be loaded.",
									"Error", JOptionPane.ERROR_MESSAGE);
				} catch (CorruptedFileException ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(frame,
							"File is corrupted. Level could not be loaded.",
							"Error", JOptionPane.ERROR_MESSAGE);
				} catch (Exception ex) {
					JOptionPane
							.showMessageDialog(
									frame,
									"Something terrible happened. Level could not be loaded.",
									"Unspecified Error",
									JOptionPane.ERROR_MESSAGE);
				}
			}
		}

	};

	// opens the "Settings" window
	AbstractAction openSettings = new AbstractAction("Settings", new ImageIcon(
			ImageHelper.loadImage("resgui/settings.png"))) {
		public void actionPerformed(ActionEvent e) {
			new SettingsGui();
		}
	};

	// opens the "Help" window
	AbstractAction openHelp = new AbstractAction("Help", new ImageIcon(
			ImageHelper.loadImage("resgui/infos.png"))) {
		public void actionPerformed(ActionEvent e) {
			new HelpGui();
		}
	};

	// listener for objectSelection, sets chosen element panel visible

	class objectSelectionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Object selected = (objectSelection.getSelectedItem());
			// removing content of the attribute panel
			attributePanel.removeAll();
			attributePanel.updateUI();

			// can't select filler/brush tool/ground selection if blocks are not
			// selected
			if (!selected.equals("Blocks")) {
				fillerToolButton.setSelected(false);
				fillerToolButton.setEnabled(false);
				brushEnabler(false);

				rb2.setSelected(true);
				Session.activeGround = LevelData.PLAYGROUND;
				rb1.setEnabled(false);
				rb2.setEnabled(false);
				rb3.setEnabled(false);
			}

			if (selected.equals("Blocks")) {
				panelSwitcher(blocksPanel);
				ClickMode.currentClickMode = ClickMode.block;
				fillerToolButton.setEnabled(true);
				brushEnabler(true);
				rb1.setEnabled(true);
				rb2.setEnabled(true);
				rb3.setEnabled(true);
			}

			if (selected.equals("Active Blocks")) {
				panelSwitcher(activeBlocksPanel);
				// ClickMode.currentClickMode = ClickMode.block;
			}

			if (selected.equals("Interactives")) {
				panelSwitcher(interactivesPanel);
				ClickMode.currentClickMode = ClickMode.interactive;
			}

			if (selected.equals("Decoration")) {
				panelSwitcher(decorationPanel);
				ClickMode.currentClickMode = ClickMode.deco;
			}

			if (selected.equals("Enemies")) {
				panelSwitcher(enemiesPanel);
				ClickMode.currentClickMode = ClickMode.enemy;
			}

			if (selected.equals("Player")) {
				panelSwitcher(playerSelecPanel);
				ClickMode.currentClickMode = ClickMode.player;
				attributePanel.add(new JScrollPane(new PlayerPanel()));

			}
		}
	}

	// starts a testrun
	class runTestListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (!Session.inTestMode) {
				// removing content of the attribute panel
				if (!objectSelection.getSelectedItem().equals("Player")) {
					attributePanel.removeAll();
					attributePanel.updateUI();
				}

				Main.startTestMode();
				runTestButton.setIcon(new ImageIcon(ImageHelper
						.loadImage("resgui/stoptest.png")));
				switchChildComponentsEnabler(EditorMainGui.this, false);
				runTestButton.setEnabled(true);
			} else {
				Main.stopTestMode();
				runTestButton.setIcon(new ImageIcon(ImageHelper
						.loadImage("resgui/runtest.png")));
				switchChildComponentsEnabler(EditorMainGui.this, true);

			}

		}
	};

	// action for "Snap to Grid"
	class snapListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (snapBox.isSelected()) {
				Session.snapToGrid = true;
			} else {
				Session.snapToGrid = false;
			}
		}
	}

	// action for "Undo"
	AbstractAction unDoTool = new AbstractAction("Undo", new ImageIcon(
			ImageHelper.loadImage("resgui/undo.png"))) {
		public void actionPerformed(ActionEvent e) {
			Main.undo();
		}
	};

	// action for "Redo"
	AbstractAction reDoTool = new AbstractAction("Redo", new ImageIcon(
			ImageHelper.loadImage("resgui/redo.png"))) {
		public void actionPerformed(ActionEvent e) {
			Main.redo();
		}
	};

	// listener for viewSelection
	class ViewSelectionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (viewBox.isSelected()) {
				Session.viewMode = Session.VIEWMODE_GAME;
			} else {
				Session.viewMode = Session.VIEWMODE_EDITOR;
			}
		}
	}

	// listener for groundSelection
	class GroundSelectionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (rb1.isSelected()) {
				Session.activeGround = LevelData.BACKGROUND;
			}
			if (rb2.isSelected()) {
				Session.activeGround = LevelData.PLAYGROUND;
			}
			if (rb3.isSelected()) {
				Session.activeGround = LevelData.FOREGROUND;
			}
		}
	}

	// listener for brushsize
	class BrushSizeListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			attributePanel.removeAll();
			attributePanel.updateUI();
			// brush size is restricted to values between 1 and 12
			if (e.getSource() == brushMinus) {

				if (Session.brushSize > 2) {
					Session.brushSize--;
				}
				if (Session.brushSize == 2) {
					Session.brushSize--;
					brushMinus.setEnabled(false);
				}
				if (Session.brushSize < 2) {
					brushMinus.setEnabled(false);
				}
				brushPlus.setEnabled(true);
			} else {

				if (Session.brushSize < 11) {
					Session.brushSize++;
				}
				if (Session.brushSize == 11) {
					Session.brushSize++;
					brushPlus.setEnabled(false);
				}
				if (Session.brushSize > 11)
					brushPlus.setEnabled(false);

				brushMinus.setEnabled(true);
			}
			brushLabel.setText("Brushsize: " + Session.brushSize + " ");
		}
	}

	// ######################
	// ##SETTINGS FOR LWJGL##
	// ######################

	public void screenSetup() {
		try {
			Display.setDisplayMode(new DisplayMode(Screen.width, Screen.height));
			Display.setParent(canvas);
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}

		// init OpenGL
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, Screen.width, Screen.height, 0, 1, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND); // Transparenz zulassen
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		canvas.requestFocus();
	}

	public void canvasRequestFocus() {
		canvas.requestFocus();
	}

	// ################################
	// ##LOADING ELEMENTS INTO PANELS##
	// ################################

	public void loadMenuContent() {
		// loading blocks
		BufferedImage[] bis = BlockType.getBiRepresentations();
		String[] toolTipStrings = BlockType.getStringRepresentations();
		for (short i = 0; i < bis.length; i++) {
			if (bis[i] != null) {
				for (int k = 0; k < 1; k++) {
					JButton button = new JButton(new ImageIcon(bis[i]));
					button.setPreferredSize(new Dimension(37, 37));
					button.setSize(37, 37);
					final short j = i;
					button.addActionListener(new ActionListener() {
						private final short ID = j;

						@Override
						public void actionPerformed(ActionEvent arg0) {
							Session.leftClickTileIDBlock = j;
						}
					});
					button.setToolTipText(toolTipStrings[i]);
					blocksPanel.add(button);
				}

			}
		}

		blocksPanel.updateUI();

		// loading decoration
		// WIP
		bis = DecoObject.getBiRepresentations();
		toolTipStrings = DecoObject.getStringRepresentations();
		for (short i = 0; i < bis.length; i++) {
			if (bis[i] != null) {
				for (int k = 0; k < 1; k++) {
					JButton button = new JButton(new ImageIcon(bis[i]));
					button.setPreferredSize(new Dimension(37, 37));
					button.setSize(37, 37);
					final short j = i;
					button.addActionListener(new ActionListener() {
						private final short ID = j;

						@Override
						public void actionPerformed(ActionEvent arg0) {
							Session.leftClickTileIDDeco = j;
						}
					});
					button.setToolTipText(toolTipStrings[i]);
					decorationPanel.add(button);
				}

			}
		}
		decorationPanel.updateUI();

		/*
		 * ################## ###INTERACTIVES### ##################
		 */
		{
			JButton button = new JButton(new ImageIcon(
					Trampoline.biRepresentation));
			button.setPreferredSize(new Dimension(37, 37));
			button.setSize(37, 37);
			final short j = Trampoline.mainID;
			button.addActionListener(new ActionListener() {
				private final short ID = j;

				@Override
				public void actionPerformed(ActionEvent arg0) {
					Session.leftClickTileIDInter = j;
				}
			});
			button.setToolTipText("Trampoline");
			interactivesPanel.add(button);
		}
		
		{
			JButton button = new JButton(new ImageIcon(
					Goal.biRepresentation));
			button.setPreferredSize(new Dimension(37, 37));
			button.setSize(37, 37);
			final short j = Goal.mainID;
			button.addActionListener(new ActionListener() {
				private final short ID = j;

				@Override
				public void actionPerformed(ActionEvent arg0) {
					Session.leftClickTileIDInter = j;
				}
			});
			button.setToolTipText("Goal");
			interactivesPanel.add(button);
		}
		
		{
			JButton button = new JButton(new ImageIcon(
					Coin.biRepresentation));
			button.setPreferredSize(new Dimension(37, 37));
			button.setSize(37, 37);
			final short j = Coin.mainID;
			button.addActionListener(new ActionListener() {
				private final short ID = j;

				@Override
				public void actionPerformed(ActionEvent arg0) {
					Session.leftClickTileIDInter = j;
				}
			});
			button.setToolTipText("Coin");
			interactivesPanel.add(button);
		}

		interactivesPanel.updateUI();

		/*
		 * ############ ###ENEMYS### ############
		 */
		{
			JButton button = new JButton(new ImageIcon(Walker.biRepresentation));
			button.setPreferredSize(new Dimension(37, 37));
			button.setSize(37, 37);
			final short j = Walker.mainID;
			button.addActionListener(new ActionListener() {
				private final short ID = j;

				@Override
				public void actionPerformed(ActionEvent arg0) {
					Session.leftClickTileIDEnemy = j;
				}
			});
			button.setToolTipText("Walker");
			enemiesPanel.add(button);
		}

		enemiesPanel.updateUI();

	}

	// #####################
	// ##UTILITY FUNCTIONS##
	// #####################

	// save Level
	public void saveLevel() {
		EditorFileChooser chooser = new EditorFileChooser();
		int option = chooser.showSaveDialog(null);
		if (option == JFileChooser.APPROVE_OPTION) {
			try {
				File f = chooser.getSelectedFile();
				// appending ".jmpy" to filename
				String filePath = f.getPath();
				if (!filePath.toLowerCase().endsWith(".jmpy")) {
					f = new File(filePath + ".jmpy");
				}
				Main.save(f);
				fileName = f.getAbsolutePath();
				changeTitle();
			} catch (IOException ioE) {
				ioE.printStackTrace();
				JOptionPane.showMessageDialog(frame,
						"An IO Exception occurred. Level could not be saved.",
						"Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	// switching the visibility of the element panels
	public void panelSwitcher(JPanel p) {
		selectedP.setVisible(false);
		selectedP = p;
		p.setVisible(true);
	}

	// switch between enabling /disabling the childcomponents of a container
	public void switchChildComponentsEnabler(Container co, boolean bo) {
		Component[] compArray = co.getComponents();
		for (int i = 0; i < compArray.length; i++) {
			if (compArray[i].getClass() != Canvas.class) {
				compArray[i].setEnabled(bo);
				// recursive method calls on all childs
				switchChildComponentsEnabler((Container) compArray[i], bo);
			}
		}
	}

	// adding a JKeyboard to all components
	public void giveJKeyboard(Container co) {
		Component[] compArray = co.getComponents();
		for (int i = 0; i < compArray.length; i++) {
			if (compArray[i].getClass() != Canvas.class) {
				compArray[i].addKeyListener(keyB);
				// recursive method calls on all childs
				giveJKeyboard((Container) compArray[i]);
			}
		}
	}

	// setting a new title for the editor window
	public void changeTitle() {
		this.setTitle("Jumpyverse - " + fileName);
	}

	// resetting the title
	public void resetTitle() {
		fileName = "Jumpyverse - Editor";
		this.setTitle("Jumpyverse - Editor");
	}

	// disabler for "Undo", used in Main
	public void unDoDisable() {
		unDoTool.setEnabled(false);
	}

	// enabler for "Undo", used in Main
	public void unDoEnable() {
		unDoTool.setEnabled(true);
	}

	// disabler for "Redo", used in Main
	public void reDoDisable() {
		reDoTool.setEnabled(false);
	}

	// enabler for "Redo", used in Main
	public void reDoEnable() {
		reDoTool.setEnabled(true);
	}

	// shows/hides the attributes for interactive objects (trampolins etc)
	public void updateAttributePanel() {
		if (Session.selectedInteractive != null) {
			attributePanel.removeAll();
			objP = Session.selectedInteractive.getObjectPanel();
			if (objP != null)
				attributePanel.add(objP);
		} else {
			attributePanel.removeAll();
		}
		attributePanel.updateUI();
	}

	// shows/hides the attributes for the player
	public void playerAttributes(boolean bo) {
		if (bo) {
			attributePanel.add(new PlayerPanel());
		} else {
			attributePanel.removeAll();
		}
		attributePanel.updateUI();
	}

	// enables/disables the brushtool
	public void brushEnabler(boolean bo) {
		brushMinus.setEnabled(bo);
		brushPlus.setEnabled(bo);
	}

	// TESTCODE!
	// //loading the elements into their panels
	// public void loadMenuContent(GameObject obj, JPanel panel){
	// //loading blocks
	// BufferedImage[] bis = obj.getBiRepresentations();
	// String[] toolTipStrings = obj.getStringRepresentations();
	// for(short i = 0; i<bis.length; i++){
	// if(bis[i] != null){
	// for(int k=0; k<1; k++){
	// JButton button = new JButton(new ImageIcon(bis[i]));
	// button.setPreferredSize(new Dimension(37,37));
	// button.setSize(37, 37);
	// final short j = i;
	// button.addActionListener(new ActionListener(){
	// private final short ID = j;
	// @Override
	// public void actionPerformed(ActionEvent arg0) {
	// Session.leftClickTileIDBlock = j;
	// }
	// });
	// button.setToolTipText(toolTipStrings[i]);
	// panel.add(button);
	// }
	//
	// }
	// }
	//
	// panel.updateUI();
	// }

}
