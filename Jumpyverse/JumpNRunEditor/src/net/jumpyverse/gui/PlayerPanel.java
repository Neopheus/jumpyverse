package net.jumpyverse.gui;

import java.awt.GridLayout;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.Player;

public class PlayerPanel extends JPanel {
	// This class defines the panel for setting the attributes of the player
	// TODO
	// change to sessionObject?
	private Player player = Session.tempLevel.getPlayer();

	private static int healthInt;
	private static int damageInt;
	private static int maxSpeedInt;
	private static int walkSpeedInt;
	private static int runSpeedInt;
	private static int fallSpeedInt;
	private static int accelerationInt;
	private static int frictionInt;
	private static int jumpPowerInt;
	private static int massInt;

	private TitledBorder playerBorder = BorderFactory
			.createTitledBorder("Player attributes");

	private JPanel playPanel = new JPanel();
	private NumberFormat nf = NumberFormat.getIntegerInstance();

	// Labels and input fields
	private JLabel healthLabel = new JLabel("Health: ");
	private JLabel damageLabel = new JLabel("Damage: ");
	private JLabel maxSpeedLabel = new JLabel("Max. speed: ");
	private JLabel walkSpeedLabel = new JLabel("Walk speed: ");
	private JLabel runSpeedLabel = new JLabel("Run speed: ");
	private JLabel fallSpeedLabel = new JLabel("Max. fall speed: ");
	private JLabel accelerationLabel = new JLabel("Acceleration: ");
	private JLabel frictionLabel = new JLabel("Friction: ");
	private JLabel jumpPowerLabel = new JLabel("Jump power: ");
	private JLabel massLabel = new JLabel("Mass: ");

	private JFormattedTextField healthField;
	private JFormattedTextField damageField;
	private JFormattedTextField maxSpeedField;
	private JFormattedTextField walkSpeedField;
	private JFormattedTextField runSpeedField;
	private JFormattedTextField fallSpeedField;
	private JFormattedTextField accelerationField;
	private JFormattedTextField frictionField;
	private JFormattedTextField jumpPowerField;
	private JFormattedTextField massField;

	public PlayerPanel() {
		playPanel.setBorder(playerBorder);
		playPanel.setLayout(new GridLayout(0, 2, 10, 10));

		healthInt = player.HEALTH;
		damageInt = player.DAMAGE;
		maxSpeedInt = (int) player.MAXSPEEDHARD;
		walkSpeedInt = (int) player.MAXSPEEDWALK;
		runSpeedInt = (int) player.MAXSPEEDRUN;
		fallSpeedInt = (int) player.MAXFALLSPEED;
		accelerationInt = (int) player.ACCELERATION;
		frictionInt = (int) player.FRICTION;
		jumpPowerInt = (int) player.JUMPPOWER;
		massInt = (int) player.MASS;

		// prevent dots from appearing in the returned number: "10500" instead
		// of "10.500"
		nf.setGroupingUsed(false);

		// configuring the input fields
		healthField = new JFormattedTextField(nf);
		damageField = new JFormattedTextField(nf);
		maxSpeedField = new JFormattedTextField(nf);
		walkSpeedField = new JFormattedTextField(nf);
		runSpeedField = new JFormattedTextField(nf);
		fallSpeedField = new JFormattedTextField(nf);
		accelerationField = new JFormattedTextField(nf);
		frictionField = new JFormattedTextField(nf);
		jumpPowerField = new JFormattedTextField(nf);
		massField = new JFormattedTextField(nf);

		// initialize with current values
		healthField.setText("" + healthInt);
		damageField.setText("" + damageInt);
		maxSpeedField.setText("" + maxSpeedInt);
		walkSpeedField.setText("" + walkSpeedInt);
		runSpeedField.setText("" + runSpeedInt);
		fallSpeedField.setText("" + fallSpeedInt);
		accelerationField.setText("" + accelerationInt);
		frictionField.setText("" + frictionInt);
		jumpPowerField.setText("" + jumpPowerInt);
		massField.setText("" + massInt);

		// adding listeners
		healthField.addCaretListener(new FieldListener());
		damageField.addCaretListener(new FieldListener());
		maxSpeedField.addCaretListener(new FieldListener());
		walkSpeedField.addCaretListener(new FieldListener());
		runSpeedField.addCaretListener(new FieldListener());
		fallSpeedField.addCaretListener(new FieldListener());
		accelerationField.addCaretListener(new FieldListener());
		frictionField.addCaretListener(new FieldListener());
		jumpPowerField.addCaretListener(new FieldListener());
		massField.addCaretListener(new FieldListener());

		// adding components
		playPanel.add(healthLabel);
		playPanel.add(healthField);
		playPanel.add(damageLabel);
		playPanel.add(damageField);
		playPanel.add(maxSpeedLabel);
		playPanel.add(maxSpeedField);
		playPanel.add(walkSpeedLabel);
		playPanel.add(walkSpeedField);
		playPanel.add(runSpeedLabel);
		playPanel.add(runSpeedField);
		playPanel.add(fallSpeedLabel);
		playPanel.add(fallSpeedField);
		playPanel.add(accelerationLabel);
		playPanel.add(accelerationField);
		playPanel.add(frictionLabel);
		playPanel.add(frictionField);
		playPanel.add(jumpPowerLabel);
		playPanel.add(jumpPowerField);
		playPanel.add(massLabel);
		playPanel.add(massField);

		this.add(playPanel);
	}

	
	//listener for updating the attributes
	private class FieldListener implements CaretListener {
		@Override
		public void caretUpdate(CaretEvent ev) {
			JFormattedTextField sourceField = (JFormattedTextField) ev
					.getSource();
			// empty fields will not trigger an update
			if (!sourceField.getText().equals("")) {
				// catch bad user input
				try {
					//get the current player, important due to commit functionality
					player = Session.tempLevel.getPlayer();
					
					if (sourceField == healthField) {
						healthInt = Integer.parseInt(healthField.getText());
						player.HEALTH = healthInt;
						System.out.println("health: " + player.HEALTH);
					}
					if (sourceField == damageField) {
						damageInt  = Integer.parseInt(damageField.getText());
						player.DAMAGE = damageInt ;
						System.out.println("damage: " + player.DAMAGE);
					}
					if (sourceField == maxSpeedField) {
						maxSpeedInt = Integer.parseInt(maxSpeedField.getText());
						player.MAXSPEEDHARD = maxSpeedInt;
						System.out.println("maxSpeed: " + player.MAXSPEEDHARD);
					}
					if (sourceField == walkSpeedField) {
						walkSpeedInt = Integer.parseInt(walkSpeedField.getText());
						player.MAXSPEEDWALK = walkSpeedInt;
						System.out.println("walkSpeed: " + player.MAXSPEEDWALK);
					}
					if (sourceField == runSpeedField) {
						runSpeedInt = Integer.parseInt(runSpeedField.getText());
						player.MAXSPEEDRUN = runSpeedInt;
						System.out.println("runSpeed: " + player.MAXSPEEDRUN);
					}
					if (sourceField == fallSpeedField) {
						fallSpeedInt = Integer.parseInt(fallSpeedField.getText());
						player.MAXFALLSPEED = fallSpeedInt;
						System.out.println("fallSpeed: " + player.MAXFALLSPEED);
					}
					if (sourceField == accelerationField) {
						accelerationInt = Integer.parseInt(accelerationField.getText());
						player.ACCELERATION = accelerationInt;
						System.out.println("acceleration: " + player.ACCELERATION);
					}
					if (sourceField == frictionField) {
						frictionInt = Integer.parseInt(frictionField.getText());
						player.FRICTION = frictionInt;
						System.out.println("friction: " + player.FRICTION);
					}
					if (sourceField == jumpPowerField) {
						jumpPowerInt = Integer.parseInt(jumpPowerField.getText());
						player.JUMPPOWER = jumpPowerInt;
						System.out.println("jumpPower: " + player.JUMPPOWER);
					}
					if (sourceField == massField) {
						massInt = Integer.parseInt(massField.getText());
						player.MASS = massInt;
						System.out.println("mass: " + player.MASS);
					}
				} catch (NumberFormatException e) {
					System.out.println(e);
				}
			}
		}
	}
}
