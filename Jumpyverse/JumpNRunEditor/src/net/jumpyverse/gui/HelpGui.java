package net.jumpyverse.gui;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.io.IOException;

import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import net.jumpyverse.utils.ImageHelper;



//This class defines the help window, the textual content is defined in a separate html file
public class HelpGui extends JDialog{
	int width = 600;
	int height = 600;

	HelpGui(){
		super(null, "Help", Dialog.ModalityType.APPLICATION_MODAL); 
		setLayout(new BorderLayout());		
		
		JEditorPane helpPane = createEditorPane();
		JScrollPane scrollPane = new JScrollPane(helpPane);
		scrollPane.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setPreferredSize(new Dimension(width, height));
		scrollPane.setMinimumSize(new Dimension(10, 10));
		
		add(scrollPane, BorderLayout.NORTH);
		setAutoRequestFocus(true);
		pack();
		setIconImage(ImageHelper.loadImage("resgui/editoricon.png"));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible (true);
	}
	
	//creating the editorpane with the html page to be displayed
	private JEditorPane createEditorPane() {
        JEditorPane editorPane = new JEditorPane();
        editorPane.setEditable(false);
        java.net.URL helpURL = HelpGui.class.getResource(
                                        "/resgui/helpText.html");
        if (helpURL != null) {
            try {
                editorPane.setPage(helpURL);
            } catch (IOException e) {
                System.err.println("Attempted to read a bad URL: " + helpURL);
            }
        } else {
            System.err.println("Couldn't find file: helpText.html");
        }

        return editorPane;
    }
}
