package net.jumpyverse.gui;

import java.awt.Dialog;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.BackgroundType;
import net.jumpyverse.utils.ImageHelper;

public class SettingsGui extends JDialog {

	private JPanel mainPanel = new JPanel();
	int width = 800;
	int height = 600;

	// ##############
	// ##WORLD SIZE##
	// ##############

	TitledBorder sizeBorder = BorderFactory
			.createTitledBorder("Change level size by rows");

	JFrame frame;
	private static boolean updateCaret = true;

	// values for parameters
	private int worldWidth = Session.tempLevel.getWidth();
	private int worldHeight = Session.tempLevel.getHeight();
	private int hOffset = 0;
	private int vOffset = 0;

	private int gravInt = Session.tempLevel.GRAVITY;
	private float timeFloat = Session.tempLevel.timeScale;
	private float backgroundScrollFactorFloat = Session.tempLevel.backgroundScrollFactor;

	// values for display
	private static int topLabelInt = 0;
	private static int rightLabelInt = 0;
	private static int bottomLabelInt = 0;
	private static int leftLabelInt = 0;

	private JButton sizeOkButton = new JButton("OK");
	private JButton sizeResetButton = new JButton("Reset");

	// Buttons for adding/deleting rows
	private JButton topPlus = new JButton("+");
	private JButton topMinus = new JButton("-");
	private JButton rightPlus = new JButton("+");
	private JButton rightMinus = new JButton("-");
	private JButton bottomPlus = new JButton("+");
	private JButton bottomMinus = new JButton("-");
	private JButton leftPlus = new JButton("+");
	private JButton leftMinus = new JButton("-");

	// Panels containing the plus/minus buttons
	private JPanel topPanel = new JPanel();
	private JPanel rightPanel = new JPanel();
	private JPanel bottomPanel = new JPanel();
	private JPanel leftPanel = new JPanel();

	// Labels containing the amount of rows changed
	private JLabel topLabel = new JLabel("" + topLabelInt);
	private JLabel rightLabel = new JLabel("" + rightLabelInt);
	private JLabel bottomLabel = new JLabel("" + bottomLabelInt);
	private JLabel leftLabel = new JLabel("" + leftLabelInt);

	private JPanel changeSizePanel = new JPanel();

	private JLabel topImage = new JLabel(new ImageIcon(
			ImageHelper.loadImage("resgui/settingsTop.png")));
	private JLabel rightImage = new JLabel(new ImageIcon(
			ImageHelper.loadImage("resgui/settingsRight.png")));
	private JLabel bottomImage = new JLabel(new ImageIcon(
			ImageHelper.loadImage("resgui/settingsBottom.png")));
	private JLabel leftImage = new JLabel(new ImageIcon(
			ImageHelper.loadImage("resgui/settingsLeft.png")));

	// ##################
	// ##LEVEL SETTINGS##
	// ##################

	TitledBorder backgroundBorder = BorderFactory
			.createTitledBorder("Level settings");

	private JPanel settingsPanel = new JPanel();

	private JLabel backgroundLabel = new JLabel("Background: ");
	private JLabel gravLabel = new JLabel("Gravity: ");
	private JLabel timeLabel = new JLabel("Game speed: ");
	private JLabel backgroundScrollFactorLabel = new JLabel("Background speed: ");

	
	//TODO
	private NumberFormat nf = NumberFormat.getIntegerInstance();
	private NumberFormat ff = new DecimalFormat("####.##");
	
	
	
	private JFormattedTextField gravField;
	private JFormattedTextField timeField;
	private JFormattedTextField backgroundScrollFactorField;

	private String[] backgroundsForSelection = null;
	private JComboBox<String> backgroundSelection = null;
	

	// ###############
	// ##CONSTRUCTOR##
	// ###############

	SettingsGui() {
		super(null, "Settings", Dialog.ModalityType.APPLICATION_MODAL);

		List<String> stringreps = new ArrayList<String>();
		stringreps.add("none");
		for (int i = 0; i < BackgroundType.getStringRepresentations().length; i++) {
			if (BackgroundType.getStringRepresentations()[i] != null) {
				stringreps.add(BackgroundType.getStringRepresentations()[i]);
			}
		}
		backgroundsForSelection = new String[stringreps.toArray().length];

		for (int i = 0; i < backgroundsForSelection.length; i++) {
			backgroundsForSelection[i] = stringreps.get(i);
		}

		backgroundSelection = new JComboBox<String>(backgroundsForSelection);
		backgroundSelection.setSelectedIndex(Session.tempLevel
				.getBackgroundImage());

		// creating the change size ui

		topPanel.setLayout(new GridLayout(2, 1, 1, 1));
		rightPanel.setLayout(new GridLayout(2, 1, 1, 1));
		bottomPanel.setLayout(new GridLayout(2, 1, 1, 1));
		leftPanel.setLayout(new GridLayout(2, 1, 1, 1));

		// adding Listeners
		topPlus.addActionListener(new topPlusListener());
		topMinus.addActionListener(new topMinusListener());
		rightPlus.addActionListener(new rightPlusListener());
		rightMinus.addActionListener(new rightMinusListener());
		bottomPlus.addActionListener(new bottomPlusListener());
		bottomMinus.addActionListener(new bottomMinusListener());
		leftPlus.addActionListener(new leftPlusListener());
		leftMinus.addActionListener(new leftMinusListener());

		sizeResetButton.addActionListener(new SizeResetListener());
		sizeOkButton.addActionListener(new SizeOkListener());

		// adding plus/minus buttons
		topPanel.add(topPlus);
		topPanel.add(topMinus);
		rightPanel.add(rightPlus);
		rightPanel.add(rightMinus);
		bottomPanel.add(bottomPlus);
		bottomPanel.add(bottomMinus);
		leftPanel.add(leftPlus);
		leftPanel.add(leftMinus);

		// adding components for changing the size
		changeSizePanel.setBorder(sizeBorder);
		changeSizePanel.setLayout(new GridLayout(0, 3, 20, 20));
		changeSizePanel.add(topImage);
		changeSizePanel.add(topPanel);
		changeSizePanel.add(topLabel);

		changeSizePanel.add(rightImage);
		changeSizePanel.add(rightPanel);
		changeSizePanel.add(rightLabel);

		changeSizePanel.add(bottomImage);
		changeSizePanel.add(bottomPanel);
		changeSizePanel.add(bottomLabel);

		changeSizePanel.add(leftImage);
		changeSizePanel.add(leftPanel);
		changeSizePanel.add(leftLabel);

		// adding the "Reset" and "OK" buttons
		changeSizePanel.add(sizeResetButton);
		changeSizePanel.add(new JLabel());
		changeSizePanel.add(sizeOkButton);

		mainPanel.add(changeSizePanel);

		// adding the misc settings
		//TODO
		// prevent dots from appearing in the returned number: "10500" instead
				// of "10.500"
				nf.setGroupingUsed(false);
//				ff.setGroupingUsed(false);

		// creating the change background ui
		settingsPanel.setLayout(new GridLayout(0, 2, 20, 20));
		settingsPanel.setBorder(backgroundBorder);
		settingsPanel.add(backgroundLabel);
		backgroundSelection.addActionListener(new backgroundListener());
		settingsPanel.add(backgroundSelection);

		// creating the gravity ui
		settingsPanel.add(gravLabel);
		gravField = new JFormattedTextField(nf);
		gravField.setText("" + gravInt);
		gravField.addCaretListener(new gravListener());
		settingsPanel.add(gravField);
		
		//TODO
		//creating the game speed ui
		settingsPanel.add(timeLabel);
		timeField = new JFormattedTextField(ff);
		timeField.setText("" + timeFloat);
		//timeField.addCaretListener(new timeListener());
		settingsPanel.add(timeField);
		
		
		//creating the background scroll factor ui
		settingsPanel.add(backgroundScrollFactorLabel);
		backgroundScrollFactorField = new JFormattedTextField(ff);
		backgroundScrollFactorField.setText("" + backgroundScrollFactorFloat);
		settingsPanel.add(backgroundScrollFactorField);

		mainPanel.add(settingsPanel);

		add(mainPanel);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				Session.rs.commit(Session.tempLevel);
				Session.tempLevel = Session.tempLevel.clone();
			}
		});
		setAutoRequestFocus(true);
		setSize(width, height);

		setIconImage(ImageHelper.loadImage("resgui/editoricon.png"));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	// ############
	// ##LISTENER##
	// ############

	// Listeners for changing size
	private class topPlusListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			worldHeight += 1;
			topLabelInt += 1;
			topLabel.setText("" + topLabelInt);
			vOffset += 1;

		}
	}

	private class topMinusListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			worldHeight -= 1;
			topLabelInt -= 1;
			topLabel.setText("" + topLabelInt);
			vOffset -= 1;

		}
	}

	private class rightPlusListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			worldWidth += 1;
			rightLabelInt += 1;
			rightLabel.setText("" + rightLabelInt);
			hOffset += 0;

		}
	}

	private class rightMinusListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			worldWidth -= 1;
			rightLabelInt -= 1;
			rightLabel.setText("" + rightLabelInt);
			hOffset += 0;

		}
	}

	private class bottomPlusListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			worldHeight += 1;
			bottomLabelInt += 1;
			bottomLabel.setText("" + bottomLabelInt);
			vOffset += 0;

		}
	}

	private class bottomMinusListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			worldHeight -= 1;
			bottomLabelInt -= 1;
			bottomLabel.setText("" + bottomLabelInt);
			vOffset += 0;

		}
	}

	private class leftPlusListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			worldWidth += 1;
			leftLabelInt += 1;
			leftLabel.setText("" + leftLabelInt);
			hOffset += 1;

		}
	}

	private class leftMinusListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			worldWidth -= 1;
			leftLabelInt -= 1;
			leftLabel.setText("" + leftLabelInt);
			hOffset -= 1;

		}
	}

	// Listener for "OK" Button

	private class SizeOkListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			if (worldWidth < 32 || worldHeight < 18
					|| worldWidth * worldHeight > 1048576) {
				JOptionPane
						.showMessageDialog(
								frame,
								"The world size you set is invalid.\n\n Minimum size:\n    Width:  32\n    Height: 18\n\n Maximum size:\n    Width*Height = 1048576 (1024*1024)\n\n Size you set:\n    Width:  "
										+ worldWidth
										+ "\n    Height: "
										+ worldHeight, "Invalid world size",
								JOptionPane.ERROR_MESSAGE);
				resetValues();
			} else {

				Session.tempLevel.setWidthHeight(worldWidth, worldHeight,
						hOffset, vOffset);

				Session.rs.commit(Session.tempLevel);
				Session.tempLevel = Session.tempLevel.clone();
				resetValues();
				JOptionPane.showMessageDialog(
						frame,
						"World size changed.\n Height: "
								+ Session.tempLevel.getHeight() + "\n Width: "
								+ Session.tempLevel.getWidth(), "Confirmed",
						JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}

	// Listener for "Reset" Button
	private class SizeResetListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			resetValues();

		}
	}

	// listener for changing the background
	private class backgroundListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			Session.tempLevel.setBackgroundImage(backgroundSelection
					.getSelectedIndex());
			// Session.rs.commit(Session.tempLevel);
		}

	}

	// listener for changing the gravity
	private class gravListener implements CaretListener {

		@Override
		public void caretUpdate(CaretEvent arg0) {
			if (!gravField.getText().equals("")) {
				// catch bad user input
				try {
					gravInt = Integer.parseInt(gravField.getText());
					Session.tempLevel.GRAVITY = gravInt;
				} catch (NumberFormatException e) {
					System.out.println(e);
				}
			}
		}
	}
	
	//TODO
	// listener for changing game speed
	private class timeListener implements CaretListener {

		@Override
		public void caretUpdate(CaretEvent arg0) {
			if (!timeField.getText().equals("") && updateCaret) {
				// catch bad user input
				try {
					timeFloat = Float.parseFloat(timeField.getText());
					Session.tempLevel.timeScale = timeFloat;
					System.out.println("" + Session.tempLevel.timeScale);
				} catch (NumberFormatException e) {
					System.out.println(e);
				}
			}
		}
	}
	
	// listener for changing background scroll speed

	// resets the local values of the variables for world size
	private void resetValues() {
		worldWidth = Session.tempLevel.getWidth();
		worldHeight = Session.tempLevel.getHeight();
		hOffset = 0;
		vOffset = 0;

		topLabelInt = 0;
		rightLabelInt = 0;
		bottomLabelInt = 0;
		leftLabelInt = 0;

		topLabel.setText("" + topLabelInt);
		rightLabel.setText("" + rightLabelInt);
		bottomLabel.setText("" + bottomLabelInt);
		leftLabel.setText("" + leftLabelInt);
	}
}
