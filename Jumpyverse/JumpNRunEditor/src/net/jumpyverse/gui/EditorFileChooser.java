package net.jumpyverse.gui;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

//This class provides a custom filter for *.jmpy files in the file chooser
public class EditorFileChooser extends JFileChooser {
	FileFilter filter = new FileNameExtensionFilter("JMPY file", "jmpy");
	
	public EditorFileChooser(){
		setFileFilter(filter);
		setDialogTitle("Jumpyverse - Choose file");
	}
}
