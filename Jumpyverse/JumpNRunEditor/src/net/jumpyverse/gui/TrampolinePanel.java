package net.jumpyverse.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.Trampoline;

public class TrampolinePanel extends JPanel{
	//This class defines the panel for setting the angle and jump power of a selected trampoline
	
	private static int angleInt;
	private static int powerInt;
	private Trampoline tramp;
	
	private TitledBorder trampBorder = BorderFactory
	.createTitledBorder("Trampoline attributes");
	
	private JPanel trampPanel = new JPanel();
	
	private JLabel angleLabel = new JLabel("Angle: ");
	private JLabel powerLabel = new JLabel("Jump power: ");
	
	private NumberFormat nf = NumberFormat.getIntegerInstance();
	 
	
	private JFormattedTextField angleField;
	private JFormattedTextField powerField;
	
	JButton setDefaultButton = new JButton("Save");
	JButton getDefaultButton = new JButton("Load");
	
	public TrampolinePanel(){
		//prevent dots from appearing in the returned number: "10500" instead of "10.500"
		nf.setGroupingUsed(false);
		angleField = new JFormattedTextField(nf);
		powerField = new JFormattedTextField(nf);
		
		if(Session.selectedInteractive instanceof Trampoline){
			tramp = (Trampoline) Session.selectedInteractive;
		}
		
		angleInt = (int) tramp.getAngle();
		powerInt = (int) tramp.getJumpPower();
		
		angleField.setText("" + angleInt);
		powerField.setText("" + powerInt);
		
		angleField.addCaretListener(new AngleListener());
		powerField.addCaretListener(new AngleListener());
		
		trampPanel.setBorder(trampBorder);
		trampPanel.setLayout(new GridLayout(0,2,10,10));
		trampPanel.add(angleLabel);
		trampPanel.add(angleField);
		trampPanel.add(powerLabel);
		trampPanel.add(powerField);
		trampPanel.add(new JLabel("Default values:"));
		trampPanel.add(new JLabel());
		setDefaultButton.addActionListener(new saveListener());
		getDefaultButton.addActionListener(new loadListener());
		trampPanel.add(setDefaultButton);
		trampPanel.add(getDefaultButton);
		this.add(trampPanel);
	}
	
	//listener for setting the angles and the power
	private class AngleListener implements CaretListener{

		@Override
		public void caretUpdate(CaretEvent arg0) {
			//empty fields will not trigger an update
			if (!angleField.getText().equals("") && !powerField.getText().equals("")){
				//catch bad user input
				try{
					angleInt = Integer.parseInt(angleField.getText());
					powerInt = Integer.parseInt(powerField.getText());
					
					tramp.setAngle(angleInt);
					tramp.setJumpPower(powerInt);
					
					System.out.println("update: " + angleInt);
					System.out.println("update: " + powerInt);
				}
				catch (NumberFormatException e){
					System.out.println(e);
				}				
			}
		}
	}
	
	//setting the current values as default values for future trampolines
	private class saveListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			Trampoline.defaultAngle = angleInt;
			Trampoline.defaultJumpPower = powerInt;
			
			System.out.println("save: " + angleInt);
			System.out.println("save: " + powerInt);
		}		
	}
	
	//loading the default values for trampolines
	private class loadListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			//DO NOT CHANGE SEQUENCE OF COMMANDS!!!
			//setText results in an update of both fields, leading to wrong behaviour if not done this way
			
//			System.out.println("load1: " + angleInt);
//			System.out.println("load1: " + powerInt);
			
			angleInt = (int) Trampoline.defaultAngle;
			tramp.setAngle(angleInt);
			angleField.setText("" + angleInt);
			
//			System.out.println("load2: " + angleInt);
//			System.out.println("load2: " + powerInt);
			
			powerInt = (int) Trampoline.defaultJumpPower;
			tramp.setJumpPower(powerInt);
			powerField.setText("" + powerInt);
		}		
	}
}
