package net.jumpyverse.game;

import static net.jumpyverse.editor.Session.rs;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.jumpyverse.editor.Session;
import net.jumpyverse.entities.BlockType;
import net.jumpyverse.entities.DecoObject;
import net.jumpyverse.entities.EnemyObject;
import net.jumpyverse.entities.InteractiveObject;
import net.jumpyverse.entities.Player;
import net.jumpyverse.wrapper.Point;

public class LevelData implements Cloneable{
	public static final int BACKGROUND = 0;
	public static final int PLAYGROUND = 1;
	public static final int FOREGROUND = 2;
	public static final int TILESIZE = 32;
	public int GRAVITY = 10;
	private static final int MAXFILLERDEPTH = 1000;
	
	private short[][] background = null;
	private short[][] playground = null;
	private short[][] foreground = null;
	private ArrayList<DecoObject> decoground = null;
	private ArrayList<InteractiveObject> interGround = null;
	private ArrayList<EnemyObject> enemys = null;
	private int selectedBackground = 1;
	private int width = 0;
	private int height = 0;
	private Player player = null;
	private Rectangle[][] blocks;
	public static int fillerDepth = 0;
	public static int maxReachedFillerDepth = 0;
	
	public float timeScale = 1f;
	public float backgroundScrollFactor = 0.25f;
	public float windSpeed = 0;
	
	
	private static List<Point> fillerEndings = new ArrayList<Point>();
	
	public LevelData(int width, int height){
		this.width = width;
		this.height = height;
		
		player = new Player(TILESIZE * 4, (height-14)*TILESIZE);
		
		background = new short[width][height];
		playground = new short[width][height];
		foreground = new short[width][height];
		decoground = new ArrayList<DecoObject>();
		interGround = new ArrayList<InteractiveObject>();
		enemys = new ArrayList<EnemyObject>();
	}
	
	@Override
	public LevelData clone(){
		LevelData clonedLevel = new LevelData(width, height);
		for(int i = 0; i<width; i++){
			for(int k = 0; k<height; k++){
				clonedLevel.setTile(i, k, BACKGROUND, getTile(i, k, BACKGROUND));
				clonedLevel.setTile(i, k, PLAYGROUND, getTile(i, k, PLAYGROUND));
				clonedLevel.setTile(i, k, FOREGROUND, getTile(i, k, FOREGROUND));
			}
		}
		clonedLevel.decoground = (ArrayList<DecoObject>) decoground.clone();
		for(int i = 0; i<interGround.size(); i++){
			try {
				clonedLevel.interGround.add((InteractiveObject)interGround.get(i).clone());
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}
		for(int i = 0; i<enemys.size(); i++){
			try {
				clonedLevel.enemys.add((EnemyObject)enemys.get(i).clone());
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}
		clonedLevel.selectedBackground = selectedBackground;
		clonedLevel.player = player.clone();
		clonedLevel.timeScale = timeScale;
		clonedLevel.backgroundScrollFactor = backgroundScrollFactor;
		clonedLevel.GRAVITY = GRAVITY;
		clonedLevel.windSpeed = windSpeed;
		return clonedLevel;
	}
	
	public void fill(int x, int y, int ground, int oldValue, int newValue){
		long anfang = System.currentTimeMillis();
		_fill(x, y, ground, oldValue, newValue, 0);
		while(fillerEndings.size() != 0){
			Point p = fillerEndings.get(0);
			_fill(p.x, p.y, ground, oldValue, newValue, 0);
			fillerEndings.remove(p);
		}
		long ende = System.currentTimeMillis();
		long diff = ende - anfang;
		System.out.println("Needed filler Time: " + diff);
	}
	
	private void _fill(int x, int y, int ground, int oldValue, int newValue, int fillDepth){
		if(x<0||y<0||x>width-1||y>height-1) return;
		
		setTile(x, y, ground, BlockType.getBlockID((short)newValue));
		
		//Anti Stack Overflow
		if(fillDepth > MAXFILLERDEPTH){
			fillerEndings.add(new Point(x, y));
			return;
		}
		
		if(getTile(x-1, y, ground)/BlockType.VARIATIONSPERTEXTURE == oldValue) _fill(x-1, y, ground, oldValue, newValue, fillDepth + 1);
		if(getTile(x+1, y, ground)/BlockType.VARIATIONSPERTEXTURE == oldValue) _fill(x+1, y, ground, oldValue, newValue, fillDepth + 1);
		if(getTile(x, y-1, ground)/BlockType.VARIATIONSPERTEXTURE == oldValue) _fill(x, y-1, ground, oldValue, newValue, fillDepth + 1);
		if(getTile(x, y+1, ground)/BlockType.VARIATIONSPERTEXTURE == oldValue) _fill(x, y+1, ground, oldValue, newValue, fillDepth + 1);
		
	}
	
	
	public void loadIngame(){
		blocks = new Rectangle[width][height];
		for(int i = 0; i<width; i++){
			for(int k = 0; k<height; k++){
				if(playground[i][k] != 0){
					blocks[i][k] = new Rectangle(i*TILESIZE, k*TILESIZE, TILESIZE, TILESIZE);
				}
			}
		}
	}
	
	public void setWidthHeight(int width, int height){
		setWidthHeight(width, height, 0, 0);
	}
	
	public void setWidthHeight(int width, int height, int offsetX, int offsetY){
		if(width*height > 1024*1024) throw new IllegalArgumentException("Width * Height must be smaller than "+(1024*1024));
		short[][] background = new short[width][height];
		short[][] playground = new short[width][height];
		short[][] foreground = new short[width][height];
		
		for(int i = 0; i<width; i++){
			for(int k = 0; k<height; k++){
				background[i][k] = getTile(i-offsetX, k-offsetY, BACKGROUND);
				playground[i][k] = getTile(i-offsetX, k-offsetY, PLAYGROUND);
				foreground[i][k] = getTile(i-offsetX, k-offsetY, FOREGROUND);
			}
		}
		
		for(int i = 0; i<decoground.size(); i++){
			decoground.get(i).addX(+offsetX*TILESIZE);
			decoground.get(i).addY(+offsetY*TILESIZE);
		}
		
		player.addX(offsetX*TILESIZE);
		player.addY(offsetY*TILESIZE);
		
		for(int i = 0; i<interGround.size(); i++){
			interGround.get(i).addX(offsetX*TILESIZE);
			interGround.get(i).addY(offsetY*TILESIZE);
		}
		
		for(int i = 0; i<enemys.size(); i++){
			enemys.get(i).addX(offsetX*TILESIZE);
			enemys.get(i).addY(offsetY*TILESIZE);
		}
		
		this.background = background;
		this.playground = playground;
		this.foreground = foreground;
		
		this.width  = width;
		this.height = height;
	}
	
	public void setTile(int x, int y, int ground, short tileID){
		if(x<0||x>=width||y<0||y>=height)
			return;
		
		switch(ground){
		case(BACKGROUND):
			background[x][y] = tileID;
			break;
		case(PLAYGROUND):
			playground[x][y] = tileID;
			break;
		case(FOREGROUND):
			foreground[x][y] = tileID;
			break;
		default:
			throw new IllegalArgumentException("Please select LevelData.XXXGROUND as third paramenter!");
		}
	}
	
	public void setBackgroundImage(int i){
		selectedBackground = i;
	}
	
	public int getBackgroundImage(){
		return selectedBackground;
	}
	
	public void addDeco(DecoObject deob){
		decoground.add(deob);
	}
	public void removeDeco(DecoObject deob){
		decoground.remove(deob);
	}
	public void addInter(InteractiveObject io){
		interGround.add(io);
	}
	public void removeInter(InteractiveObject io){
		interGround.remove(io);
	}
	public void addEnemy(EnemyObject eo){
		enemys.add(eo);
	}
	public void removeEnemy(EnemyObject eo){
		enemys.remove(eo);
	}
	
	public DecoObject getDeco(float x, float y){
		for(int i = decoground.size()-1; i>=0; i--){
			DecoObject deco = decoground.get(i);
			float decox = deco.getX();
			float decoy = deco.getY();
			if(x>decox && x<decox+DecoObject.size && y>decoy && y<decoy+DecoObject.size){
				return deco;
			}
		}
		return null;
	}
	public DecoObject getDeco(int i){
		return decoground.get(i);
	}
	public int getDecoSize() {
		return decoground.size();
	}
	
	public InteractiveObject getInter(float x, float y){
		for(int i = interGround.size()-1; i>=0; i--){
			InteractiveObject io = interGround.get(i);
			float iox = io.getX();
			float ioy = io.getY();
			if(x>iox && x<iox+io.getWidth() && y>ioy && y<ioy+io.getHeight()){
				return io;
			}
		}
		return null;
	}
	public InteractiveObject getInter(int i){
		return interGround.get(i);
	}
	public int getInterSize() {
		return interGround.size();
	}
	
	
	public EnemyObject getEnemy(float x, float y){
		for(int i = enemys.size()-1; i>=0; i--){
			EnemyObject eo = enemys.get(i);
			float eox = eo.getX();
			float eoy = eo.getY();
			if(x>eox && x<eox+eo.getWidth() && y>eoy && y<eoy+eo.getHeight()){
				return eo;
			}
		}
		return null;
	}
	public EnemyObject getEnemy(int i){
		return enemys.get(i);
	}
	public int getEnemySize(){
		return enemys.size();
	}
	
	public List<DecoObject> getDecoList(){
		return decoground;
	}
	
	public List<InteractiveObject> getInterList(){
		return interGround;
	}
	
	public List<EnemyObject> getEnemyList(){
		return enemys;
	}
	
	
	
	public int getWidth(){
		return width;
	}
	public int getHeight(){
		return height;
	}
	
	public short getTile(int x, int y, int ground){
		if(x<0||x>=width||y<0||y>=height)return 0;
		
		switch(ground){
		case(BACKGROUND):
			return background[x][y];
		case(PLAYGROUND):
			return playground[x][y];
		case(FOREGROUND):
			return foreground[x][y];
		default:
			throw new IllegalArgumentException("Please select LevelData.XXXGROUND as third paramenter!");
		}
	}
	
	public short[][] getBackground(){
		return background;
	}
	public short[][] getPlayground(){
		return playground;
	}
	public short[][] getForeground(){
		return foreground;
	}
	
	public Player getPlayer(){
		return player;
	}
	
	public Rectangle getBlock(int x, int y){
		if(x<0||x>width-1||y<0||y>height-1){
			return null;
		}else{
			return blocks[x][y];
		}
	}
	
	
	public void update(float timeSinceLastFrame){
		for(int i = 0; i<decoground.size(); i++){
			decoground.get(i).update(timeSinceLastFrame, this);
		}
	}
}
