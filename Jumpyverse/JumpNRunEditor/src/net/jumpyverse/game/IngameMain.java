package net.jumpyverse.game;

import static net.jumpyverse.editor.Session.*;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import net.jumpyverse.LWJGLCore.JKeyboard;
import net.jumpyverse.LWJGLCore.Screen;
import net.jumpyverse.exceptions.CorruptedFileException;
import net.jumpyverse.exceptions.UnsupportedFileException;
import net.jumpyverse.exceptions.UnsupportedVersionException;
import net.jumpyverse.gui.EditorFileChooser;
import net.jumpyverse.saversNLoaders.Loader;
import net.jumpyverse.saversNLoaders.ResourceLoader;
import net.jumpyverse.utils.FileHelper;
import net.jumpyverse.utils.ImageHelper;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

public class IngameMain {
	public static void main(String[] args) {
		if(startupMode == VIEWMODE_GAME){
			try {
				Display.setDisplayMode(new DisplayMode(Screen.width, Screen.height));
				Display.setTitle("Jumpyverse");
				
				try {
				    ByteBuffer[] icons = new ByteBuffer[2];
				    icons[0] = ImageHelper.loadIconAsByteBuffer("resgui/editorIcon_16.png", 16, 16);
				    icons[1] = ImageHelper.loadIconAsByteBuffer("resgui/editorIcon_32.png", 32, 32);
				    Display.setIcon(icons);
				} catch (IOException ex) {
				    ex.printStackTrace();
				}
				
				Display.create();
			} catch (LWJGLException e) {
				e.printStackTrace();
				System.exit(1);
			}
			
			EditorFileChooser chooser = new EditorFileChooser();
			chooser.showOpenDialog(null);
			LevelData leveldata = null;
			try {
				File f = chooser.getSelectedFile();
				FileHelper.unZipIt(f.getAbsolutePath());
				File tempFile = new File(f.getAbsoluteFile() + ".temp");
				leveldata = Loader.load(tempFile);
				tempFile.delete();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (UnsupportedFileException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (UnsupportedVersionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			catch (CorruptedFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			tempLevel = leveldata;
			editorState = tempLevel.clone();
			
			
			
			
			GL11.glMatrixMode(GL11.GL_PROJECTION);
			GL11.glLoadIdentity();
			GL11.glOrtho(0, Screen.width, Screen.height, 0, 1, -1);
			GL11.glMatrixMode(GL11.GL_MODELVIEW);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			GL11.glEnable(GL11.GL_BLEND);	//Transparenz zulassen
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			
			ResourceLoader.load();
			
			
			//Keyboard startup
			new JKeyboard();
		}
		
		
		tempLevel.loadIngame();
		
		
		long lastFrame = System.currentTimeMillis();
		float timeSinceLastFrame = 0;
		float workDownTime = 0;
		final float WORKDOWNTIME = 0.002f;
		while(!Display.isCloseRequested()){
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			
			if(startupMode == VIEWMODE_EDITOR && !inTestMode) return;
			
			/*###############
			 *##TIMER STUFF##
			 *###############
			 */
			long thisFrame = System.currentTimeMillis();
			timeSinceLastFrame = ((float)(thisFrame-lastFrame))/1000f * tempLevel.timeScale;
			lastFrame = thisFrame;
			
			if(timeSinceLastFrame > 1)timeSinceLastFrame = 1;
			
			timeSinceStart += timeSinceLastFrame;
			workDownTime += timeSinceLastFrame;
			
			/* ##############################
			 * ##Reset/Update/Poll Keyboard##
			 * ##############################
			 */
			
			JKeyboard.poll();
			
			
			/*################
			 *##UPDATE STUFF##
			 *################
			 */
			
			while(workDownTime>WORKDOWNTIME){
				tempLevel.update(WORKDOWNTIME);	
				tempLevel.getPlayer().update(WORKDOWNTIME, tempLevel);
				for(int i = 0; i<tempLevel.getInterSize(); i++){
					tempLevel.getInter(i).update(WORKDOWNTIME, tempLevel);
				}
				for(int i = 0; i<tempLevel.getEnemySize(); i++){
					tempLevel.getEnemy(i).update(WORKDOWNTIME, tempLevel);
				}
				workDownTime-=WORKDOWNTIME;
			}
			
			
			
			

			/*##############
			 *##DRAW STUFF##
			 *##############
			 */
			//every frame is deleted, then redrawn each cycle
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			Renderer.draw();
			Display.update();
			
			
			
			
			
		
			
			/*############
			 *##NAP TIME##
			 *############
			 */
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		
		Display.destroy();
	}
}
