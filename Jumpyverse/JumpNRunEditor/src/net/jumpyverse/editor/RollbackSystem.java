package net.jumpyverse.editor;

import static net.jumpyverse.editor.Session.tempLevel;

import java.io.File;
import java.io.IOException;

import net.jumpyverse.game.LevelData;

public class RollbackSystem {
	private LevelData[] states;
	private int pointer = 0;
	
	public RollbackSystem(int length, LevelData start){
		if(length<=0)throw new IllegalArgumentException("Rollback System length must be > 0");
		
		states = new LevelData[length];
		states[0] = start;
	}
	
	public void commit(LevelData current){
		if(pointer == 0){
			for(int i = states.length - 1 ; i>0 ; i--){
				states[i] = states[i-1];
			}
			
			states[0] = current;
		}
		else{
			for(int i = 1; i<states.length; i++){
				if(i+pointer-1<states.length) states[i] = states[i+pointer-1];
				else states[i] = null;
			}
			
			pointer--;
			states[0] = current;
			
			pointer = 0;
		}
		
		tempLevel = tempLevel.clone();
		try {
			File f = new File("Autosaves");
			if(!f.exists()) f.mkdirs();
			Main.save(new File("Autosaves/Autosave "+System.currentTimeMillis()+".jmpy"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean hasPrevious(){
		return pointer<states.length-1 && states[pointer+1]!=null;
	}
	public boolean hasNext(){
		return pointer>0;
	}
	
	public void goBack(){
		if(hasPrevious()) pointer++;
	}
	
	public void goForward(){
		if(hasNext()) pointer--;
	}
	
	public LevelData getCurrentState(){
		return states[pointer];
	}
}
