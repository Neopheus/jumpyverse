package net.jumpyverse.editor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.JOptionPane;

import net.jumpyverse.LWJGLCore.JKeyboard;
import net.jumpyverse.LWJGLCore.Screen;
import net.jumpyverse.entities.BlockType;
import net.jumpyverse.entities.Coin;
import net.jumpyverse.entities.DecoObject;
import net.jumpyverse.entities.Goal;
import net.jumpyverse.entities.InteractiveObject;
import net.jumpyverse.entities.Trampoline;
import net.jumpyverse.entities.Walker;
import net.jumpyverse.enums.ClickMode;
import net.jumpyverse.exceptions.CorruptedFileException;
import net.jumpyverse.exceptions.UnsupportedFileException;
import net.jumpyverse.exceptions.UnsupportedVersionException;
import net.jumpyverse.game.IngameMain;
import net.jumpyverse.game.LevelData;
import net.jumpyverse.game.Renderer;
import net.jumpyverse.gui.EditorMainGui;
import net.jumpyverse.saversNLoaders.Loader;
import net.jumpyverse.saversNLoaders.ResourceLoader;
import net.jumpyverse.saversNLoaders.Saver;
import net.jumpyverse.utils.FileHelper;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.TextureLoader;

import static net.jumpyverse.editor.Session.*;

public class Main {
	public static void main(String[] args) {
		Session.viewMode = Session.VIEWMODE_EDITOR;
		Session.startupMode = Session.VIEWMODE_EDITOR;
		editorGUI = new EditorMainGui();
		
		
		try {
			Thread.sleep(50); //Give EDT time to start
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		
		//Load LWJGL, Canvas
		editorGUI.screenSetup();
		
		
		//Load BlockTypes from BlockTypeList.txt
		ResourceLoader.load();
		try {
			logoID = TextureLoader.getTexture("PNG", org.newdawn.slick.util.ResourceLoader.getResourceAsStream("res/editoricon_tricolor.png")).getTextureID();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		
		editorGUI.loadMenuContent();
	
		Renderer.startup();

		long lastFrame = System.currentTimeMillis();
		
		
		
		while (true) {
			
			/*###############
			 *##TIMER STUFF##
			 *###############
			 */
			long thisFrame = System.currentTimeMillis();
			timeSinceLastFrame = ((float)(thisFrame-lastFrame))/1000f;
			lastFrame = thisFrame;
			
			timeSinceStart += timeSinceLastFrame;
			
			
			/*####################
			 *##UPDATE MOUSEINFO##
			 *####################
			 */
			mouseLastX = mouseX;
			mouseLastY = mouseY;
			mouseLastTileX = mouseTileX;
			mouseLastTileY = mouseTileY;
			
			mouseX = Mouse.getX();
			mouseY = -Mouse.getY() + Screen.height - 1;
			mouseDeltaX = mouseX - mouseLastX;
			mouseDeltaY = mouseY - mouseLastY;
			
			mouseTileX = (int) (mouseX + camX);
			mouseTileY = (int) (mouseY + camY);
			
			if(mouseTileX<0)mouseTileX-=LevelData.TILESIZE;
			if(mouseTileY<0)mouseTileY-=LevelData.TILESIZE;
			
			mouseTileX /= LevelData.TILESIZE;
			mouseTileY /= LevelData.TILESIZE;			
			
			mouseDeltaTileX = (mouseTileX-mouseLastTileX);
			mouseDeltaTileY = (mouseTileY-mouseLastTileY);			
			
			mouseLastLeftDown = mouseLeftDown;
			mouseLastRightDown = mouseRightDown;
			mouseLastMiddleDown = mouseMiddleDown;
			
			mouseLeftDown = Mouse.isButtonDown(0);
			mouseRightDown = Mouse.isButtonDown(1);
			mouseMiddleDown = Mouse.isButtonDown(2);
			
			/* ##############################
			 * ##Reset/Update/Poll Keyboard##
			 * ##############################
			 */
			
			JKeyboard.poll();
			
			
			
			/*#################
			 *##MOUSE CONTROL##
			 *#################
			 */
			if(ClickMode.currentClickMode == ClickMode.block){
				if (mouseLeftDown || JKeyboard.isKeyDown(Keyboard.KEY_INSERT)) {
					for (int i = 1000; i >= 1; i--) {
						//placing elements
						int mouseTileX = (int) (mouseX + camX - mouseDeltaX*i/1000);
						int mouseTileY = (int) (mouseY + camY - mouseDeltaY*i/1000);
						
						if(mouseTileX<0)mouseTileX-=LevelData.TILESIZE;
						if(mouseTileY<0)mouseTileY-=LevelData.TILESIZE;
						
						mouseTileX /= LevelData.TILESIZE;
						mouseTileY /= LevelData.TILESIZE;
						for(int k = mouseTileX-brushSize+1; k<mouseTileX+brushSize; k++){
							for(int j = mouseTileY-brushSize+1; j<mouseTileY+brushSize; j++){
								tempLevel.setTile(k, j, Session.activeGround, BlockType.getBlockID(Session.leftClickTileIDBlock));
							}
						}
					}
				}
				if (mouseRightDown || JKeyboard.isKeyDown(Keyboard.KEY_DELETE)) {
					for (int i = 1000; i >= 1; i--) {
						//deleting elements
						int mouseTileX = (int) (mouseX + camX + mouseDeltaX*i/1000);
						int mouseTileY = (int) (mouseY + camY + mouseDeltaY*i/1000);
						
						if(mouseTileX<0)mouseTileX-=LevelData.TILESIZE;
						if(mouseTileY<0)mouseTileY-=LevelData.TILESIZE;
						
						mouseTileX /= LevelData.TILESIZE;
						mouseTileY /= LevelData.TILESIZE;
						for(int k = mouseTileX-brushSize+1; k<mouseTileX+brushSize; k++){
							for(int j = mouseTileY-brushSize+1; j<mouseTileY+brushSize; j++){
								tempLevel.setTile(k, j, Session.activeGround, BlockType.getBlockID(Session.rightClickTileIDBlock));
							}
						}
					}
				}
			}
			else if(ClickMode.currentClickMode == ClickMode.deco){
				if (mouseLeftDown && !mouseLastLeftDown){
					if(!snapToGrid){
						tempLevel.addDeco(new DecoObject(mouseX+camX-DecoObject.size, mouseY+camY-DecoObject.size, Session.leftClickTileIDDeco));
					}else{
						int x = (int) (mouseX+camX-DecoObject.size);
						int y = (int) (mouseY+camY-DecoObject.size);
						
						x /= LevelData.TILESIZE;
						y /= LevelData.TILESIZE;
						
						x++;
						y++;
						
						x *= LevelData.TILESIZE;
						y *= LevelData.TILESIZE;
						
						tempLevel.addDeco(new DecoObject(x, y, Session.leftClickTileIDDeco));
					}
				}
				if (mouseRightDown && !mouseLastRightDown && editorGUI.isActive()){
					Session.selectedDeco = tempLevel.getDeco(mouseX+camX, mouseY+camY);
				}
				if (mouseRightDown && mouseLastRightDown && Session.selectedDeco != null){
					Session.selectedDeco.addX(mouseDeltaX);
					Session.selectedDeco.addY(mouseDeltaY);
				}
				if (Session.selectedDeco != null && JKeyboard.isKeyDown(Keyboard.KEY_DELETE)){
					tempLevel.removeDeco(Session.selectedDeco);
					Session.selectedDeco = null;
					rs.commit(tempLevel);
				}
			}
			else if(ClickMode.currentClickMode == ClickMode.interactive){
				if (mouseLeftDown && !mouseLastLeftDown){
					InteractiveObject io = null;
					switch(leftClickTileIDInter){
					case(Trampoline.mainID):
						Trampoline t = new Trampoline();
						io = t;
						break;
					case(Goal.mainID):
						io = new Goal();
						break;
					case(Coin.mainID):
						io = new Coin();
						break;
					default:
						System.err.println("Illegal Interactive ID! "+leftClickTileIDInter);
						break;
					}
					if(!snapToGrid){
						io.setX(mouseX+camX-DecoObject.size);
						io.setY(mouseY+camY-DecoObject.size);
					}else{
						int x = (int) (mouseX+camX-DecoObject.size);
						int y = (int) (mouseY+camY-DecoObject.size);
						
						x /= LevelData.TILESIZE;
						y /= LevelData.TILESIZE;
						
						x++;
						y++;
						
						x *= LevelData.TILESIZE;
						y *= LevelData.TILESIZE;
						
						io.setX(x - io.getWidth()/2 + LevelData.TILESIZE/2);
						io.setY(y - io.getHeight()/2 + LevelData.TILESIZE/2);
					}
					if(io != null)tempLevel.addInter(io);
				}
				if (mouseRightDown && !mouseLastRightDown && editorGUI.isActive()){   //Abfrage .isActive() muss sein, da sonst das Programm freezed. Grund unbekannt.
					Session.selectedInteractive = tempLevel.getInter(mouseX+camX, mouseY+camY);
					editorGUI.updateAttributePanel();
				}
				if (mouseRightDown && mouseLastRightDown && Session.selectedInteractive != null){
					Session.selectedInteractive.addX(mouseDeltaX);
					Session.selectedInteractive.addY(mouseDeltaY);
				}
				if (Session.selectedInteractive != null && JKeyboard.isKeyDown(Keyboard.KEY_DELETE)){
					tempLevel.removeInter(Session.selectedInteractive);
					Session.selectedInteractive = null;
					rs.commit(tempLevel);
				}
			}
			else if(ClickMode.currentClickMode == ClickMode.fill){
				try{
					int mouseHoverTile = tempLevel.getTile(mouseTileX, mouseTileY, Session.activeGround) / BlockType.VARIATIONSPERTEXTURE;
					if(mouseLeftDown && !mouseLastLeftDown && mouseHoverTile != Session.leftClickTileIDBlock){
						tempLevel.fill(mouseTileX, mouseTileY, Session.activeGround, mouseHoverTile, Session.leftClickTileIDBlock);
					}
					if(mouseRightDown && !mouseLastRightDown && mouseHoverTile != Session.rightClickTileIDBlock){
						tempLevel.fill(mouseTileX, mouseTileY, Session.activeGround, mouseHoverTile, Session.rightClickTileIDBlock);
					}
				}catch(StackOverflowError e){
					if(canUndo()){
						Runnable r = new Runnable() {
							@Override
							public void run() {
								JOptionPane
								.showMessageDialog(
										editorGUI,
										"Woops! Stacksize was not big enough, we are working on it!",
										"Stackerror", JOptionPane.ERROR_MESSAGE);
							}
						};
						
						Thread t = new Thread(r);
						t.start();
					}
				}
			}
			else if(ClickMode.currentClickMode == ClickMode.player){
				if(mouseLeftDown || mouseRightDown){
					tempLevel.getPlayer().setX(mouseX + camX);
					tempLevel.getPlayer().setY(mouseY + camY);
				}
			}
			else if(ClickMode.currentClickMode == ClickMode.enemy){
				if (mouseLeftDown && !mouseLastLeftDown){
					int x = (int) (mouseX+camX);
					int y = (int) (mouseY+camY);
					if(snapToGrid){
						x /= LevelData.TILESIZE;
						y /= LevelData.TILESIZE;
						
						x++;
						y++;
						
						x *= LevelData.TILESIZE;
						y *= LevelData.TILESIZE;
						
					}
					
					switch(Session.leftClickTileIDEnemy){
					case(Walker.mainID):
						tempLevel.addEnemy(new Walker(x, y));
						break;
					default:
						System.err.println("Illegal EnemyID");
						break;
					}
				}
				if (mouseRightDown && !mouseLastRightDown && editorGUI.isActive()){  //Abfrage .isActive() muss sein, da sonst das Programm freezed. Grund unbekannt.
					Session.selectedEnemy = tempLevel.getEnemy(mouseX+camX, mouseY+camY);
					editorGUI.updateAttributePanel();
				}
				if (mouseRightDown && mouseLastRightDown && Session.selectedEnemy != null){
					Session.selectedEnemy.addX(mouseDeltaX);
					Session.selectedEnemy.addY(mouseDeltaY);
				}
				if (Session.selectedEnemy != null && JKeyboard.isKeyDown(Keyboard.KEY_DELETE)){
					tempLevel.removeEnemy(Session.selectedEnemy);
					Session.selectedEnemy = null;
					rs.commit(tempLevel);
				}
			}
			
			/*
			 * ################
			 * ###TEST BLOCK###
			 * ################
			 * 
			 * ALL THINGS HERE SHOULD BE DELETED AT DEPLOYMENT!
			*/
			
			/*###################
			 *##ROLLBACK UPDATE##
			 *###################
			 */
			if(Session.mouseLastLeftDown && !Session.mouseLeftDown){
				rs.commit(tempLevel);
			}
			if(Session.mouseLastRightDown && !Session.mouseRightDown && ClickMode.currentClickMode != ClickMode.interactive && ClickMode.currentClickMode != ClickMode.enemy && ClickMode.currentClickMode != ClickMode.deco){
				rs.commit(tempLevel);
			}
			
			
			/*###############
			 *##CAM CONTROL##
			 *###############
			 */
			if (mouseMiddleDown||JKeyboard.isKeyDown(Keyboard.KEY_LSHIFT)){
				if(fixMousePointX==-10000&&fixMousePointY==-10000){
					fixMousePointX = Session.mouseX;
					fixMousePointY = Session.mouseY;
				}else{
					camX += (Session.mouseX - fixMousePointX) * timeSinceLastFrame * 3;
					camY += (Session.mouseY - fixMousePointY) * timeSinceLastFrame * 3;
				}
			}else{
				fixMousePointX = -10000;
				fixMousePointY = -10000;
			}

			if (JKeyboard.isKeyDown(Keyboard.KEY_UP)||JKeyboard.isKeyDown(Keyboard.KEY_W)) {
				camY -= CAMSPEED*timeSinceLastFrame;
			}
			if (JKeyboard.isKeyDown(Keyboard.KEY_DOWN)||JKeyboard.isKeyDown(Keyboard.KEY_S)) {
				camY += CAMSPEED*timeSinceLastFrame;
			}
			if (JKeyboard.isKeyDown(Keyboard.KEY_LEFT)||JKeyboard.isKeyDown(Keyboard.KEY_A)) {
				camX -= CAMSPEED*timeSinceLastFrame;
			}
			if (JKeyboard.isKeyDown(Keyboard.KEY_RIGHT)||JKeyboard.isKeyDown(Keyboard.KEY_D)) {
				camX += CAMSPEED*timeSinceLastFrame;
			}
			
			if(camX < 0 ) camX = 0;
			if(camY < 0 ) camY = 0;
			
			if(camX > tempLevel.getWidth() * LevelData.TILESIZE - Screen.width) camX = tempLevel.getWidth() * LevelData.TILESIZE - Screen.width;
			if(camY > tempLevel.getHeight() * LevelData.TILESIZE - Screen.height) camY = tempLevel.getHeight() * LevelData.TILESIZE - Screen.height;
			
			
			/*################
			 *##UPDATE STUFF##
			 *################
			 */
			
			tempLevel.update(timeSinceLastFrame);	
			
			
			
			

			/*##############
			 *##DRAW STUFF##
			 *##############
			 */
			//every frame is deleted, then redrawn each cycle
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			Renderer.draw();
			
			frameUpdate();
			Display.update();
			
			
			if(inTestMode){
				editorState = tempLevel.clone();
				editorGUI.canvasRequestFocus();
				viewMode = VIEWMODE_GAME;
				IngameMain.main(null);
				inTestMode = false;
				tempLevel = editorState;
				viewMode = VIEWMODE_EDITOR;
			}
			
			
			
			
			/*############
			 *##NAP TIME##
			 *############
			 */
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
	
	
	/*###################
	 *##Save, Load, New##
	 *###################
	 */
	
	public static void save(final File f) throws IOException{
		final LevelData savefile = tempLevel.clone();
		Thread saverThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					File tempFile = new File(f.getAbsolutePath() + ".temp");
					Saver.save(tempFile, savefile);
					
			    	byte[] buffer = new byte[1024];
			    	 
			    	try{
			 
			    		FileOutputStream fos = new FileOutputStream(f);
			    		ZipOutputStream zos = new ZipOutputStream(fos);
			    		ZipEntry ze= new ZipEntry(f.getName()+".leveldata");
			    		zos.putNextEntry(ze);
			    		FileInputStream in = new FileInputStream(tempFile);
			 
			    		int len;
			    		while ((len = in.read(buffer)) > 0) {
			    			zos.write(buffer, 0, len);
			    		}
			 
			    		in.close();
			    		zos.closeEntry();
			 
			    		zos.close();
			    		
			    		tempFile.delete();
			    	}catch(IOException ex){
			    	   ex.printStackTrace();
			    	}

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		saverThread.start();
	}
	
	public static void load(File f) throws IOException, UnsupportedFileException, UnsupportedVersionException, CorruptedFileException{
		FileHelper.unZipIt(f.getAbsolutePath());
		File tempFile = new File(f.getAbsoluteFile() + ".temp");
		tempLevel = Loader.load(tempFile);
		tempFile.delete();
		rs.commit(tempLevel);
		camX = DEFAULT_CAMX;
		camY = DEFAULT_CAMY;
	}
	
	public static void refreshLevelDate(){
		tempLevel = rs.getCurrentState().clone();
	}
	
	public static void _new(){
		tempLevel = new LevelData(DEFAULT_MAPWIDTH, DEFAULT_MAPHEIGHT);
		rs.commit(tempLevel);
		camX = DEFAULT_CAMX;
		camY = DEFAULT_CAMY;
	}
	
	public static void startTestMode(){
		inTestMode = true;
	}
	
	public static void stopTestMode(){
		inTestMode = false;
	}
	
	
	/*###################################
	 *##FUNCTIONALITY UnDo/ReDo Buttons##
	 *###################################
	 */
	
	public static void undo(){
		rs.goBack();
		tempLevel = rs.getCurrentState().clone();
	}
	
	public static void redo(){
		rs.goForward();
		tempLevel = rs.getCurrentState().clone();
	}
	
	public static boolean canUndo(){
		return rs.hasPrevious();
	}
	
	public static boolean canRedo(){
		return rs.hasNext();
	}
	
	public static void frameUpdate(){
		
		//enabling/disabling undo/redo buttons
		if (canUndo()){
			editorGUI.unDoEnable();
		}
		else editorGUI.unDoDisable();
		
		//enabling/disabling undo/redo buttons
		if (canRedo()){
			editorGUI.reDoEnable();
		}
		else editorGUI.reDoDisable();
	}
}
